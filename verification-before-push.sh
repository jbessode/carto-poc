#!/bin/bash

SONAR_KEY="49778b262e22a86667285555d2a94d268349cf2d"
SONAR_PROJECT_KEY="carto-poc"
SONAR_SRV_URL="http://vps743774.ovh.net:9000"

run_cmd() {
  if eval "$@"; then
    echo -e "\e[32mOK $*"
  else
    echo -e "\e[31mFAIL $*"
    exit 1
  fi
}


echo ""
echo "--------------- unitaire test stage started ---------------"
 ./mvnw clean verify >> .test-quarkus.log


echo "--------------- analyze stage started ---------------"

run_cmd ./mvnw sonar:sonar \
  -Dsonar.projectKey=$SONAR_PROJECT_KEY \
  -Dsonar.host.url=$SONAR_SRV_URL \
  -Dsonar.login=$SONAR_KEY \
  -Dsonar.java.binaries=target/classes/fr/ngms/ \
  -Dsonar.sources=src/main/java,webapp/src \
  -Dsonar.exclusions=webapp/src/assets,webapp/dist,webapp/node,webapp/src/app/**/*.spec.ts \
  -Dsonar.tests=src/test/java \
  -Dsonar.test.inclusions=**/*.spec.ts \
  -Dsonar.dynamicAnalysis=reuseReports \
  -Dsonar.junit.reportsPath=target/surefire-reports \
  -Dsonar.java.coveragePlugin=jacoco \
  -Dsonar.coverage.exclusions=src/main/java/fr/ngms/cartopoc/orm/**,src/main/java/fr/ngms/cartopoc/models/**,src/main/java/fr/ngms/cartopoc/core/**,src/main/java/fr/ngms/cartopoc/security/utils/** \
  -Dsonar.coverage.jacoco.xmlReportPaths=target/site/jacoco-it/jacoco.xml


echo "--------------- Results ---------------"

# Get analysis result
echo "url to check: $SONAR_SRV_URL/api/qualitygates/project_status?projectKey=$SONAR_PROJECT_KEY"
project_status=$(curl -u $SONAR_KEY: -s $SONAR_SRV_URL/api/qualitygates/project_status?projectKey=$SONAR_PROJECT_KEY | awk -F'"' '{print $6}')
if [ "$project_status" = "OK" ]; then
  echo -e "\e[32mOK $*"
else
  echo "Quality Gate: $project_status"
  echo "Results: $SONAR_SRV_URL/api/qualitygates/project_status?projectKey=$SONAR_PROJECT_KEY"
  echo -e "\e[31mFAIL $*"
  exit 1
fi

echo ""
echo ""
echo "--------------- Results test unitaire---------------"

abc=$(cat ".test-quarkus.log"|grep -A4 -e 'Results:'| grep -e 'Tests run:' -e ', Failures:')
while [ -z "${abc}" ]
do  
  sleep 1
  abc=$(cat ".test-quarkus.log"|grep -A4 -e 'Results:'| grep -e 'Tests run:' -e ', Failures:')
done

verif1=$(echo $abc|cut -d ":" -f3|head -c 2)
verif2=$(echo $abc|cut -d ":" -f4|head -c 2)



if [ $verif1 == 0 ] && [ $verif2 == 0 ];
then
        echo -e "\e[32m$abc\e[0m"
else
        echo -e "\e[31m$abc\e[0m"
fi
echo ""
echo ""
echo "For more information : cat .test-quarkus.log"
echo "To test again : ./mvnw clean verify "



