#!/bin/bash

run_cmd() {
  if eval "$@"; then
    echo -e "\e[32mOK $*"
  else
    echo -e "\e[31mFAIL $*"
    exit 1
  fi
}


GROUP_REGISTRY="registry.gitlab.com/cracs_airbus/ngms"
IMAGE_TAG="v$CI_PIPELINE_IID"

echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $GROUP_REGISTRY
run_cmd docker info

echo "=========== pull image =========== "
run_cmd docker pull $GROUP_REGISTRY/$IMAGE_NAME:latest || true

echo "=========== build image ==========="
run_cmd docker build --tag $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG \
  --build-arg VCS_REF=$CI_PIPELINE_IID --build-arg VCS_URL=$CI_PROJECT_URL \
  --cache-from $GROUP_REGISTRY/$IMAGE_NAME:latest -f $DOCKEFILE_PATH $DOCKEFILE_CONTEXT

echo "=========== push image tagged with ci id ==========="
run_cmd docker push $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG

echo "=========== tag image with latest ==========="
run_cmd docker tag $GROUP_REGISTRY/$IMAGE_NAME:$IMAGE_TAG $GROUP_REGISTRY/$IMAGE_NAME:latest

echo "=========== push image with latest ==========="
run_cmd docker push $GROUP_REGISTRY/$IMAGE_NAME:latest