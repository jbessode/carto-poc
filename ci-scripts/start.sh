#!/bin/bash

run_cmd() {
  if eval "$@"; then
    echo -e "\e[32mOK $*"
  else
    echo -e "\e[31mFAIL $*"
    exit 1
  fi
}

echo "--------------- docker login ---------------"

run_cmd docker login registry.gitlab.com/cracs_airbus/ngms -u cracs -p sb1HzR-X7jc_DaYsMDzQ

sleep 2

name='postgis-ngms'

[[ $(docker ps -f "name=$name" --format '{{.Names}}') == $name ]] ||
  docker docker run --rm -d -p 6543:5432 \
    -e POSTGRES_DB=ngms \
    -e POSTGRES_USER=root \
    -e POSTGRES_PASSWORD=root \
    --name postgis-ngms --network=ngms-network registry.gitlab.com/cracs_airbus/ngms/dockerfiles/postgis

docker kill carto-poc-ngms || true

docker run --rm --pull always -p 15080:15080 \
  --network=ngms-network \
  --name carto-poc-ngms registry.gitlab.com/cracs_airbus/ngms/carto-poc
