#!/bin/bash

echo "-------------- cleaning -------------------"
rm -r target/ || true
rm -r webapp/dist/ || true

echo "--------------- build stage ---------------"

run_cmd() {
  if eval "$@"; then
    echo -e "\e[32mOK $*"
  else
    echo -e "\e[31mFAIL $*"
    exit 1
  fi
}

echo " --------- package application -----------"
run_cmd ./mvnw -Dui.deps verify

run_cmd ./mvnw package -Dui

cd webapp/ && npm run test
cd ..