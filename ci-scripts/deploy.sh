#!/bin/bash

session_name="ngms-app"

run_cmd() {
  if eval "$@"; then
    echo -e "\e[32mOK $*"
  else
    echo -e "\e[31mFAIL $*"
    exit 1
  fi
}

mkdir -p ~/.ssh
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >~/.ssh/config

run_cmd chmod 400 $KEY_SSH_FILENAME

run_cmd scp -i $KEY_SSH_FILENAME -r $BUNDLE_SRC $USER_NAME@$IP_EC2:/home/$USER_NAME/artefacts

ssh -i $KEY_SSH_FILENAME $USER_NAME@$IP_EC2 tmux kill-session -t $session_name || true #todo: find better solution
sleep 10

run_cmd ssh -i $KEY_SSH_FILENAME $USER_NAME@$IP_EC2 tmux new -s $session_name -d '/home/$USER_NAME/run.sh |& tee tmux.log'
# sleep 10
# msg=$(curl -LI http://$IP_EC2:$PORT -o /dev/null -w '%{http_code}\n' -s)
# if [ "$msg" != 200 ]; then
#  echo "status check failed $msg"
#  exit 1
# else
#  exit 0
# fi

# echo "app run on: https://$IP_EC2:$PORT"

