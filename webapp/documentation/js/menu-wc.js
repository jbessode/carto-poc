'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">Cartopoc angular documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/ApiModule.html" data-type="entity-link">ApiModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' : 'data-target="#xs-components-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' :
                                            'id="xs-components-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' : 'data-target="#xs-injectables-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' :
                                        'id="xs-injectables-links-module-AppModule-6195fccea42225e7206d8c3db23759c2"' }>
                                        <li class="link">
                                            <a href="injectables/AppConfigService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AppConfigService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GeocodingModule.html" data-type="entity-link">GeocodingModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' : 'data-target="#xs-components-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' :
                                            'id="xs-components-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' }>
                                            <li class="link">
                                                <a href="components/GeocodingComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GeocodingComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' : 'data-target="#xs-injectables-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' :
                                        'id="xs-injectables-links-module-GeocodingModule-53b6d7c4776e8c817eb55fa095277dc8"' }>
                                        <li class="link">
                                            <a href="injectables/GeocodingService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>GeocodingService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StateService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StateService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ManagementPOIModule.html" data-type="entity-link">ManagementPOIModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' : 'data-target="#xs-components-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' :
                                            'id="xs-components-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' }>
                                            <li class="link">
                                                <a href="components/AddCategoryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddCategoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddLayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddLayerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddPoiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddPoiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AddSpecificPoiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AddSpecificPoiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AttachOtherCategoryLayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AttachOtherCategoryLayerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DeleteCategoryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DeleteCategoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DeleteLayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DeleteLayerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DeletePoiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DeletePoiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ImportPoiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ImportPoiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ManagementPoiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ManagementPoiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UpdateCategoryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UpdateCategoryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UpdateLayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UpdateLayerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UpdatePoiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UpdatePoiComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' : 'data-target="#xs-injectables-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' :
                                        'id="xs-injectables-links-module-ManagementPOIModule-b2afbd94ba95ea387b16e139c4a236d8"' }>
                                        <li class="link">
                                            <a href="injectables/POIService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>POIService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-178d87b907453fd6a4c73086b0429ffc"' : 'data-target="#xs-components-links-module-SharedModule-178d87b907453fd6a4c73086b0429ffc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-178d87b907453fd6a4c73086b0429ffc"' :
                                            'id="xs-components-links-module-SharedModule-178d87b907453fd6a4c73086b0429ffc"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavBarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavBarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageNotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PageNotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserAccountModule.html" data-type="entity-link">UserAccountModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserAccountModule-3d5ace8995872551a142bc747775150e"' : 'data-target="#xs-components-links-module-UserAccountModule-3d5ace8995872551a142bc747775150e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserAccountModule-3d5ace8995872551a142bc747775150e"' :
                                            'id="xs-components-links-module-UserAccountModule-3d5ace8995872551a142bc747775150e"' }>
                                            <li class="link">
                                                <a href="components/AccounLoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccounLoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AccountAuthentificationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccountAuthentificationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SearchUserAccountComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchUserAccountComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserDetailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserDetailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/Configuration.html" data-type="entity-link">Configuration</a>
                            </li>
                            <li class="link">
                                <a href="classes/CustomHttpParameterCodec.html" data-type="entity-link">CustomHttpParameterCodec</a>
                            </li>
                            <li class="link">
                                <a href="classes/Utils.html" data-type="entity-link">Utils</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/DefaultService.html" data-type="entity-link">DefaultService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/AuthInterceptor.html" data-type="entity-link">AuthInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthentificationGuard.html" data-type="entity-link">AuthentificationGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Address.html" data-type="entity-link">Address</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/AuthResponse.html" data-type="entity-link">AuthResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicCategoryPayload.html" data-type="entity-link">BasicCategoryPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicLayersPayload.html" data-type="entity-link">BasicLayersPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/BasicPOIPayload.html" data-type="entity-link">BasicPOIPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CategoryCreationRequest.html" data-type="entity-link">CategoryCreationRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CategoryInformation.html" data-type="entity-link">CategoryInformation</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ConfigurationParameters.html" data-type="entity-link">ConfigurationParameters</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GeoCodingDirectPayload.html" data-type="entity-link">GeoCodingDirectPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GeoCodingIndirectPayload.html" data-type="entity-link">GeoCodingIndirectPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GeoJsonPayload.html" data-type="entity-link">GeoJsonPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Geometry.html" data-type="entity-link">Geometry</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LayerGeoJson.html" data-type="entity-link">LayerGeoJson</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LayerInformation.html" data-type="entity-link">LayerInformation</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LayerInformationRequest.html" data-type="entity-link">LayerInformationRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PoiCategoryEntity.html" data-type="entity-link">PoiCategoryEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PoiCategoryPayload.html" data-type="entity-link">PoiCategoryPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/POIGeoJson.html" data-type="entity-link">POIGeoJson</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/POIImportRequest.html" data-type="entity-link">POIImportRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PoiInformation.html" data-type="entity-link">PoiInformation</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/POIInsertionRequest.html" data-type="entity-link">POIInsertionRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/POIRequest.html" data-type="entity-link">POIRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ResponseBody.html" data-type="entity-link">ResponseBody</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserEntity.html" data-type="entity-link">UserEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserInformation.html" data-type="entity-link">UserInformation</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserInformationRequest.html" data-type="entity-link">UserInformationRequest</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserList.html" data-type="entity-link">UserList</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserListResponse.html" data-type="entity-link">UserListResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserLogin.html" data-type="entity-link">UserLogin</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserLoginRequest.html" data-type="entity-link">UserLoginRequest</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});