export const environment = {
  production: true,
  poiAPIUrl: window.location.origin || 'http://localhost:15080',
  TILESERVER_DEFAULT_PORT: 15020,
  GEOSERVER_DEFAULT_PORT: 8080
};
