import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './modules/app-routing.module';
import {UserAccountModule} from './modules/user-account/user-account.module';
import {ManagementPOIModule} from './modules/management-poi/management-poi.module';
import {GeocodingModule} from './modules/geocoding/geocoding.module';
import {SharedModule} from './modules/shared/shared.module';
import { AppConfigService } from './modules/config/appconfig.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthInterceptor } from './modules/interceptor/interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

export function appInit(appConfigService: AppConfigService) {
  return () => appConfigService.load();
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    UserAccountModule,
    ManagementPOIModule,
    GeocodingModule,
    SharedModule,
    BrowserAnimationsModule,
    NgbModule,
    NgxSpinnerModule,
    NgxPaginationModule
  ],
  providers: [AppConfigService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [AppConfigService]
   }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
