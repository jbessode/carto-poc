export enum Role {
  USER,
  EDITOR,
  ADMIN
}
