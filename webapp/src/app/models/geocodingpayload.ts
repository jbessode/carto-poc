import { Geometry } from "./geometry";

export interface GeoJsonPayload {
    geometry: Geometry,
    properties : any
}