import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import Text from 'ol/style/Text';

export const BASE_STYLE = new Style({
  fill: new Fill({
    color: 'rgba(255, 255, 255, 0.6)',
  }),
  stroke: new Stroke({
    color: '#319FD3',
    width: 1,
  }),
  text: new Text({
    font: '12px Calibri,sans-serif',
    fill: new Fill({
      color: '#000',
    }),
    stroke: new Stroke({
      color: '#fff',
      width: 3,
    }),
  }),
});


export const POINT_STYLE = new Style({
  image: new CircleStyle({
    radius: 10,
    fill: new Fill({ color: 'orange'}),
    stroke: new Stroke({ color: "black", width: 2 }),
  }),
});

export const POINT_STYLE_SELECTED = new Style({
  image: new CircleStyle({
    radius: 10,
    fill: new Fill({ color: 'red'}),
    stroke: new Stroke({ color: "black", width: 2 }),
  }),
});
