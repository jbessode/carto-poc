import { Injectable } from '@angular/core';
import { DefaultService } from 'dist/openapi';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AppConfigService {

  private _cache: Map<string, string>;

  constructor(private api: DefaultService) {
    this._cache = new Map<string, string>();
  }

  load() {
    this.api.configuration.basePath = environment.poiAPIUrl;
  }

  healthCheck() {
    return this.api.healthGet();
  }
  getTileserverUrl(): Observable<any> {
    if (this._cache.has('TILESERVER_URL')) {
      return of(this._cache.get('TILESERVER_URL'));
    }
    return this.api.envValuesGet('TILESERVER_URL');
  }

  getGeoServerUrl(): Observable<any> {
    if (this._cache.has('GEOSERVER_URL')) {
      return of(this._cache.get('GEOSERVER_URL'));
    }
    return this.api.envValuesGet('GEOSERVER_URL');
  }

  putInCache(key: string, value: string) {
    if (!this._cache.has(key)) {
      this._cache.set(key, value);
    }
  }

  getFromCache(key: string) {
    return this._cache.get(key);
  }
}
