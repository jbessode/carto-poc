import {fakeAsync, TestBed} from '@angular/core/testing';

import { POIService } from './poi.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('POIService', () => {
  let service: POIService;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [POIService]
    });
    service = TestBed.inject(POIService);
  }));


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should GET all categories', fakeAsync(() => {
    service.getAllCategories().subscribe((result) => {
      expect(result.length).toBe(6);
      expect(result.length).toBeGreaterThan(0);
    });
  }));


});
