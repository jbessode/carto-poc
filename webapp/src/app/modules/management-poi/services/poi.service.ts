import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {
  CategoryCreationRequest,
  DefaultService,
  LayerInformation,
  LayerInformationRequest, POIImportRequest,
  POIInsertionRequest,
  POIRequest
} from 'dist/openapi';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class POIService {

  // Declare http client & default service
  constructor(private http: HttpClient, private api: DefaultService) {
  }

  /**
   * Call POI api to add category
   * @param category information of category to add
   */
  addCategory(category: CategoryCreationRequest): Observable<any> {
    return this.api.categoriesCreatePost(category);
  }

  /**
   * Call POI api to get all categories
   */
  getAllCategories(): Observable<any> {
    return this.api.categoriesGet();
  }

  /**
   * Call POI api to get all layers in category
   * @param id id of category
   */
  getLayersByCategory(id: number): Observable<any> {
    return this.api.categoriesIdLayersGet(id);
  }

  /**
   * Call POI api to get pois in layer
   * @param id id of layer
   */
  getPoisByLayer(id: number): Observable<any> {
    return this.api.layersIdPoisGet(id);
  }

  /**
   * Call POI api to attach a new layer to given category
   * @param layer Information of layer to attach
   * @param id  id of category
   */
  addLayer(layer: LayerInformationRequest, id: number): Observable<any> {
    return this.api.categoriesIdAttachlayerPost(id, layer);
  }

  /**
   * Call POI API to get specific layer by id
   * @param id id of layer to find
   */
  getLayerById(id: number): Observable<any> {
    return this.api.layersIdGet(id);
  }

  /**
   * Call POI API to update layer with information passed in parameter
   * @param layer Information of layer to update
   * @param id id of layer to update
   */
  updateLayer(layer: LayerInformationRequest, id: number): Observable<any> {
    return this.api.layersUpdateIdPut(id, layer);
  }

  /**
   * Call POI API to delete layer by ID
   * @param id id of layer to delete
   */
  deleteLayer(id: number): Observable<any> {
    return this.api.layersDeleteIdDelete(id);
  }

  /**
   * Call POI API to get to information about category
   * @param id id of category
   */
  getCategoryInformation(id: number): Observable<any> {
    return this.api.categoriesIdGet(id);
  }

  /**
   * Call POI API to update category with following information
   * @param id id of category to update
   * @param category following information to update
   */
  updateCategory(id: number, category: CategoryCreationRequest): Observable<any> {
    return this.api.categoriesUpdateIdPut(id, category);
  }

  /**
   * Call POI API to delete category by ID
   * @param id id of category to delete
   */
  deleteCategory(id: number): Observable<any> {
    return this.api.categoriesDeleteIdDelete(id);
  }

  /**
   * Call POI API to attach one or several pois to an existing layer
   * @param id id of layer
   * @param pois pois to attach
   */
  addPoiToLayer(id: number, pois: POIInsertionRequest): Observable<any> {
    return this.api.layersIdAttachpoiPost(id, pois);
  }

  /**
   * Call POI API to update poi
   * @param poi Information of poi to update
   * @param id id of poi to update
   */
  updatePoi(poi: POIRequest, id: number): Observable<any> {
    return this.api.poiUpdateIdPut(id, poi);
  }

  /**
   * Call POI API  to delete poi by ID
   * @param id id of poi to delete
   */
  deletePoi(id: number): Observable<any> {
    return this.api.poiDeleteIdDelete(id);
  }

  /**
   * Call POI API to export layer of poi by ID
   * @param id
   */
  exportLayer(id: number): Observable<any> {
    return this.api.layersExportIdGet(id);
  }

  /**
   * Call POI API to detach current layer and attach it to new category
   * @param idCategoryCurrent id of current category which layer is attached
   * @param idCategoryAttach id of category to attach layer
   * @param layer information of layer
   */
  detachAndAttachLayerToCategory(idCategoryCurrent: number, idCategoryAttach: number, layer: LayerInformation): Observable<any> {
    return this.api.categoriesIdCurrentCategoryAttachLayerToIdCategoryToAttachPost(idCategoryAttach, idCategoryCurrent, layer);
  }

  /**
   * Call POI API to import layer of POI
   * @param layerId id of layer to import
   * @param poiImport Inofmration of poi to import
   */
  importPois(layerId: number, poiImport: POIImportRequest): Observable<any> {
    return this.api.layersIdImportPoisPost(layerId, poiImport);
  }

}
