import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {CategoryInformation, LayerInformation} from '../../../../../../../dist/openapi';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {POIService} from '../../../services/poi.service';

@Component({
  selector: 'app-attach-other-category-layer',
  templateUrl: './attach-other-category-layer.component.html',
  styleUrls: ['./attach-other-category-layer.component.scss']
})
export class AttachOtherCategoryLayerComponent implements OnInit {
  @Input() idCurrentCategory: number;
  @Input() layer: LayerInformation;
  @Input() categories: Array<CategoryInformation>;
  @Input() layers: Array<LayerInformation>;
  @ViewChild('closebutton') closebutton;
  idCategory: number;

  // Inject poi & toast service
  constructor(private toast: ToastrService, private poiService: POIService) { }

  ngOnInit(): void {
  }

  /**
   * Send attach to other category request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(form: NgForm): void {
    // check if the category is given
    if (!this.idCategory){
      this.toast.error('Category is required', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
    }
    this.poiService.detachAndAttachLayerToCategory(this.idCurrentCategory, this.idCategory, this.layer).subscribe({
      next: _ => {
        this.layers.splice(this.layers.indexOf(this.layer), 1);
        this.toast.success('Layer ' + this.layer.name + ' was attach to category !', 'Layer Category update', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.resetForm(form);
        this.closebutton.nativeElement.click();
      },
      error: err => {
        this.toast.success('Error ' + err.error.message, 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });
  }

  /**
   * Reset all form's field to ""
   * @param form Form bound with ngForm directive
   */
  resetForm(form: NgForm): void{
    this.idCategory = undefined;
    form.reset();
  }

}
