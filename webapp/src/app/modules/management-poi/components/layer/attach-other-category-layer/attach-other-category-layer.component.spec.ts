import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachOtherCategoryLayerComponent } from './attach-other-category-layer.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ToastrModule} from "ngx-toastr";

describe('AttachOtherCategoryLayerComponent', () => {
  let component: AttachOtherCategoryLayerComponent;
  let fixture: ComponentFixture<AttachOtherCategoryLayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttachOtherCategoryLayerComponent ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        ToastrModule.forRoot()
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachOtherCategoryLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
