import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {LayerInformation, LayerInformationRequest, ResponseBody} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {Utils} from 'src/app/helpers/Utils';
import {POIService} from '../../../services/poi.service';
import {FLOAT} from 'ol/webgl';

@Component({
  selector: 'app-add-layer',
  templateUrl: './add-layer.component.html',
  styleUrls: ['./add-layer.component.scss']
})
export class AddLayerComponent implements OnInit {

  addForm: FormGroup;
  listAttributes: FormArray;
  layer: LayerInformationRequest;
  @Input()
  idCategory: number;
  @Input()
  layers: Array<LayerInformation>;
  @ViewChild('closebutton')
  closebutton;
  @ViewChild('icon') iconLayer;
  isEmpty = false;
  base64Icon: any;
  idType: number;
  fileName: string;
  listAttributeDataTypes = [
    {id: 0, label: 'URL', type: URL},
    {id: 1, label: 'Number', type: Number},
    {id: 2, label: 'Email', type: Date},
    {id: 3, label: 'Comment | String', type: String},
    {id: 4, label: 'True | False', type: Boolean},
    {id: 5, label: 'Float', type: FLOAT}
  ];

  // Inject poi & toast service
  constructor(private fb: FormBuilder, private servicePoi: POIService,
              private toastService: ToastrService) {

    this.addForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', [Validators.required]],
      listAttributes: this.fb.array([])
    });
    this.addForm.value.listAttributes = '';
  }

  ngOnInit(): void {
  }

  /**
   * Create formGroup for attributes data which will be insert into FormArray)
   */
  createAttributeData() {
    return this.fb.group({
      listAttributes: ['']
    });
  }

  /**
   * Push attribute data (one FormGroup) into list of attributes (FormArray)
   */
  addAttributeData(): void {
    if (!this.listAttributes) {
      this.listAttributes = this.addForm.get('listAttributes') as FormArray;
    }
    this.listAttributes.push(this.createAttributeData());
  }

  /**
   * Remove attribute data (one FormGroup) from list of attributes (FormArray)
   */
  removeAttributeData(): void {
    if (!this.listAttributes) {
      this.listAttributes = this.addForm.get('listAttributes') as FormArray;
      this.listAttributes.removeAt(0);
    } else {
      this.listAttributes.removeAt(this.listAttributes.length - 1);
    }
  }

  /**
   * Send add layer request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(): void {
    if (this.addForm.value.name === '' || this.addForm.value.description === '') {
      this.toastService.error('Name and description fields can\'t be empty', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (this.addForm.invalid) {
      this.toastService.error('Name, description or attributes field is invalid', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (!this._validateAfterSubmit(this.addForm)) {
      this.toastService.error('Name, description or attributes field is invalid', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    /* if (!this.idType){
       this.toastService.error('Attribute data type is required', 'Error', {
         timeOut: 4000,
         positionClass: 'toast-top-center'
       });
       return;
     }*/
    const attribute = [];
    this.addForm.value.listAttributes.forEach((typeAttribute) => {
      const valueTypeAttribute = Object.values(typeAttribute).toString();
      if (!Utils.noWhitespaceValidator(valueTypeAttribute)) {
        this.isEmpty = true;
      }
      if (valueTypeAttribute != null) {
        attribute.push(valueTypeAttribute);
      }

    });

    if (this.isEmpty) {
      this.toastService.error('Attributes field cannot be blank', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      this.isEmpty = false;
      return;
    }

    // rebuild object
    this.layer = {
      name: this.addForm.value.name,
      description: this.addForm.value.description,
      layerAttributes: attribute,
      icon: this.base64Icon
    };

    this.servicePoi.addLayer(this.layer, this.idCategory).subscribe({
      next: (val: ResponseBody) => {
        const response: LayerInformation = val.payload;
        this.layers.push(response);
        this.toastService.success('Layer ' + response.name + ' was added !', 'Layer added', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.addForm.reset();
        this.base64Icon = null;
        this.closebutton.nativeElement.click();
      },
      error: error => {
        this.toastService.error('Layer was not added ' + error.error.message, 'ERROR ', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.iconLayer.nativeElement.value = '';
        this.closebutton.nativeElement.click();
      }
    });

  }

  /**
   * Reset form with initial values
   */
  resetForm() {
    this.addForm.reset();
    if (this.listAttributes) {
      for (let i = 0; i < this.listAttributes.length; i++) {
        this.listAttributes.removeAt(i);
      }
    }
    this.base64Icon = null;
    this.iconLayer.nativeElement.value = '';
  }

  /**
   * Doing some validations before call to POI API
   * @param form given formGroup
   * @private use only here
   */
  private _validateAfterSubmit(form: FormGroup) {
    let boolName = true;
    let boolDescription = true;
    if (!Utils.noWhitespaceValidator(this.addForm.value.name)) {
      this.addForm.controls.name.setErrors({incorrect: true});
      this.addForm.controls.name.reset();
      boolName = false;
    }
    if (!Utils.noWhitespaceValidator(this.addForm.value.description)) {
      this.addForm.controls.description.setErrors({incorrect: true});
      this.addForm.controls.description.reset();
      boolDescription = false;
    }
    return boolName && boolDescription;
  }

  /**
   * Get image from user computer, to attach it with POI into current layer
   * @param evt asynchronous event
   */
  getPicture(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      const name: string = file.name;
      this.fileName = name;
      if (name.endsWith('png') || name.endsWith('jpg')) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      } else {
        this.toastService.error('Image must be png or jpg format', 'Error Image upload', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.base64Icon = null;
        this.iconLayer.nativeElement.value = '';
        this.fileName = null;
      }
    }
  }

  handleReaderLoaded(e) {
    // this.base64Icon = 'data:image/png;base64,' + btoa(e.target.result);
    this.base64Icon = btoa(e.target.result);
  }

  /**
   * Reset icon of POI
   */
  resetIcon() {
    this.base64Icon = null;
    this.iconLayer.nativeElement.value = '';
    this.fileName = null;
  }

}
