import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {LayerInformation, LayerInformationRequest} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {Utils} from 'src/app/helpers/Utils';
import {POIService} from '../../../services/poi.service';


@Component({
  selector: 'app-update-layer',
  templateUrl: './update-layer.component.html',
  styleUrls: ['./update-layer.component.scss']
})
export class UpdateLayerComponent implements OnInit, OnChanges {

  @Input() layer: LayerInformation;
  layerLocal = {} as LayerInformationRequest;
  @ViewChild('closebutton') closebutton;
  @ViewChild('icon') iconLayer;
  @Output('showLayer') showUpdatingLayer: EventEmitter<any> = new EventEmitter();
  base64Icon : any;
  removeIconForm: boolean = false;
  fileName: string;

  // Inject poi & toast service
  constructor(private fb: FormBuilder, private servicePoi: POIService,
              private toastService: ToastrService) {
  }

  /**
   * This method is call when component is initializing
   */
  ngOnInit(): void {
    if (this.layer == null) {
      return;
    }
  }

  /**
   * This method is call when any data-bound property of a template directive changes
   * @param changes current changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    this.layerLocal.name = this.layer.name;
    this.layerLocal.description = this.layer.description;
  }

  /**
   * Send update layer request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(updateLayerForm: NgForm) {
    if (updateLayerForm.invalid) {
      this.toastService.warning('Form is invalid', 'Warning', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (!this._validateAfterSubmit(updateLayerForm)) {
      this.toastService.error('Form is invalid, you use only escape', 'Warning', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if(this.base64Icon || this.removeIconForm){
      this.layerLocal.icon = this.base64Icon;
    }
    this.servicePoi.updateLayer(this.layerLocal, this.layer.id).subscribe({
      next: _ => {
        this._updateLayer();
        this.showUpdatingLayer.emit({
          id: this.layer.id
        });
        this.layer.icon = _.payload.layers[0].icon;
        this.toastService.success('Layer ' + this.layerLocal.name + ' was updated !', 'Layer Updated', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.iconLayer.nativeElement.value="";
        this.closebutton.nativeElement.click();
      },
      error: _ => {
        this.toastService.error('Layer ' + this.layer.name + ' was not updated !', 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.iconLayer.nativeElement.value="";
        this.closebutton.nativeElement.click();
      }
    });
  }

  /**
   * Doing some validations before call to POI API
   * @param form Form bound with template directive
   * @private use only here
   */
  private _validateAfterSubmit(form: NgForm) {
    let boolName = true;
    let boolDescription = true;
    if (!Utils.noWhitespaceValidator(this.layerLocal.name)) {
      form.controls.name.setErrors({'incorrect': true});
      form.controls.name.reset();
      boolName = false;
    }
    if (!Utils.noWhitespaceValidator(this.layerLocal.description)) {
      form.controls.description.setErrors({'incorrect': true});
      form.controls.description.reset();
      boolDescription = false;
    }
    return boolName && boolDescription;
  }

  /**
   * Update layer with local values
   * @private use only here
   */
  private _updateLayer() {
    this.layer.name = this.layerLocal.name;
    this.layer.description = this.layerLocal.description;
    if(this.base64Icon){
      this.layer.icon = this.base64Icon;
    }
  }

  /**
   * Reset form with local values
   */
  resetForm() {
    this.layerLocal.name = this.layer.name;
    this.layerLocal.description = this.layer.description;
    this.base64Icon = null;
    this.iconLayer.nativeElement.value="";
  }

  /**
   * Get image from user computer, to attach it with POI into current layer
   * @param evt asynchronous event
   */
  getPicture(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      const name: string = file.name;
      this.fileName = name;
      if(name.endsWith("png") || name.endsWith("jpg")) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }else {
        this.toastService.error("Image must be png or jpg format", "Error Image upload",{
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.base64Icon = null;
        this.iconLayer.nativeElement.value="";
        this.fileName = null;
      }
    }
  }
  handleReaderLoaded(e) {
    //this.base64Icon = 'data:image/png;base64,' + btoa(e.target.result);
    this.base64Icon = btoa(e.target.result);
    this.removeIconForm = false;
  }

  /**
   * Reset icon of POIs
   */
  resetIcon(){
    this.base64Icon = null;
    this.iconLayer.nativeElement.value="";
    this.fileName = null;
  }

  /**
   * Remove icon of POIs
   */
  removeIcon(){
    this.resetIcon();
    this.removeIconForm = true;
  }

}
