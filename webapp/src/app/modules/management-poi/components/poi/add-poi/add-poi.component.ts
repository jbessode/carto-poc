import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LayerGeoJson, LayerInformation, POIGeoJson, POIInsertionRequest, POIRequest, ResponseBody} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {Utils} from 'src/app/helpers/Utils';
import {POIService} from '../../../services/poi.service';

@Component({
  selector: 'app-add-poi',
  templateUrl: './add-poi.component.html',
  styleUrls: ['./add-poi.component.scss']
})
export class AddPoiComponent implements OnInit, OnChanges {

  @Input() layer: LayerInformation;
  @Input() poiList: Array<POIGeoJson>;
  @Input() hasAttribute: boolean;
  @Output('showLayer') showUpdatingLayer: EventEmitter<any> = new EventEmitter();
  poiInsertionRequest = {} as POIInsertionRequest;
  poi = {} as POIRequest;
  attributeValue = new Map();
  @ViewChild("closebutton") closebutton;

  constructor(private service: POIService, private toast: ToastrService) {
  }

  ngOnInit(): void {}

  /**
   * This method is call when any data-bound property of a template directive changes
   * @param changes current changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (this.layer.attributes == null) {
      return;
    }
    this.attributeValue = null;
    this.attributeValue = new Map();
  }

  /**
   * Send add poi request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(form: NgForm): void {
    if (form.invalid) {
      this.toast.error("Form invalid", "Warning", {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (!this._validateAfterSubmit(form)) {
      this.toast.error('Name, description or attributes field is invalid', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    this.poi.attributesData = this.attributeValue as any;
    this.poiInsertionRequest.pois = Array<POIRequest>();
    this.poiInsertionRequest.pois.push(this.poi);
    this.service.addPoiToLayer(this.layer.id, this.poiInsertionRequest).subscribe({
      next: (val: ResponseBody) => {
        let layer: LayerGeoJson = val.payload;
        let poiRespone: POIGeoJson = layer.features[layer.features.length - 1];
        if (this.poiList.length < layer.features.length) {
          this.poiList.push(poiRespone);
        }
        this.toast.success("Poi " + poiRespone.properties.name + " has been added to layer " + this.layer.name, "Poi added", {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.showUpdatingLayer.emit({
          id: this.layer.id
        });
        this.closebutton.nativeElement.click();
        form.reset();
      },
      error: err => {
        this.toast.error("Poi " + this.poi.name + " was not added to layer " + this.layer.name, "Error", {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.closebutton.nativeElement.click();
      }
    });
    this.attributeValue = null;
    this.attributeValue = new Map();
  }

  /**
   * Doing some validations before call to POI API
   * @param form Form bound with template directive
   * @private use only here
   */
  private _validateAfterSubmit(form: NgForm): boolean {
    for (let attr of this.layer.attributes) {
      if (!this.attributeValue[attr]) {
        this.attributeValue[attr] = '';
      } else {
        if (!Utils.noWhitespaceValidator(this.attributeValue[attr])) {
          form.controls[attr].setErrors({'incorrect': true});
          return false;
        }
      }
    }
    if (!Utils.noWhitespaceValidator(this.poi.name)) {
      form.controls.poiFormName.setErrors({'incorrect': true});
      form.controls.poiFormName.reset();
      return false;
    }
    return true;
  }

  /**
   * Reset form with initial values
   * @param form Form bound with template directive
   */
  resetForm(form: NgForm): void {
    form.resetForm();
  }

}
