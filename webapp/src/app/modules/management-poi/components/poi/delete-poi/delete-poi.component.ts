import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {LayerInformation, POIGeoJson} from "../../../../../../../dist/openapi";
import {POIService} from "../../../services/poi.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-delete-poi',
  templateUrl: './delete-poi.component.html',
  styleUrls: ['./delete-poi.component.scss']
})
export class DeletePoiComponent implements OnInit, OnChanges {

  @Input() poiToDelete = {} as POIGeoJson;
  @Input() indexTablePoiToDelete: number;
  @Input() poisList: Array<POIGeoJson>;
  @Input() layer: LayerInformation;
  @Output('deletingPoiToMap') deletingPoiToMap: EventEmitter<any> = new EventEmitter();
  latitude: string;
  longitude: string;
  name: string;
  idPoi: number;

  @ViewChild('closebutton') closebutton;

  // Inject poi & toast service
  constructor(private servicePOI: POIService, private toastService: ToastrService) {}

  ngOnInit(): void {}

  /**
   * This method is call when any data-bound property of a template directive changes
   * @param changes current changes
   */
  ngOnChanges(changes: SimpleChanges) {
    this.latitude = this.poiToDelete.geometry.coordinates[1];
    this.longitude = this.poiToDelete.geometry.coordinates[0];
    this.name = this.poiToDelete.properties.name;
    this.idPoi = parseInt(this.poiToDelete.properties.id);
  }

  /**
   * Send delete poi request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(): void {
    this.closebutton.nativeElement.click();

    if (this.idPoi <= 0) {
      return;
    }

    this.servicePOI.deletePoi(this.idPoi).subscribe({
      next: _ => {
        if (this.indexTablePoiToDelete > -1) {
          this.poisList.splice(this.indexTablePoiToDelete, 1)
        }

        this.deletingPoiToMap.emit({
          poi: this.poiToDelete,
          layer: this.layer
        });
        this.toastService.success('Poi ' + this.name + ' was deleted !', 'Poi deleted', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      },
      error: _ => {
        this.toastService.error('Poi ' + this.name+ ' was not deleted !', 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });

  }
}
