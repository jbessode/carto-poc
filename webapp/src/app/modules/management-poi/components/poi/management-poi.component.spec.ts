import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { ToastrModule } from "ngx-toastr";
import { AppConfigService } from "../../../config/appconfig.service";
import { ManagementPoiComponent } from './management-poi.component';

describe('ManagementPoiComponent', () => {
  let component: ManagementPoiComponent;
  let fixture: ComponentFixture<ManagementPoiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ManagementPoiComponent],
      imports: [HttpClientTestingModule, ToastrModule.forRoot(), RouterTestingModule],
      providers: [AppConfigService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementPoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create poiSource', () => {
    expect(component.poiSource).toBeTruthy();
  });

});

