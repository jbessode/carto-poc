import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {
  BasicCategoryPayload,
  BasicLayersPayload, LayerGeoJson,
  LayerInformation, POIGeoJson, POIInsertionRequest, POIRequest,
  ResponseBody
} from "../../../../../../../dist/openapi";
import {POIService} from "../../../services/poi.service";
import {ToastrService} from "ngx-toastr";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Utils} from "../../../../../helpers/Utils";

@Component({
  selector: 'app-add-specific-poi',
  templateUrl: './add-specific-poi.component.html',
  styleUrls: ['./add-specific-poi.component.scss']
})
export class AddSpecificPoiComponent implements OnInit, OnChanges {

  @Input() categoriesList = Object as BasicCategoryPayload;
  @Input() latitude: number;
  @Input() longitude: number;
  @ViewChild("closebutton") closebutton;
  @Output('showLayer') showInsertedLayer: EventEmitter<any> = new EventEmitter();
  addSpecificPoiForm: FormGroup;
  layersList = Array<LayerInformation>();
  currentLayer: LayerInformation;
  poi = {} as POIRequest;
  attributeValue = new Map();
  poiInsertionRequest = {} as POIInsertionRequest;
  hasAttributes: boolean;
  emptyLayerID = null;

  // Inject poi & toast service
  constructor(private fb: FormBuilder, private poiService: POIService,
              private toastService: ToastrService) {
    this.addSpecificPoiForm = this.fb.group({
      namePoi: [null, Validators.required],
      selectedCategory: [null, Validators.required],
      selectedLayer: [null, [Validators.required, Validators.min(1)]],
      latitudePoi: [this.latitude, Validators.required],
      longitudePoi: [this.longitude, Validators.required],
      attributeValuePoi: ['']
    });
  }

  ngOnInit(): void {}


  /**
   * This method is call when any data-bound property of a template directive changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (this.currentLayer && this.currentLayer.attributes == null) {
      return;
    }
    this.attributeValue = null;
    this.attributeValue = new Map();
  }

  /**
   * When changes occurred (when value of select list changes), we call this method
   * @param categoryID, id of category in categoriesList
   */
  onChangesSelectCategory(categoryID: string) {
    this.getLayersByCategory(parseInt(categoryID));
  }

  /**
   * Call poi service to get layers in category by given id
   * @param id, given id
   */
  getLayersByCategory(id: number): void {
    this.poiService.getLayersByCategory(id).subscribe({
      next: (layers: ResponseBody) => {
        const body: BasicLayersPayload = layers.payload;
        this.layersList = body.layers;
        if(this.layersList.length > 0) {
          this.currentLayer = this.layersList[0];
        }
      },
      error: _ => {
        this.toastService.error('An error occurred while requesting the display of layers of category n° ' + id, 'Display layers of specific category', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        return;
      }
    });
  }

  /**
   * Set current layer selected by user in select list
   * @param layerValueFormID id of selected layer by user, into layer list
   */
  setSelectedLayer(layerValueFormID: string): void {
    let idLayer = parseInt(layerValueFormID);
    let layerFound = this.layersList.find(elt => elt.id == idLayer)
    if(layerFound) {
      this.currentLayer = layerFound;
      this.hasAttributes = this.currentLayer.attributes.length > 0 && Utils.noWhitespaceValidator(this.currentLayer.attributes[0]);
    }
  }

  /**
   * Send add specific poi request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit() {
    if (this.addSpecificPoiForm.invalid) {
      this.toastService.error("Form invalid", "Warning", {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    this.poi.name = this.addSpecificPoiForm.value.namePoi;
    this.poi.posX = this.addSpecificPoiForm.value.latitudePoi;
    this.poi.posY = this.addSpecificPoiForm.value.longitudePoi;
    this.poi.attributesData = this.attributeValue as any;
    this.poiInsertionRequest.pois = Array<POIRequest>();
    this.poiInsertionRequest.pois.push(this.poi);
    this.poiService.addPoiToLayer(this.currentLayer.id, this.poiInsertionRequest).subscribe({
      next: (val: ResponseBody) => {
        let layer: LayerGeoJson = val.payload;
        let poiResponse: POIGeoJson = layer.features[layer.features.length - 1];
        this.toastService.success("Poi " + poiResponse.properties.name + " has been added to layer " + this.currentLayer.name, "Poi added", {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.showInsertedLayer.emit({
          id: this.currentLayer.id
        });
        this.closebutton.nativeElement.click();
        this.addSpecificPoiForm.reset();
      },
      error: __ => {
        this.toastService.error("Poi " + this.poi.name + " was not added to layer " + this.currentLayer.name, "Error", {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.closebutton.nativeElement.click();
      }
    });
    this.attributeValue = null;
    this.attributeValue = new Map();

  }

  /**
   * Reset all values of FormGroup and set empty layers list
   */
  resetForm() {
    this.addSpecificPoiForm.reset();
    this.layersList = [];
  }

}
