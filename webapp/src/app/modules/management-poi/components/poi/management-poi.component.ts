import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {
  BasicCategoryPayload,
  BasicLayersPayload,
  LayerGeoJson,
  LayerInformation,
  POIGeoJson,
  ResponseBody
} from 'dist/openapi';
import {saveAs} from 'file-saver';
import {ToastrService} from 'ngx-toastr';
import {Feature, Map, View} from 'ol';
import convexHull from 'ol-ext/geom/ConvexHull';
import Hover from 'ol-ext/interaction/Hover';
import AnimatedCluster from 'ol-ext/layer/AnimatedCluster';
import olms from 'ol-mapbox-style';
import {Control, defaults} from "ol/control";
import MousePosition from "ol/control/MousePosition";
import {createStringXY} from "ol/coordinate";
import GeoJSON from 'ol/format/GeoJSON';
import {Polygon} from 'ol/geom';
import BaseLayer from 'ol/layer/Base';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import * as olProj from 'ol/proj';
import {transform} from 'ol/proj';
import {Cluster, Vector} from 'ol/source';
import TileWMS from 'ol/source/TileWMS';
import {Circle, Fill, Icon, Stroke, Style, Text} from 'ol/style';
import {POINT_STYLE, POINT_STYLE_SELECTED} from 'src/app/helpers/constants';
import {Utils} from 'src/app/helpers/Utils';
import {AppConfigService} from 'src/app/modules/config/appconfig.service';
import {environment} from "../../../../../environments/environment";
import {Role} from "../../../../models/roles";
import {UserService} from "../../../user-account/service/user.service";
import {POIService} from '../../services/poi.service';


@Component({
  selector: 'app-management-poi',
  templateUrl: './management-poi.component.html',
  styleUrls: ['./management-poi.component.scss']
})
export class ManagementPoiComponent implements OnInit, AfterViewInit {

  //  properties of selected element
  idSelectedCategory: number;
  idSelectedLayer: number;
  selectionedLayer: number;
  selectedLayer = {} as LayerInformation;
  nameOfSelectedCategory: string;
  nameOfSelectedLayer: string;
  descriptionOfSelectedCategory: string;
  descriptionOfSelectedLayer: string;
  selectedPoi = {} as POIGeoJson;
  lastFeatureSelected: Feature;

  // index
  indexCategory: number;
  indexLayer: number;
  indexSelectedPoi: number;
  poiIndexClicked: number;
  poiIndexTab: number;

  // map properties
  map: Map;
  poiSource: Vector;
  currentLatitude: number;
  currentLongitude: number;
  styleCache = {
    1: POINT_STYLE
  } as Object;

  //wms or geojson
  wmsProvider: boolean = false;
  providerForm: FormGroup;
  currentLayerWMS: BaseLayer;

  // payloads
  layers = Object as BasicLayersPayload;
  allCategoriesModel = Object as BasicCategoryPayload;

  listLayers = Array<LayerInformation>();
  pois = Array<POIGeoJson>();

  // boolean
  showLayers = false;
  isEmptyAttribute: boolean;
  addPoiToMapIsEnable = false;

  // constants
  numberLayerPerPage = 5;
  currentLayerPage = 1;
  numberCategoryPerPage = 10;
  currentCategoryPage = 1;
  numberPoiPerPage = 5;
  currentPoiPage = 1;
  numberLayersByCategory: number;


  constructor(private poiService: POIService,
              private toast: ToastrService,
              private elementRef: ElementRef,
              private configService: AppConfigService,
              private userService: UserService
  ) {
    this.poiSource = new Vector({
      format: new GeoJSON()
    });
    this.selectedPoi = {
      geometry: {
        coordinates: []
      },
      properties: {},
      type: ''
    };
    this.providerForm = new FormGroup({
      provider: new FormControl("geojson")
    });
  }

  /**
   * This method is call when management-poi component is initializing.
   * Get all categories with call to POI API
   */
  ngOnInit(): void {
    this.poiService.getAllCategories().subscribe({
        next: (cats: ResponseBody) => {
          this.allCategoriesModel = cats.payload;
        },
        error: _ => {
          this.toast.error('An error occurred while requesting the display of POI categories', 'Display of POI categories', {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });
          return;
        }
      }
    );

    this.initializeMapAndBottomLayer();
    this.addClusterLayer();

    this.addClickInteractiontoMap();
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'white';
  }

  /**
   * Initialize map with mouse control & bottom layer from TileServer GL
   */
  initializeMapAndBottomLayer(): void {
    let mousePositionControl = new MousePosition({
      coordinateFormat: createStringXY(4),
      projection: 'EPSG:4326',
      undefinedHTML: '&nbsp;'
    });
    this.map = new Map({
      controls: defaults().extend([mousePositionControl]),
      target: 'myMap',
      layers: [],
      view: new View({
        projection: 'EPSG:3857',
        center: olProj.fromLonLat([1.5976721, 42.5422699]),
        zoom: 1,
        showFullExtent: true
      })
    });

    this.configService.getTileserverUrl().subscribe({
      next: (response) => {
        let url = response.TILESERVER_URL || `${window.location.protocol}//${window.location.hostname}:${environment.TILESERVER_DEFAULT_PORT}/styles/basic-preview/style.json`;
        this.configService.putInCache('TILESERVER_URL', url);
        olms(this.map, url);
      },
      error: err => console.error(err)
    });


    this.configService.getGeoServerUrl().subscribe({
      next: (response) => {
        let geoserverUrl = response.GEOSERVER_URL || `${window.location.protocol}//${window.location.hostname}:${environment.GEOSERVER_DEFAULT_PORT}/geoserver/ngms/wms`;
        this.configService.putInCache('GEOSERVER_URL', geoserverUrl);
        this.providerForm
          .valueChanges
          .subscribe({
            next: (data: any) => {
              let provider: string = data.provider.toString().toLowerCase();
              if (provider === "wms") {
                this.wmsProvider = true;
                let targetLayer = this.selectedLayer.id;
                this.map.removeLayer(this.currentLayerWMS);
                this.currentLayerWMS = new TileLayer({
                  source: new TileWMS({
                    url: geoserverUrl,
                    params: {
                      'LAYERS': 'ngms:ngms-poi',
                      'TILED': true,
                      'TRANSPARENT': true,
                      'STYLES': 'ngms:point-ngms',
                      'CRS': 'EPSG:3857',
                      'CQL_FILTER': `layer_id = ${targetLayer}`
                    },
                    serverType: 'geoserver',
                    transition: 0,
                  }),
                  zIndex: 1001
                });
                this.map.addLayer(this.currentLayerWMS);
                this.poiSource.clear();
              } else {
                this.wmsProvider = false;
                if (this.currentLayerWMS) {
                  this.map.removeLayer(this.currentLayerWMS);
                  this.showPoisByLayer(this.selectedLayer);
                }
              }
            }
          });
      },
      error: err => console.error(err)
    });
  }

  /**
   * Create custom for cluster layer
   * @param feature given feature from poi points
   * @param resolution given resolution
   */
  getStyle(feature: Feature, resolution: number): any {
    const size = feature.get('features').length;
    let style = this.styleCache[size];
    if (this.selectedLayer && size === 1) {
      if (this.selectedLayer.icon) {
        const image = 'data:image/png;base64,' + this.selectedLayer.icon;
        style = new Style({
          image: new Icon({
            src: image
          })
        });
      }
    }
    const coordsFeature = (feature.getGeometry() as any).flatCoordinates;
    const coordslastFeatureSelected = (this.lastFeatureSelected ? (this.lastFeatureSelected.getGeometry() as any).flatCoordinates : []);
    const equals = (coordsFeature[0] == coordslastFeatureSelected[0] && coordsFeature[1] == coordslastFeatureSelected[1]);
    if (equals) {
      return POINT_STYLE_SELECTED;
    }
    if (!style) {
      const color = size > 25 ? '248, 128, 0' : size > 8 ? '248, 192, 0' : '128, 192, 64';
      const radius = Math.max(8, Math.min(size * 0.75, 20));
      style = this.styleCache[size] = [
        new Style({
          image: new Circle({
            radius: radius + 2,
            stroke: new Stroke({
              color: 'rgba(' + color + ',0.3)',
              width: 4
            })
          })
        }),
        new Style({
          image: new Circle({
            radius,
            fill: new Fill({
              color: 'rgba(' + color + ',0.6)'
            })
          }),
          text: new Text({
            text: size.toString(),
            fill: new Fill({
              color: '#000'
            })
          })
        })
      ];
    }
    return style;
  }


  /**
   * Add basic cluster layer with hover interaction to map, with custom style (getStyle method call)
   */
  addClusterLayer(): void {
    let button = document.createElement('button');
    button.innerHTML = '<i class="" aria-hidden="true">&#x1F4CC;</i>';

    let handleAddPoi = e => {
      let specificPoiSpan = document.getElementById("spanAddSpecificPoi");
      let specificPoiButton = button;
      if (this.addPoiToMapIsEnable) {
        specificPoiSpan.innerHTML = "";
        specificPoiButton.style.backgroundColor = "#003c8880";
        this.addPoiToMapIsEnable = false;
      } else {
        specificPoiSpan.innerHTML = "The feature of adding poi on the map is active";
        specificPoiButton.style.backgroundColor = "#ffc107";
        this.addPoiToMapIsEnable = true;
      }
      e.stopPropagation()
    };
    button.addEventListener('click', handleAddPoi, false);

    let element = document.createElement('div');
    element.className = 'ol-unselectable ol-control';
    element.style.top = '75px';
    element.style.left = '8px';
    element.appendChild(button);

    let AddPoiControl = new Control({
      element: element
    });
    this.map.addControl(AddPoiControl);

    // Cluster Source
    const clusterSource = new Cluster({
      distance: 40,
      source: this.poiSource
    });
    // Animated cluster layer
    const clusterLayer = new AnimatedCluster(
      {
        name: 'Cluster',
        source: clusterSource,
        style: this.getStyle.bind(this),
        zIndex: 1001
      });
    this.map.addLayer(clusterLayer);

    // Add over interaction that draw hull in a layer
    const vector = new VectorLayer({source: new Vector()});
    vector.setMap(this.map);

    const hover = new Hover({
      cursor: 'pointer', layerFilter(l) {
        return l === clusterLayer;
      }
    });
    this.map.addInteraction(hover);
    hover.on('enter', (e) => {
      let h = e.feature.get('convexHull');
      if (!h) {
        const cluster = e.feature.get('features');
        if (cluster && cluster.length) {
          const c = [];
          for (let i = 0, f; f == cluster[i]; i++) {
            c.push(f.getGeometry().getCoordinates());
          }
          h = convexHull(c);
          e.feature.get('convexHull', h);
        }
      }
      vector.getSource().clear();
      if (h.length > 2) {
        vector.getSource().addFeature(new Feature(new Polygon([h])));
      }
    });
    hover.on('leave', () => {
      vector.getSource().clear();
    });
  }

  /**
   * Add click interaction to map with possibility to save coordinates & zoom to current poi
   */
  addClickInteractiontoMap(): void {
    this.map.on('click', e => {
      const feature = this.map.forEachFeatureAtPixel(e.pixel, feat => {
        return feat;
      });
      if (e.coordinate && e.coordinate.length == 2) {
        let coordinates = transform(e.coordinate, 'EPSG:3857', 'EPSG:4326');
        this.currentLatitude = coordinates[0];
        this.currentLongitude = coordinates[1];
      }
      if (feature && feature.getGeometry().getType() === 'Point') {
        this.pois.filter(item => (feature.getProperties()) && (feature.getProperties().features[0]) &&
          item.properties.id === feature.getProperties().features[0].getProperties().id).forEach(poi => {
          this.currentPoiPage = Math.trunc(this.pois.indexOf(poi) / 5) + 1;
          this.showPoiClicked(poi, this.pois.indexOf(poi) % 5);
        });
      }

    });
  }

  /**
   * Change boolean value to 'enable' add poi to map functionality
   * @param boolean true if functionality is active, false otherwise
   */
  enableAddPoi(boolean): void {
    this.addPoiToMapIsEnable = boolean;
  }

  /**
   * Set selected layer when user click on element in table
   * @param layer current layer information
   * @param layerIndex index of layer in table
   */
  setSelectedLayer(layer: LayerInformation, layerIndex: number): void {
    this.selectedLayer = layer;
    this.idSelectedLayer = layer.id;
    this.nameOfSelectedLayer = layer.name;
    this.descriptionOfSelectedLayer = layer.description;
    this.indexLayer = (this.currentLayerPage === 1) ? layerIndex : layerIndex + (this.numberLayerPerPage * (this.currentLayerPage - 1));
    this.isEmptyAttribute = layer.attributes.length > 0 && Utils.noWhitespaceValidator(layer.attributes[0]);

    if (this.wmsProvider) {
      this.poiSource.clear();
      this.map.removeLayer(this.currentLayerWMS);

      let geoserverUrl = this.configService.getFromCache('GEOSERVER_URL');
      this.currentLayerWMS = new TileLayer({
        source: new TileWMS({
          url: geoserverUrl,
          params: {
            'LAYERS': 'ngms:ngms-poi',
            'TILED': true,
            'TRANSPARENT': true,
            'STYLES': 'ngms:point-ngms',
            'CRS': 'EPSG:3857',
            'CQL_FILTER': `layer_id = ${layer.id}`
          },
          serverType: 'geoserver',
          transition: 0,
        }),
        zIndex: 1001
      });
      this.map.addLayer(this.currentLayerWMS);
    }
  }

  /**
   * Set selected category when user click on accordion item (table) and
   * calls service to get all layers in this one.
   * @param indexCategoryInTable index of category in table
   */
  setSelectedCategory(indexCategoryInTable: number): void {
    this.indexCategory = (this.currentCategoryPage === 1) ?
      indexCategoryInTable : indexCategoryInTable + (this.numberCategoryPerPage * (this.currentCategoryPage - 1));
    if (indexCategoryInTable > -1) {
      this.idSelectedCategory = this.allCategoriesModel.categories[indexCategoryInTable].id;
      this.nameOfSelectedCategory = this.allCategoriesModel.categories[indexCategoryInTable].name;
      this.descriptionOfSelectedCategory = this.allCategoriesModel.categories[indexCategoryInTable].description;
    }
    this.poiService.getLayersByCategory(this.idSelectedCategory).subscribe({
      next: (layers: ResponseBody) => {
        const body: BasicLayersPayload = layers.payload;
        this.numberLayersByCategory = body.layers.length;
      },
      error: _ => {
        this.toast.error('An error occurred while requesting the display of layers of category n° ' +
          this.idSelectedCategory, 'Display layers of specific category', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        return;
      }
    });

  }

  /**
   * Set selected poi when user click on element in sub-accordion item
   * @param id id of current poi
   * @param poi geojson (information) of poi
   * @param tabIndex index of poi in table
   */
  setSelectedPoi(id: number, poi: POIGeoJson, tabIndex: number): void {
    this.indexSelectedPoi = (this.currentPoiPage === 1) ? id : id + (this.numberPoiPerPage * (this.currentPoiPage - 1));
    this.selectedPoi = poi;
    this.poiIndexTab = tabIndex;
  }

  /**
   * Calls service to get all layer in given category
   * @param id id of given category
   */
  getLayersByCategory(id: number): void {
    this.currentLayerPage = 1;
    this.idSelectedCategory = id;
    this.showLayers = false;
    this.poiService.getLayersByCategory(id).subscribe({
      next: (layers: ResponseBody) => {
        const body: BasicLayersPayload = layers.payload;
        this.listLayers = body.layers;
      },
      error: _ => {
        this.toast.error('An error occurred while requesting the display of layers of category n° ' + id, 'Display layers of specific category', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        return;
      }
    });
  }

  /**
   * Show POI for pagination
   * @param page current page
   */
  poiPageChange(page): void {
    this.currentPoiPage = page;
    this.showPoisByLayer({
      id: this.selectionedLayer
    });
  }

  /**
   * Call service to get all pois in given layer
   * @param id id of given layer
   */
  getPois(id: number): void {
    this.currentPoiPage = 1;
    this.selectionedLayer = id;
    this.poiService.getPoisByLayer(id).subscribe({
      next: (pois: ResponseBody) => {
        const poisLayer: LayerGeoJson = pois.payload;
        this.pois = poisLayer.features;
        this.showLayers = true;
      },
      error: () => {
        this.toast.error('An error occurred while requesting POIs ' + id, 'Display POIs of specific layer', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        return;
      }
    });
  }

  /**
   * Call service to show in map all pois in given layer
   * @param layer given layer
   */
  showPoisByLayer(layer: LayerInformation): void {
    if (this.wmsProvider) {
      return;
    }

    this.poiIndexClicked = null;
    this.lastFeatureSelected = null;
    this.map.getView().setZoom(1);
    this.poiSource.refresh();
    this.poiService.getPoisByLayer(layer.id).subscribe({
      next: (pois: ResponseBody) => {
        const layerGeoJson: LayerGeoJson = pois.payload;
        if (layerGeoJson.features.length === 0) {
          return;
        }
        const format = new GeoJSON();
        const features = format.readFeatures(layerGeoJson, {featureProjection: 'EPSG:3857'});
        this.poiSource.addFeatures(features);
        this.map.getView().setCenter(olProj.fromLonLat(layerGeoJson.features[0].geometry.coordinates));
        this.map.getView().setZoom(6);
        this.map.updateSize();
      },
      error: __ => {
        this.toast.error('An error occurred while requesting POIs ', 'Display POIs of specific layer', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        return;
      }
    });

  }

  /**
   * After user interaction by clicking in table, show poi clicked in map
   * @param poi current poi
   * @param index index of poi in table
   */
  showPoiClicked(poi: POIGeoJson, index: number): void {
    this.poiIndexClicked = index;
    this.poiSource.getFeaturesAtCoordinate(olProj.fromLonLat(poi.geometry.coordinates)).forEach(feature => {
      if (feature) {
        this.lastFeatureSelected = feature;
        const viewMap = this.map.getView();
        viewMap.setCenter(olProj.fromLonLat(poi.geometry.coordinates));
        viewMap.fit(feature.getGeometry().getExtent(), {duration: 1000});
        viewMap.setZoom(15);
      }
    });
  }

  /**
   * Call service to export given layer
   * @param layer given layer
   */
  exportLayer(layer: LayerInformation): void {
    const id = layer.id;
    this.poiService.exportLayer(id).subscribe({
      next: (value: ResponseBody) => {
        const file = new Uint8Array(atob(value.message).split('').map(char => char.charCodeAt(0)));
        const blob = new Blob([file], {type: 'application/vnd.geo+json;charset=utf-8'});
        const decode = new File([blob],
          layer.name + '.geojson');
        this.toast.success('Export of the layer ' + layer.name + ' has began', 'Export', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        saveAs(decode);

      },
      error: __ => {
        this.toast.error('An error occurred while exporting POIs ', 'Display POIs of specific layer', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });
  }

  /**
   * Show updated poi in map
   * @param event asynchronous event
   */
  showPoiUpdate(event): void {
    const format = new GeoJSON();
    const feature = format.readFeature(event.poi, {featureProjection: 'EPSG:3857'});
    const poi: POIGeoJson = event.poiToDelete;
    this.poiSource.getFeaturesAtCoordinate(olProj.fromLonLat(poi.geometry.coordinates)).forEach(feat => {
      this.poiSource.removeFeature(feat);
    });
    this.poiSource.addFeature(feature);
    this.showPoiClicked(event.poi, event.index);
  }

  /**
   * Show updated layer in map
   * @param event asynchronous event
   */
  showLayerUpdate(event): void {
    this.poiService.getLayerById(event.id).subscribe({
      next: (_: ResponseBody) => {
        const layer: LayerInformation = _.payload;
        this.showPoisByLayer(layer);
      }
    });
  }

  /**
   * Display toast which contains description of selected category
   */
  toastDescriptionCategory(): void {
    this.toast.info(this.descriptionOfSelectedCategory, 'Description of category ' + this.nameOfSelectedCategory, {
      timeOut: 8000,
      progressBar: true,
      closeButton: true,
      positionClass: 'toast-top-center'
    });
  }

  /**
   * Delete poi inside event
   * @param event asynchronous event
   */
  deletePoiToMap(event): void {
    const layer: LayerInformation = event.layer;
    this.showPoisByLayer(layer);
  }

  /**
   * Refresh poi layer in map
   * @param event asynchronous event
   */
  refreshMap(event): void {
    this.poiSource.refresh();
  }

  /**
   * Import pois in map inside event
   * @param event asynchronous event
   */
  importPoiToMap(event) {
    this.pois = event.pois;
    this.showPoisByLayer(event.layer);
  }

  /**
   * Boolean method to know if user is admin or not
   */
  isAdmin(): boolean {
    return this.userService.getUserRoles() === Role.ADMIN;
  }

  /**
   * Boolean method to know if user is editor or not
   */
  isEditor(): boolean {
    return this.userService.getUserRoles() === Role.EDITOR;
  }

}
