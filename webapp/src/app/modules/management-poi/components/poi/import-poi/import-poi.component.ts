import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {LayerGeoJson, LayerInformation, POIGeoJson, POIImportRequest} from "../../../../../../../dist/openapi";
import {NgForm} from "@angular/forms";
import {POIService} from "../../../services/poi.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-import-poi',
  templateUrl: './import-poi.component.html',
  styleUrls: ['./import-poi.component.scss']
})
export class ImportPoiComponent implements OnInit {
  @Input() layer: LayerInformation;
  @Input() poiList: Array<POIGeoJson>;
  base64Icon: string = undefined;
  poiImport: POIImportRequest;
  fileName: string;
  @ViewChild('closebutton') closebutton;
  @Output('importPois') importPois: EventEmitter<any> = new EventEmitter();

  // Inject poi & toast service
  constructor(private service: POIService, private toast: ToastrService) {
  }

  ngOnInit(): void {
  }

  /**
   * Get image from user computer, to attach it with POI into current layer
   * @param event asynchronous event
   */
  getFile(event): void {
    const file = event.target.files[0];
    if (file) {
      const name: string = file.name;
      this.fileName = name;
      if (name.endsWith("geojson")) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      } else {
        this.toast.error("File must be geojson format", "Error File upload", {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.base64Icon = null;
      }
    }
  }

  handleReaderLoaded(e) {
    //this.base64Icon = 'data:image/png;base64,' + btoa(e.target.result);
    this.base64Icon = btoa(e.target.result);
  }

  /**
   * Send add layer request as soon as the form is submitted,
   * and close modal window
   * @param form Form bound with ngModel directive
   */
  onSubmit(form: NgForm): void {
    if (!this.base64Icon) {
      this.toast.error("File not selected", "Error File upload", {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    this.poiImport = {
      file64Encoded: this.base64Icon
    };
    this.service.importPois(this.layer.id, this.poiImport).subscribe({
      next: _ => {
        const layer: LayerGeoJson = _.payload;
        this.importPois.emit({
          layer: this.layer,
          pois: layer.features
        });
        this.toast.success("POI'S of file " + this.fileName + " where imported to layer " + this.layer.name
          , "Success import", {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });


        this.resetForm(form);
        this.closebutton.nativeElement.click();
      },
      error: _ => {
        console.log(_);
        const msg = (_.error) ? _.error.message : _.message;
        this.toast.error("POI'S not imported due to : " + msg, "Error", {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.resetForm(form);
      }
    });
  }

  /**
   * Reset form with undefined values
   * @param form Form bound with ngModel directive
   */
  resetForm(form: NgForm): void {
    this.base64Icon = undefined;
    this.fileName = undefined;
  }

  /**
   * Reset file with undefined values
   * @param form Form bound with ngModel directive
   */
  resetFile(): void {
    this.base64Icon = undefined;
    this.fileName = undefined;
  }
}
