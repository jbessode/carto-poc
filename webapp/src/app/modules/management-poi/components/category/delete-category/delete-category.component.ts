import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {POIService} from "../../../services/poi.service";
import {BasicCategoryPayload} from "../../../../../../../dist/openapi";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-delete-category',
  templateUrl: './delete-category.component.html',
  styleUrls: ['./delete-category.component.scss']
})
export class DeleteCategoryComponent implements OnInit {

  @Input() idCategoryToDelete: number;
  @Input() nameCategory: string;
  @Input() descriptionCategory: string;
  @Input() numberOfLayersAttachedToCategory: number;
  @Input() categories: BasicCategoryPayload;
  @Input() indexTableCategory: number;
  @ViewChild('closebutton') closebutton;
  @Output('removeCategoryFromMap') removeCategoryFromMap: EventEmitter<any> = new EventEmitter();

  // Inject poi & toast service
  constructor(private servicePOI: POIService, private toastService: ToastrService) {
  }

  ngOnInit(): void {
  }

  /**
   * Send delete category request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(): void {
    this.closebutton.nativeElement.click();

    if (this.idCategoryToDelete <= 0) {
      return;
    }

    this.servicePOI.deleteCategory(this.idCategoryToDelete).subscribe({
      next: _ => {
        if (this.indexTableCategory > -1) {
          this.categories.categories.splice(this.indexTableCategory, 1);
        }
        this.toastService.success('Category ' + this.nameCategory + ' was deleted !', 'Category deleted', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.removeCategoryFromMap.emit();
      },
      error: _ => {
        this.toastService.error('Category ' + this.nameCategory + ' was not deleted !', 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });

  }

}
