import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddCategoryComponent } from './add-category.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import { ToastrModule } from 'ngx-toastr';

describe('AddCategoryComponent', () => {
  let component: AddCategoryComponent;
  let fixture: ComponentFixture<AddCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        ToastrModule.forRoot()
      ],
      declarations: [ AddCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should return False when form is empty", () => {
    expect(component.addForm.valid).toBeFalsy();
  });

  it("should be Falsy when nameCategory field is empty", () => {
    let name = component.addForm.controls['nameCategory'];
    expect(name.valid).toBeFalsy();
  });

  it("should be falsy when descriptionCategory field is empty", () => {
    let description = component.addForm.controls['descriptionCategory'];
    expect(description.valid).toBeFalsy();
  });

  it("should be truthy when nameCategory field contains errors", () => {
    let errors: {};
    let name = component.addForm.controls['nameCategory'];
    errors = name.errors || {};
    expect(errors['required']).toBeTruthy();
  });
  
  it("should be truthy when descriptionCategory field contains errors", () => {
    let errors: {};
    let description = component.addForm.controls['descriptionCategory'];
    errors = description.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it("should be correct when set values in form", () => {
    expect(component.addForm.valid).toBeFalsy();
    component.addForm.controls['nameCategory'].setValue("Natural parks");
    component.addForm.controls['descriptionCategory'].setValue("Category which contains natural parks");
    expect(component.addForm.valid).toBeTruthy();
  });

  it("should be truthy when nameCategory length is less than two", () => {
    let errors: {};
    component.addForm.controls['nameCategory'].setValue("N");
    let name = component.addForm.controls['nameCategory'];
    errors = name.errors || {};
    expect(errors['minlength']).toBeTruthy();
  });

  it("should be truthy when descriptionCategory length is less than two", () => {
    let errors: {};
    component.addForm.controls['descriptionCategory'].setValue("C")
    let description = component.addForm.controls['descriptionCategory'];
    errors = description.errors || {};
    expect(errors['minlength']).toBeTruthy();
  })

  it("should reset form correctly", () => {
    component.addForm.controls['nameCategory'].setValue("Natural parks");
    component.addForm.controls['descriptionCategory'].setValue("Category which contains natural parks");
    component.addForm.reset();
    expect(component.addForm.valid).toBeFalsy();
  });

  it("should be falsy when one field is empty", () => {
    component.addForm.controls['nameCategory'].setValue("Natural parks");
    expect(component.addForm.valid).toBeFalsy();
  });
});
