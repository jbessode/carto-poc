import {Component, Input, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {BasicCategoryPayload, CategoryCreationRequest, CategoryInformation, ResponseBody} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {POIService} from '../../../services/poi.service';
import { Utils } from 'src/app/helpers/Utils';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent {

  addForm: FormGroup;
  private _category = {} as CategoryCreationRequest;
  @Input() categories: BasicCategoryPayload;
  @ViewChild('closebutton') closebutton;

  // Inject poi & toast service
  constructor(private fb: FormBuilder, private servicePoi: POIService, private toastService: ToastrService) {
    this.addForm = this.fb.group({
      nameCategory: ['', Validators.required],
      descriptionCategory: ['', Validators.required]
    });
  }

  /**
   * Send add category request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(): void {
    this._category.description = this.addForm.value['descriptionCategory'];
    this._category.name = this.addForm.value['nameCategory'];
    if (this._category.name === '' || this._category.description === '') {
      this.toastService.error('Name and Description fields can\'t be empty' , 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (this.addForm.invalid) {
      this.toastService.error('Invalid fields for name and description' , 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    if(!this._validateAfterSubmit(this.addForm)){
      this.toastService.error('Form is invalid, you use only escape', 'Warning', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    this.servicePoi.addCategory(this._category).subscribe({
      next: (cat: ResponseBody) => {
        let category: CategoryInformation = cat.payload;
        this.categories.categories.push(category);
        this.toastService.success('Category ' + category.name + ' was created !', 'Category created', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.addForm.reset();
      },
      error: _ => {
        this.toastService.error('Category was not created !', 'ERROR', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });
    this.closebutton.nativeElement.click();
  }

  /**
   * Doing some validations before call to POI API
   * @param form AddCategory formGroup
   * @private use only here
   */
  private _validateAfterSubmit(form: FormGroup){
    let boolName = true;
    let boolDescription = true;
    if(!Utils.noWhitespaceValidator(form.controls.nameCategory.value)){
      form.controls.nameCategory.setErrors({'incorrect' : true});
      form.controls.nameCategory.reset();
      boolName = false;
    }
    if(!Utils.noWhitespaceValidator(form.controls.descriptionCategory.value)){
      form.controls.descriptionCategory.setErrors({'incorrect' : true});
      form.controls.descriptionCategory.reset();
      boolDescription = false;
    }
    return boolName && boolDescription;
  }
}
