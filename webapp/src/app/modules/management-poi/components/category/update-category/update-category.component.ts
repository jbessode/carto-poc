import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
  BasicCategoryPayload,
  CategoryCreationRequest,
  ResponseBody
} from "../../../../../../../dist/openapi";
import {POIService} from "../../../services/poi.service";
import {ToastrService} from "ngx-toastr";
import {Utils} from 'src/app/helpers/Utils';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.scss']
})
export class UpdateCategoryComponent implements OnInit, OnChanges {

  @Input() categories: BasicCategoryPayload;
  @Input() id: number;
  @Input() indexTableCategory: number;
  @Input() nameCategoryToUpdate: string;
  @Input() descriptionCategoryToUpdate: string;
  @ViewChild('closebutton') closebutton;
  updateCategoryForm: FormGroup;
  private _category = {} as CategoryCreationRequest;
  private _saveCategory = {} as CategoryCreationRequest

  // Inject poi & toast service
  constructor(private fb: FormBuilder, private servicePoi: POIService, private toastService: ToastrService) {
    this.updateCategoryForm = this.fb.group({
      nameCategory: [this.nameCategoryToUpdate, Validators.required],
      descriptionCategory: [this.descriptionCategoryToUpdate, Validators.required]
    });
  }

  /**
   * This method is called when any data-bound property of a template directive changes
   * @param changes current changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    this.updateCategoryForm = this.fb.group({
      nameCategory: [this.nameCategoryToUpdate, Validators.required],
      descriptionCategory: [this.descriptionCategoryToUpdate, Validators.required]
    });
    this._saveCategory.name = this.nameCategoryToUpdate;
    this._saveCategory.description = this.descriptionCategoryToUpdate;
  }

  ngOnInit(): void {
  }

  /**
   * Send update category request as soon as the form is submitted,
   * and close modal window
   */
  onSubmit(): void {
    this._category.name = this.updateCategoryForm.value['nameCategory'];
    this._category.description = this.updateCategoryForm.value['descriptionCategory'];

    if (this._category.name === '' || this._category.description === '') {
      this.toastService.error('Name and Description fields can\'t be empty', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }
    if (this.updateCategoryForm.invalid) {
      this.toastService.error('Invalid fields for name or description.', 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    if (!this._validateAfterSubmit(this.updateCategoryForm)) {
      this.toastService.error('Form is invalid, you use only escape', 'Warning', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    if (this.id <= 0) {
      return;
    }

    this.servicePoi.updateCategory(this.id, this._category).subscribe({
      next: (categoryResponseBody: ResponseBody) => {
        this.categories.categories[this.indexTableCategory] = categoryResponseBody.payload;
        this.toastService.success('Category ' + this.nameCategoryToUpdate + ' was updated !', 'Category Updated', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      },
      error: _ => {
        this.toastService.error('Category ' + this.nameCategoryToUpdate + ' was not updated !', 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });
    this.closebutton.nativeElement.click();
  }

  /**
   * Doing some validations before call to POI API
   * @param form given UpdateCategory formGroup
   * @private use only here
   */
  private _validateAfterSubmit(form: FormGroup) {
    let boolName = true;
    let boolDescription = true;
    if (!Utils.noWhitespaceValidator(form.controls.nameCategory.value)) {
      form.controls.nameCategory.setErrors({'incorrect': true});
      form.controls.nameCategory.reset();
      boolName = false;
    }
    if (!Utils.noWhitespaceValidator(form.controls.descriptionCategory.value)) {
      form.controls.descriptionCategory.setErrors({'incorrect': true});
      form.controls.descriptionCategory.reset();
      boolDescription = false;
    }
    return boolName && boolDescription;
  }

  /**
   * Reset fields of form with saved values
   */
  resetForm(): void {
    this.updateCategoryForm = this.fb.group({
      nameCategory: [this._saveCategory.name, Validators.required],
      descriptionCategory: [this._saveCategory.description, Validators.required]
    });
  }

}
