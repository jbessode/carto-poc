import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdateCategoryComponent} from './update-category.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {POIService} from "../../../services/poi.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ToastrModule} from "ngx-toastr";
import {of} from 'rxjs';

describe('UpdateCategoryComponent', () => {
  let component: UpdateCategoryComponent;
  let fixture: ComponentFixture<UpdateCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateCategoryComponent],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        ToastrModule.forRoot()
      ],
      providers: [POIService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCategoryComponent);
    component = fixture.componentInstance;
    component.categories = null;
    component.id = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should return False when form is empty", () => {
    expect(component.updateCategoryForm.valid).toBeFalsy();
  });

  it("should be Falsy when nameCategory field is empty", () => {
    let name = component.updateCategoryForm.controls['nameCategory'];
    expect(name.valid).toBeFalsy();
  });

  it("should be falsy when descriptionCategory field is empty", () => {
    let description = component.updateCategoryForm.controls['descriptionCategory'];
    expect(description.valid).toBeFalsy();
  });

  it("should be truthy when nameCategory field contains errors", () => {
    let errors: {};
    let name = component.updateCategoryForm.controls['nameCategory'];
    errors = name.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it("should be truthy when descriptionCategory field contains errors", () => {
    let errors: {};
    let description = component.updateCategoryForm.controls['descriptionCategory'];
    errors = description.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it("should be truthy when nameCategory length is less than two", () => {
    let errors: {};
    component.updateCategoryForm.controls['nameCategory'].setValue("N");
    let name = component.updateCategoryForm.controls['nameCategory'];
    errors = name.errors || {};
    expect(errors['minlength']).toBeTruthy();
  });

  it("should be truthy when descriptionCategory length is less than two", () => {
    let errors: {};
    component.updateCategoryForm.controls['descriptionCategory'].setValue("C")
    let description = component.updateCategoryForm.controls['descriptionCategory'];
    errors = description.errors || {};
    expect(errors['minlength']).toBeTruthy();
  });

  it("should reset form correctly", () => {
    component.updateCategoryForm.controls['nameCategory'].setValue("Natural parks");
    component.updateCategoryForm.controls['descriptionCategory'].setValue("Category which contains natural parks");
    component.updateCategoryForm.reset();
    expect(component.updateCategoryForm.valid).toBeFalsy();
  });

  it("should be falsy when one field is empty", () => {
    component.updateCategoryForm.controls['nameCategory'].setValue("Natural parks");
    expect(component.updateCategoryForm.valid).toBeFalsy();
  });

  it("should assign the input value to categories", () => {
    // Arrange
    component.categories = {};
    const categoriesInput = {
      "categories": [
        {
          "id": 9,
          "name": "Radars feux rouge europe",
          "description": "Radars qui flash lors du dépassement du feux par un véhicule"
        },
        {
          "id": 10,
          "name": "Radars fixe europe",
          "description": "Radars qui flash lors du dépassement de la vitesse autorisé par un véhicule"
        },
        {
          "id": 11,
          "name": "Police",
          "description": "Elements de Police"
        },
        {
          "id": 12,
          "name": "Pompier",
          "description": "Elements Pompiers"
        },
        {
          "id": 13,
          "name": "Etablissements Hopitaliers Ile de France",
          "description": "Etablissements hospitaliers en Ile de France"
        },
        {
          "id": 14,
          "name": "Etablissements d enseignement supérieur publics",
          "description": "Etablissements d enseignements supérieurs"
        },
        {
          "id": 15,
          "name": "Other",
          "description": "Category wich contains deleted layers and other layers without specified category"
        }]
    };
    component.categories = categoriesInput;

    // Assert
    expect(component.categories).toEqual(categoriesInput);
    expect(component.categories.categories.length).toBeGreaterThanOrEqual(7);
    expect(component.categories.categories[0].name.length).toBeGreaterThan(2);
    expect(component.categories.categories[0].name.length).toBeLessThan(50);
  });

  it("should assign the input value to nameCategoryToUpdate & descriptionCategoryToUpdate", () => {
    // Arrange
    component.nameCategoryToUpdate = '';
    component.descriptionCategoryToUpdate = '';

    const nameCategoryInput = 'Radars feux rouge europe'
    const descriptionCategoryInput = 'Radars qui flash lors du dépassement du feux par un véhicule'

    component.nameCategoryToUpdate = nameCategoryInput;
    component.descriptionCategoryToUpdate = descriptionCategoryInput;

    // Assert
    expect(component.nameCategoryToUpdate).toEqual(nameCategoryInput);
    expect(component.descriptionCategoryToUpdate).toEqual(descriptionCategoryInput);
  });

  it("should reset form", () => {
    // Arrange
    component.updateCategoryForm.controls['nameCategory'].setValue("Routes du désert");
    component.updateCategoryForm.controls['descriptionCategory'].setValue("Routes du désert du sahara");
    component.resetForm();

    // Assert
    expect(component.updateCategoryForm.controls['nameCategory'].value).toEqual(null);
    expect(component.updateCategoryForm.controls['descriptionCategory'].value).toEqual(null);
  });


});
