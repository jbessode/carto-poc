import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/user-account/service/user.service';
import {Role} from "../../../../models/roles";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  // inject service
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  /**
   * Check if user have token, so if it undefined
   */
  isAuth(){
   return localStorage.token !== undefined;
  }

  /**
   * Call service to logout current user
   */
  logout(){
    this.userService.logout();
  }

  /**
   * Check with service if current user is admin
   */
  isAdmin(): boolean {
    return this.userService.getUserRoles() === Role.ADMIN;
  }

  /**
   * Check with service if current user is editor
   */
  isEditor(): boolean {
    return this.userService.getUserRoles() === Role.EDITOR;
  }
}
