import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthResponse, DefaultService, UserInformation, UserInformationRequest, UserLoginRequest} from 'dist/openapi';
import {Observable} from 'rxjs';
import {Role} from '../../../models/roles';
import jwtDecode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isAuth = false;
  token: string;

  // Inject default service
  constructor(private api: DefaultService, private router: Router) {
  }

  /**
   * Get current role of user by decoding token
   */
  getUserRoles(): Role {
    let currentToken = localStorage.getItem('token');
    if (currentToken) {
      let decodedJWT: any = jwtDecode(currentToken);
      let userRoles = decodedJWT.groups;
      switch (userRoles[0].toLowerCase()) {
        case "user":
          return Role.USER;
        case "editor":
          return Role.EDITOR;
        case "admin":
          return Role.ADMIN;
      }
    }
  }

  /**
   * Call User API to sign up new user with given information
   * @param user information of new user
   */
  signUp(user: UserInformationRequest): Observable<UserInformation> {
    return this.api.userCreatePost(user);
  }

  /**
   * Call user API to login user with given information
   * @param user information of existing user
   */
  login(user: UserLoginRequest): Observable<AuthResponse> {
    return this.api.userLoginPost(user);
  }

  /**
   * Update current token
   * @param token new token
   */
  updateToken(token: string): void {
    this.token = token;
  }

  /**
   * Logout current user by deleting its token, and redirect it to home page
   */
  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  /**
   * Call user API to get user by ID
   * @param id if of given user
   */
  getUserById(id: number): Observable<any> {
    return this.api.userIdGet(id);
  }

  /**
   * Call user API to update given user
   * @param id id of user to update
   * @param user information of user to update
   */
  updateUser(id: number, user: UserInformationRequest): Observable<any> {
    return this.api.userUpdateIdPut(id, user);
  }

  /**
   * Call user API to get all users
   */
  getAllUsers(): Observable<any> {
    return this.api.userAllGet();
  }

  /**
   * Call user API to delete user by ID
   * @param id id of user to delete
   *
   */
  deleteUser(id: number): Observable<any> {
    return this.api.userDeleteIdDelete(id);
  }
}
