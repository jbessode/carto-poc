import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AccountAuthentificationComponent } from './components/account-authentification/account-authentification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccounLoginComponent } from './components/account-login/accounts-login.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { SearchUserAccountComponent } from './components/search-user-account/search-user-account.component';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  declarations: [
    AccountAuthentificationComponent,
    AccounLoginComponent,
    UserDetailComponent,
    SearchUserAccountComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    AppRoutingModule
  ]
})
export class UserAccountModule {
}
