import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthResponse, ResponseBody, UserLoginRequest} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-account-login',
  templateUrl: './accounts-login.component.html',
  styleUrls: ['./accounts-login.component.scss']
})
export class AccounLoginComponent implements OnInit {

  isValid: boolean;
  isSubmitted: boolean;
  user: UserLoginRequest;
  userForm: FormGroup;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router, private toast: ToastrService) {
    if (localStorage.token) {
      this.router.navigate(['/home']);
    }
  }


  /**
   * This method is called when accountsLogin component is initializing.
   * Create user formGroup
   */
  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  /**
   * Send login request as soon as the form is submitted,
   * and redirect user to home page
   */
  onSubmitForm(): void {
    if (!this.userForm.valid) {
      this.isValid = false;
      return;
    }
    const formvalue = this.userForm.value;
    this.user = {
      email: formvalue.email,
      password: formvalue.password
    };
    // @ts-ignore
    this.userService.login(this.user).subscribe({
      next: (res: ResponseBody) => {
        const token: AuthResponse = res.payload;
        this.isSubmitted = true;
        this.userService.isAuth = true;
        this.isValid = true;
        localStorage.setItem('token', token.token);
        this.router.navigate(['/home']);
      },
      error: er => {
        this.isSubmitted = true;
        this.isValid = false;
        this.toast.error('Error during account authentification ' + er.error.message, 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });
  }

}
