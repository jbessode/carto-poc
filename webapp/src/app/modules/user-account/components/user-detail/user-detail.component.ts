import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ResponseBody, UserInformation, UserInformationRequest} from 'dist/openapi';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../service/user.service';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  user: UserInformation;
  userCpy: UserInformationRequest;
  userFound: boolean = false;
  initialRole: string;
  @ViewChild('closebutton') closebutton;

  // Inject user & toast service
  constructor(private service: UserService,
              private routes: ActivatedRoute,
              private router: Router,
              private toast: ToastrService) {
  }

  /**
   * This method is called when userDetail component is initializing.
   * Call service toc get current user by ID
   */
  ngOnInit(): void {

    let targetUserId: number = parseInt(this.routes.snapshot.paramMap.get("id"));
    if (!targetUserId) {
      this.toast.error("Failed to get user id", 'Error', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
    }
    this.service.getUserById(targetUserId)
      .subscribe({
        next: (response: ResponseBody) => {
          let paylaod: UserInformationRequest = response.payload as UserInformationRequest;
          if (paylaod) {
            this.user = paylaod;
            this.initialRole = this.user.role;
            this.userCpy = paylaod;
            this.userFound = true;
          }
        },
        error: (error) => {
          this.toast.error(error, 'Error', {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });

        },
      });
  }

  /**
   * Send updated values of user as soon as the form is submitted,
   * and toast message
   */
  onSubmit(): void {
    // copy
    this.userCpy.email = this.user.email;
    this.userCpy.firstname = this.user.firstname;
    this.userCpy.lastname = this.user.lastname;
    this.userCpy.password = this.user.password;
    this.userCpy.role = this.user.role;

    let targetUserId: number = this.user.id;
    this.service
      .updateUser(targetUserId, this.userCpy)
      .subscribe({
        next: (response: ResponseBody) => {
          if (!response.success) {
            this.toast.error(response.message, 'Error', {
              timeOut: 4000,
              positionClass: 'toast-top-center'
            });
          } else {
            this.toast.success("User information updated", 'Success', {
              timeOut: 4000,
              positionClass: 'toast-top-center'
            });
          }
        },
        error: (error) => {
          this.toast.error(error.error.message, 'Error', {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });
        }
      });
  }

  /**
   * Call service to delete current user
   */
  deleteUser(): void {
    this.service.deleteUser(this.user.id).subscribe({
      next: _ => {
        this.toast.success("User " + this.user.firstname + " - " + this.user.lastname + ' has been deleted', 'Success', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.closebutton.nativeElement.click();
        this.router.navigate(['/accounts']);
      },
      error: _ => {
        this.toast.error(_.error.message, 'Error', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
        this.closebutton.nativeElement.click();
        return;
      }
    });
  }
}
