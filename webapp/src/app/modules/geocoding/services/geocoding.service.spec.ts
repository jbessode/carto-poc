import { TestBed, inject } from '@angular/core/testing';
import { GeocodingService } from './geocoding.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('GeocodingService', () => {
  let service: GeocodingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        GeocodingService]
    });
    service = TestBed.inject(GeocodingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
