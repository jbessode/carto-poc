import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GeoCodingDirectPayload, GeoCodingIndirectPayload} from 'dist/openapi';
import {ToastrService} from "ngx-toastr";
import {Feature, Map, View} from 'ol';
import olms from 'ol-mapbox-style';
import {Extent} from 'ol/extent';
import GeoJSON from 'ol/format/GeoJSON';
import {Geometry} from "ol/geom";
import VectorLayer from 'ol/layer/Vector';
import * as olProj from 'ol/proj';
import {Vector} from 'ol/source';
import {POINT_STYLE, POINT_STYLE_SELECTED} from 'src/app/helpers/constants';
import {GeoJsonPayload} from 'src/app/models/geocodingpayload';
import {GeocodingService} from 'src/app/modules/geocoding/services/geocoding.service';
import {environment} from 'src/environments/environment';
import {Utils} from "../../../helpers/Utils";
import {AppConfigService} from '../../config/appconfig.service';
import {StateService} from "../services/state.service";

@Component({
  selector: 'app-geocoding',
  templateUrl: './geocoding.component.html',
  styleUrls: ['./geocoding.component.scss']
})

export class GeocodingComponent implements OnInit {

  map: Map;
  directForm: FormGroup;
  inverseForm: FormGroup;
  lat: string;
  lon: string;
  geoDirectPayload: GeoCodingDirectPayload = {};
  geoInversePayload: GeoCodingIndirectPayload = {};
  response: GeoJsonPayload[] = [];
  poiSource: Vector;
  isDirectGeoCodeResearch = false;
  isReverseGeoCodeResearch = false;
  messageError = false;
  extentsPOI: Extent[];
  allFeaturesList: Feature<Geometry>[];
  state: any;

  // Inject geocoding & toast service
  constructor(private geocodingService: GeocodingService,
              private formBuilder: FormBuilder,
              private toastService: ToastrService,
              private stateService: StateService,
              private elementRef: ElementRef,
              private configService: AppConfigService) {
    this.poiSource = new Vector({
      format: new GeoJSON()
    });
  }


  ngOnInit(): void {
    this.state = this.stateService.state$.getValue() || {};
    let defaultDirectFormValue = '';
    let defaultInverseFormLongitudeValue = null;
    let defaultInverseFormLatitudeValue = null;
    if (this.state.directFormSaved !== undefined) {
      defaultDirectFormValue = this.state.directFormSaved.value['address'];
    }
    if (this.state.inverseFormSaved !== undefined) {
      defaultInverseFormLatitudeValue = this.state.inverseFormSaved.value['lat'];
      defaultInverseFormLongitudeValue = this.state.inverseFormSaved.value['lon'];
    }

    // init forms
    this.directForm = this.formBuilder.group({
      address: [defaultDirectFormValue, Validators.required]
    });
    this.inverseForm = this.formBuilder.group({
      lat: [defaultInverseFormLatitudeValue, Validators.required],
      lon: [defaultInverseFormLongitudeValue, Validators.required]
    });

    this.getSavedValues();
    this.initMap();
  }

  /**
   * Initialize map with mouse control & bottom layer from TileServer GL
   */
  initMap(): void {
    this.map = new Map({
      target: 'map',
      layers: [new VectorLayer({
        source: this.poiSource,
        style: (_) => POINT_STYLE,
        zIndex: 1001
      })],
      view: new View({
        projection: 'EPSG:3857',
        center: olProj.fromLonLat([1.5976721, 42.5422699]),
        zoom: 1,
        showFullExtent: true
      })
    });
    this.configService.getTileserverUrl().subscribe({
      next: response => {
        let url = response.TILESERVER_URL || `${window.location.protocol}//${window.location.hostname}:${environment.TILESERVER_DEFAULT_PORT}/styles/basic-preview/style.json`;
        olms(this.map, url);
      },
      error: err => console.error(err)
    });
  }

  /**
   * Get saved values into behaviorSubject object (field in service
   */
  getSavedValues(): void {
    // get previous geocode response
    if (this.state.responseSaved) {
      this.response = this.state.responseSaved;
    }

    // get type of research saved
    this.isDirectGeoCodeResearch = this.state.isDirectGeoCodeResearchSaved;
    this.isReverseGeoCodeResearch = this.state.isReverseGeoCodeResearchSaved;

    // get map properties
    if (this.state.allfeaturesListSaved && this.state.extentsPOISaved && this.state.poiSourceSaved) {
      this.poiSource = this.state.poiSourceSaved;
      this.allFeaturesList = this.state.allfeaturesListSaved;
      this.extentsPOI = this.state.extentsPOISaved;
    }

  }

  /**
   * Send direct geocoding research as soon as the form is submitted
   */
  onSubmitGeoDirect(): void {
    if (this.directForm.invalid) {
      this.toastService.warning('Input error, address is invalid', 'Direct Geocoding', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    if (!Utils.noWhitespaceValidator(this.directForm.value['address'])) {
      this.toastService.warning('Input error, address cannot be blank', 'Direct Geocoding', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    this.isReverseGeoCodeResearch = false;
    this.isDirectGeoCodeResearch = true;
    this.geoDirectPayload.name = this.directForm.value['address'];
    this.geocodingService.geocodingDirect(this.geoDirectPayload).subscribe({
      next: (val: any) => {
        const features = val[Object.keys(val)[2]];
        const arrFeatures = Array.from(features);

        if (arrFeatures.length === 0) {
          this.toastService.warning('No result for address ' + this.geoDirectPayload.name, 'Direct Geocoding', {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });
          return;
        }
        this.response = val['features'];
        this._showPoint(val);

        // save values
        this.state.isDirectGeoCodeResearchSaved = this.isDirectGeoCodeResearch;
        this.state.isReverseGeoCodeResearchSaved = this.isReverseGeoCodeResearch;
        this.state.directFormSaved = this.directForm;
        this.state.responseSaved = this.response;
        this.stateService.state$.next(this.state);
      },
      error: _ => {
        this.toastService.error('An error occurred while requesting direct geocoding : ' + _.error.message, 'Direct Geocoding', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    });

  }

  /**
   * Send reverse geocoding research as soon as the form is submitted
   */
  onSubmitGeoInverse(): void {
    if (this.inverseForm.invalid) {
      this.toastService.warning('Input error, Latitude or Longitude is invalid', 'Reverse Geocoding', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    if (!Utils.noWhitespaceValidator(this.inverseForm.value['lon']) || !Utils.noWhitespaceValidator(this.inverseForm.value['lat'])) {
      this.toastService.warning('Input error, latitude or longitude cannot be blank', 'Reverse Geocoding', {
        timeOut: 4000,
        positionClass: 'toast-top-center'
      });
      return;
    }

    this.isReverseGeoCodeResearch = true;
    this.isDirectGeoCodeResearch = false;
    this.geoInversePayload.lat = this.inverseForm.value['lat'];
    this.geoInversePayload.lon = this.inverseForm.value['lon'];
    this.geocodingService.geocodingInverse(this.geoInversePayload).subscribe({
      next: (val: any) => {
        const features = val[Object.keys(val)[2]];
        const arrFeatures = Array.from(features);
        if (arrFeatures.length === 0) {
          this.toastService.warning('No result for address ' + this.geoDirectPayload.name, 'Reverse Geocoding', {
            timeOut: 4000,
            positionClass: 'toast-top-center'
          });
          return;
        }
        this.response = val['features'];
        this._showPoint(val);

        // save values
        this.state.isDirectGeoCodeResearchSaved = this.isDirectGeoCodeResearch;
        this.state.isReverseGeoCodeResearchSaved = this.isReverseGeoCodeResearch;
        this.state.inverseFormSaved = this.inverseForm;
        this.state.responseSaved = this.response;
        this.stateService.state$.next(this.state);
      },
      error: _ => {
        this.toastService.error('An error occurred while requesting reverse geocoding', 'Reverse Geocoding', {
          timeOut: 4000,
          positionClass: 'toast-top-center'
        });
      }
    })

  }

  /**
   * Create action when user clicking on row in result table
   */
  clickOnRow(index: number): void {
    this.allFeaturesList.forEach(elt => elt.setStyle(POINT_STYLE));
    this.allFeaturesList[index].setStyle(POINT_STYLE_SELECTED);
    const viewMap = this.map.getView();
    viewMap.setCenter(olProj.fromLonLat(this.response[index].geometry.coordinates));
    viewMap.fit(this.extentsPOI[index], {duration: 1000});
    viewMap.setZoom(16);
  }


  /**
   * Read features and displays POIs in map
   */
  private _showPoint(geoJson: any): void {
    this.poiSource.refresh();
    const format = new GeoJSON();
    this.allFeaturesList = format.readFeatures(geoJson, {featureProjection: 'EPSG:3857'});
    this.poiSource.addFeatures(this.allFeaturesList);
    this.map.getView().setCenter(olProj.fromLonLat(this.response[0].geometry.coordinates));

    this.extentsPOI = this.allFeaturesList
      .map(f => f.getGeometry())
      .map(g => g.getExtent());

    this.map.updateSize();
    //  save values
    this.state.poiSourceSaved = this.poiSource;
    this.state.allfeaturesListSaved = this.allFeaturesList;
    this.state.extentsPOISaved = this.extentsPOI;
    this.stateService.state$.next(this.state);
  }

}
