import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GeocodingComponent } from './geocoding/components/geocoding.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { AccounLoginComponent } from './user-account/components/account-login/accounts-login.component';
import { AccountAuthentificationComponent } from './user-account/components/account-authentification/account-authentification.component';
import { ManagementPoiComponent } from './management-poi/components/poi/management-poi.component';
import { UserDetailComponent } from './user-account/components/user-detail/user-detail.component';
import { SearchUserAccountComponent } from './user-account/components/search-user-account/search-user-account.component';
import { AuthentificationGuard } from '../helpers/authentification.guard';


const routes: Routes = [
  {
    path: 'login', component: AccounLoginComponent
  },
  {
    path: 'auth', component: AccountAuthentificationComponent
  },
  {
    path: 'home', component: ManagementPoiComponent, canActivate: [AuthentificationGuard],
  },
  {
    path: 'accounts', component: SearchUserAccountComponent, canActivate: [AuthentificationGuard]
  },
  {
    path: 'account/:id', component: UserDetailComponent, canActivate: [AuthentificationGuard]
  },
  {
    path: 'geocoding', component: GeocodingComponent, canActivate: [AuthentificationGuard]
  },
  { path: '', redirectTo: '/home', pathMatch: 'full', canActivate: [AuthentificationGuard] },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
