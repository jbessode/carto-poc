import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserService } from '../user-account/service/user.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  // Inject user service
  constructor(private userService: UserService, private router: Router) {
  }

  /**
   * Intercept http request and add token to Authorization header
   * @param req instance of request
   * @param next stream of HttpEvents
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!localStorage.getItem('token')) {
      return next.handle(req);
    }

    const req1 = req.clone({
      headers: req.headers.set('Authorization', `Bearer ` + localStorage.getItem('token')),
    });

    return next.handle(req1).pipe(catchError((err: HttpErrorResponse) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          localStorage.removeItem('token');
          this.router.navigate(['/login']);
        }
        if (err.status === 403) {
          this.router.navigate(['home']);
        }
      }
      return throwError(err);
    }));
  }

}
