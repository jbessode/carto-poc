![pipeline](https://gitlab.com/cracs_Airbus/ngms/carto-poc/badges/master/pipeline.svg?style=flat)

# Requirements

Container registry login

```
docker login registry.gitlab.com/cracs_airbus/ngms -u cracs -p sb1HzR-X7jc_DaYsMDzQ
```

Container network host

```
docker network create ngms-network
```

Install openapi-cli (maybe need sudo)

```
npm install -g @openapitools/openapi-generator-cli
```

# Dockers

## Run dev local database

In a new terminal, go to the root of project and run following command to launch database docker

```
docker run --rm --pull always -p 6543:5432 \
 -e POSTGRES_DB=ngms \
 -e POSTGRES_USER=root \
 -e POSTGRES_PASSWORD=root \
 --name postgis-ngms \
 --memory="2G" \
 --cpus=1.0 \
 --log-driver=gelf \
 --log-opt gelf-address=udp://127.0.0.1:12201 \
 --network=ngms-network registry.gitlab.com/cracs_airbus/ngms/dockerfiles/postgis
```

# Run local nominatim

In a new terminal, go to the root of project and run following command to launch nominatim docker

```
docker run --rm --pull always -p 18080:15030 \
 -e DB_USER=root \
 -e DB_PASS=root \
 -e DB_NAME=nominatim \
 -e DB_HOST=postgis-ngms \
 -e DB_PORT=5432 \
 -v $(pwd)/data:/srv/data \
 --memory="2G" \
 --cpus=1.0 \
 --network=ngms-network \
 --log-driver=gelf \
 --log-opt gelf-address=udp://127.0.0.1:12201 \
 --name nominatim-ngms registry.gitlab.com/cracs_airbus/ngms/dockerfiles/nominatim
```

# Run dockerized carto-poc

In a new terminal, after run an docker of database and nominatim

```
docker run --rm --pull always -p 15080:15080 \
 -e QUARKUS_DATASOURCE_JDBC_URL=jdbc:postgresql://postgis-ngms:5432/ngms \
 -e GEOCODING_URL=http://172.17.0.1:15030 \
 -e TILESERVER_URL=http://172.17.0.1:15020/styles/basic-preview/style.json \
 -e GEOSERVER_URL=http://172.17.0.1:8080/geoserver/ngms/wms \
 --network=ngms-network \
 --memory="2G" \
 --cpus=1.0 \
 --log-driver=gelf \
 --log-opt gelf-address=udp://127.0.0.1:12201 \
 --name carto-poc-ngms registry.gitlab.com/cracs_airbus/ngms/carto-poc
```

# Run dockerized tileservergl

In a new terminal, go to the folder witch contains your config.json, styles, fonts elements

```
docker run --rm --pull always -p 15020:15020 \
 --memory="2G" \
 --cpus=1.0 \
 --network=ngms-network \
 -v $(pwd)/data:/data \
 --name tileserver-ngms \
 registry.gitlab.com/cracs_airbus/ngms/dockerfiles/tileserver
```

# Run dockerized geoserver

In a new terminal, after run a database instance

```
docker run --rm --pull always -p 8080:8080 \
 -e POSTGRES_USER=root \
 -e POSTGRES_PASS=root \
 -e POSTGRES_DB=ngms \
 -e HOST=postgis-ngms \
 -e POSTGRES_PORT=5432 \
 -e ADMIN_LOGIN=admin \
 -e ADMIN_PASS=geoserver \
 --network=ngms-network \
 --name geoserver-ngms registry.gitlab.com/cracs_airbus/ngms/dockerfiles/geoserver
```

# Dependencies

Go to the root of repository

## Install all dependencies

### Linux

```
./mvnw -Dui.deps compile
```

### Windows

```
mvnw.cmd -Dui.deps compile
```

# Run application

## Backend

Go to root of repository and execute to run quarkus

### Linux

```
./mvnw clean compile quarkus:dev
```

### Windows

```
mvnw.cmd clean compile quarkus:dev
```

## Frontend

Go to **webapp/** folder of repository and execute to run angular

```
npm install
npm run generate:api
npm start
```

# Package app to release

## Build jar of api and front

### Linux

```
./mvnw package -Dui
```

### Windows

```
mvnw.cmd package -Dui
```
