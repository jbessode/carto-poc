package fr.ngms.cartopoc.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.user.AuthResponse;
import fr.ngms.cartopoc.models.user.UserListResponse;
import fr.ngms.cartopoc.test.helpers.DatabaseCleaner;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.*;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collector;

import static io.restassured.RestAssured.given;
import static io.smallrye.common.constraint.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;


@QuarkusTest
@Tag("integration")
class UserTest {

    private static String tokenAdmin;
    private static String tokenEditor;
    private static String tokenUser;

    @BeforeAll
    public static void setup() throws SQLException {
        DatabaseCleaner.clean();
        DatabaseCleaner.setup();

        var authAdminResponse = given().
                port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class))
                .body("{\n" +
                        "    \"email\" : \"admin@admin\",\n" +
                        "    \"password\": \"admin\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());

        var authEditorResponse = given().
                port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class))
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());

        var authUserResponse = given().
                port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class))
                .body("{\n" +
                        "    \"email\" : \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());

        tokenAdmin = authAdminResponse.payload.getToken();
        tokenEditor = authEditorResponse.payload.getToken();
        tokenUser = authUserResponse.payload.getToken();
    }

    @AfterAll
    public static void clean() throws SQLException {
        DatabaseCleaner.clean();
    }


    @Test
    @Tag("FindAll")
    void shouldReturnOKWhenFindAllUsers() {
        var users = given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when().get("/user/all")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(userListResponse());
        assertTrue(users.payload.getUserInformations().size() > 0);
    }


    @Test
    @Tag("FindByID")
    void shouldReturnOKWhenFindSeveralUsersByID() {
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .get("/user/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .get("/user/2")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .get("/user/3")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("FindByID")
    void shouldReturnForbiddenWhenFindUserByIDWithoutSufficientAccess() {
        given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .get("/user/1")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenUser)
                .when()
                .contentType(ContentType.JSON)
                .get("/user/2")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();

    }

    @Test
    @Tag("FindByID")
    void shouldReturnNotFoundWhenFindUnknownUserByID() {
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .get("/user/99")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Login")
    void shouldReturnOKWhenPerformsLogin() {
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"admin@admin\",\n" +
                        "    \"password\": \"admin\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenUser)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Login")
    void shouldReturnOKWhenPerformsLoginAfterSignUp() {
        var authResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"nicolas.dupont@outlook.eu\",\n" +
                        "  \"firstname\": \"nicolas\",\n" +
                        "  \"lastname\": \"dupont\",\n" +
                        "  \"password\": \"airbus23\",\n" +
                        "  \"role\": \"USER\"\n" +
                        "}")
                .post("/user/create")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());
        assertAll(() -> {
            assertTrue(authResponse.sucess);
            assertNull(authResponse.message);
            assertNull(authResponse.payload);
        });

        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"nicolas.dupont@outlook.eu\",\n" +
                        "    \"password\": \"airbus23\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Login")
    void shouldReturnBadRequestWhenPerformsLoginAndBodyIsNull() {
        given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .body("null")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Login")
    void shouldReturnBadRequestWhenPerformsLoginAndBodyIsEmpty() {
        given()
                .header("Authorization", "Bearer " + tokenUser)
                .when()
                .contentType(ContentType.JSON)
                .body("")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Login")
    void shouldReturnBadRequestWhenPerformsLoginAndPasswordFieldIsMissing() {
        given()
                .header("Authorization", "Bearer " + tokenUser)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"test@test\",\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Login")
    void shouldReturnUnauthorizedWhenPerformsLoginAndUserIsUnknown() {
        var authResponse = given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"martin.dupont@outlook.fr\",\n" +
                        "    \"password\": \"greatSpacy88\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());

        assertAll(() -> {
            assertFalse(authResponse.sucess);
            assertNull(authResponse.payload);
            assertEquals("Unknown user with email:  martin.dupont@outlook.fr", authResponse.message);
        });
    }


    @Test
    @Tag("Login")
    void shouldReturnUnauthorizedWhenPerformsLoginAndPasswordIsIncorrect() {
        var authResponse = given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"admin@admin\",\n" +
                        "    \"password\": \"superman\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());
        assertAll(() -> {
            assertFalse(authResponse.sucess);
            assertNull(authResponse.payload);
            assertEquals("Unauthorized operation", authResponse.message);
        });
    }

    @Test
    @Tag("Login")
    void shouldReturnBadRequestWhenPerformsLoginAndEmailIsEmpty() {
        var authResponse = given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"\",\n" +
                        "    \"password\": \"superman\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());
        assertAll(() -> {
            assertFalse(authResponse.sucess);
            assertNull(authResponse.payload);
        });
    }

    @Test
    @Tag("Login")
    void shouldReturnBadRequestWhenPerformsLoginAndPasswordIsEmpty() {
        var authResponse = given()
                .header("Authorization", "Bearer " + tokenEditor)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"email\": \"admin@admin\",\n" +
                        "    \"password\": \"\"\n" +
                        "}")
                .post("/user/login")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());
        assertAll(() -> {
            assertFalse(authResponse.sucess);
            assertNull(authResponse.payload);
        });
    }

    @Test
    @Tag("SignUp")
    void shouldReturnOKWhenPerformsSignUp() {
        var authResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"marc.elevers@outlook.de\",\n" +
                        "  \"firstname\": \"marc\",\n" +
                        "  \"lastname\": \"elevers\",\n" +
                        "  \"password\": \"airbus87\",\n" +
                        "  \"role\": \"USER\"\n" +
                        "}")
                .post("/user/create")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());

        assertAll(() -> {
            assertTrue(authResponse.sucess);
            assertNull(authResponse.message);
            assertNull(authResponse.payload);
        });
    }

    @Test
    @Tag("SignUp")
    void shouldReturnBadRequestWhenPerformsSignUpAndBodyIsNull() {
        given()
                .when()
                .contentType(ContentType.JSON)
                .body("null")
                .post("/user/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("SignUp")
    void shouldReturnBadRequestWhenPerformsSignUpAndBodyIsIncorrect() {
        given()
                .when()
                .contentType(ContentType.JSON)
                .body("qughieugiveligbirtbhnut")
                .post("/user/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("SignUp")
    void shouldReturnBadRequestWhenPerformsSignUpAndValuesAreEmpty() {
        var authResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"marc.elevers@outlook.de\",\n" +
                        "  \"firstname\": \"\",\n" + // empty
                        "  \"lastname\": \"elevers\",\n" +
                        "  \"password\": \"\",\n" + // empty
                        "  \"role\": \"USER\"\n" +
                        "}")
                .post("/user/create")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());

        assertAll(() -> {
            assertFalse(authResponse.sucess);
            assertEquals("fields: password firstname missing", authResponse.message);
            assertNull(authResponse.payload);
        });
    }

    @Test
    @Tag("Long signUp")
    void shouldReturnOKWhenSignUpTenUserThenDeleteThem() {
        for (var i = 0; i < 10; i++) {
            var rand = new SecureRandom();
            var max = 999;
            var min = 10;
            var generatedInteger = rand.nextInt((max - min) + 1) + min;


            given()
                    .header("Authorization", "Bearer " + tokenAdmin)
                    .when()
                    .contentType(ContentType.JSON)
                    .body("{\n" +
                            "    \"email\": \"" + generatedInteger + "." + generatedInteger + "@airbus.eu\",\n" +
                            "    \"firstname\": \"" + generatedInteger + "\",\n" +
                            "    \"lastname\": \"" + generatedInteger + "\",\n" +
                            "    \"password\": \"airbus" + generatedInteger + "\",\n" +
                            "    \"role\": \"USER\"\n" +
                            "}")
                    .post("/user/create")
                    .then()
                    .statusCode(Response.Status.OK.getStatusCode())
                    .assertThat();
        }

        var lastTenUsers = given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when().get("/user/all")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(userListResponse()).payload.getUserInformations().stream().collect(lastN(10));

        for (var user : lastTenUsers) {
            given()
                    .when()
                    .header("Authorization", "Bearer " + tokenAdmin)
                    .contentType(ContentType.JSON)
                    .delete("/user/delete/" + user.getId())
                    .then()
                    .statusCode(Response.Status.OK.getStatusCode())
                    .assertThat();
        }
    }


    @Test
    @Tag("Update")
    void shouldReturnOKWhenPerformsUpdateOfAnUser() {
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"gpioli0@live.com\",\n" + // value updated
                        "  \"firstname\": \"Gerta\",\n" +
                        "  \"lastname\": \"Pioli\",\n" +
                        "  \"password\": \"airbus87\",\n" + // value updated
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Update")
    void shouldReturnOKWhenPerformsUpdateSeveralUsers() {
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"gpioli0@live.com\",\n" + // value updated
                        "  \"firstname\": \"Gerta\",\n" +
                        "  \"lastname\": \"Pioli\",\n" +
                        "  \"password\": \"airbus87\",\n" + // value updated
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"jpenzer1@netvibes.com\",\n" +
                        "  \"firstname\": \"Jeremias\",\n" +
                        "  \"lastname\": \"Penzer\",\n" +
                        "  \"password\": \"airbus14\",\n" + // value updated
                        "  \"role\": \"EDITOR\"\n" + // value updated
                        "}")
                .put("/user/update/2")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"csimons2@nifty.com\",\n" +
                        "  \"firstname\": \"Cynthy\",\n" +
                        "  \"lastname\": \"Bernard\",\n" + // value updated
                        "  \"password\": \"airbus10\",\n" + // value updated
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/3")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Update")
    void shouldReturnExpectationFailedWhenUpdateBadExpectedUser() {
        // Expected user is Gerta Pioli
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"jpenzer1@netvibes.com\",\n" +
                        "  \"firstname\": \"Jeremias\",\n" +
                        "  \"lastname\": \"Penzer\",\n" +
                        "  \"password\": \"airbus14\",\n" + // value updated
                        "  \"role\": \"EDITOR\"\n" + // value updated
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Update")
    void shouldReturnUnauthorizedWhenPerformsUpdateWithoutToken() {
        given()
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"gpioli0@live.com\",\n" + // value updated
                        "  \"firstname\": \"Gerta\",\n" +
                        "  \"lastname\": \"Pioli\",\n" +
                        "  \"password\": \"airbus87\",\n" + // value updated
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Update")
    void shouldReturnForbiddenWhenPerformsUpdateWithoutSufficientAccess() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenEditor)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"gpioli0@live.com\",\n" + // value updated
                        "  \"firstname\": \"Gerta\",\n" +
                        "  \"lastname\": \"Pioli\",\n" +
                        "  \"password\": \"airbus87\",\n" + // value updated
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();

        given()
                .when()
                .header("Authorization", "Bearer " + tokenUser)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"gpioli0@live.com\",\n" + // value updated
                        "  \"firstname\": \"Gerta\",\n" +
                        "  \"lastname\": \"Pioli\",\n" +
                        "  \"password\": \"airbus87\",\n" + // value updated
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Update")
    void shouldReturnBadRequestWhenPerformsUpdateAndBodyIsNull() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenAdmin)
                .contentType(ContentType.JSON)
                .body("null")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Update")
    void shouldReturnBadRequestWhenPerformsUpdateAndBodyContainsTokenSoIncorrect() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenAdmin)
                .contentType(ContentType.JSON)
                .body("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJuZ21zIiwic3ViIjoiYWRtaW5" +
                        "AYWRtaW4iLCJpYXQiOjE2MTQ1OTgxMjAsImV4cCI6MTYxNDYwMTcyMCwiZ3JvdXBzIjpbIkFETUlOIl0sImp0aSI6IjY0ZWNhNTBiLTFl" +
                        "MzMtNDVmZC04MTkwLWI0ZDQzN2NjYmNmNCJ9.LYfKoLsyzfspS9ENJjVMI9uTfJUoasN6AIGbvQpsGJv8xOjwykP-PrmNldhtx5Nd3ocLGj9p6" +
                        "0f8lTDHtGeHwwoidWZ1bplV8C8EX0dtbu4QmWVJXZkzw0CThujCrGyNjAfHUbYdalPoeNMV7-rCkpy4S9rFPK-e_RDePETWqWVsWaeq6_QdBdUylp" +
                        "gQOM8g5VPYhbG3vRkPfRihht1-zXciftvdfizuSAlz4nC3xYAUrQPLDgPC73SQBVFGglqKDfy0PRkySUkIViNdVGmVdcjhu3EkPdcmneAYVg_6J5HTIzd" +
                        "jRosK2NEa1ZUDiqW_HUhnd74i1qfQWwoATUUBJA")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Update")
    void shouldReturnBadRequestWhenPerformsUpdateAndValuesAreEmpty() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenAdmin)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"gpioli0@live.com\",\n" +
                        "  \"firstname\": \"\",\n" + // value empty
                        "  \"lastname\": \"Pioli\",\n" +
                        "  \"password\": \"\",\n" + // value empty
                        "  \"role\": \"USER\"\n" +
                        "}")
                .put("/user/update/1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Long update")
    void shouldReturnOKWhenUpdateAUserALot() {
        for (var i = 0; i < 30; i++) {
            var rand = new SecureRandom();
            var max = 999;
            var min = 10;
            var generatedPassword = rand.nextInt((max - min) + 1) + min;

            given()
                    .header("Authorization", "Bearer " + tokenAdmin)
                    .when()
                    .contentType(ContentType.JSON)
                    .body("{\n" +
                            "  \"email\": \"gpioli0@live.com\",\n" + // value updated
                            "  \"firstname\": \"Gerta\",\n" +
                            "  \"lastname\": \"Pioli\",\n" +
                            "  \"password\": \"" + generatedPassword + "\",\n" + // value updated
                            "  \"role\": \"USER\"\n" +
                            "}")
                    .put("/user/update/1")
                    .then()
                    .statusCode(Response.Status.OK.getStatusCode())
                    .assertThat();
        }
    }

    @Test
    @Tag("Update")
    void shouldReturnUnauthorizedWhenPerformsUpdateWithUnknownUser() {
        given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"email\": \"unknown1@netvibes.com\",\n" +
                        "  \"firstname\": \"Unknown\",\n" +
                        "  \"lastname\": \"Unknown\",\n" +
                        "  \"password\": \"unknown14\",\n" +
                        "  \"role\": \"EDITOR\"\n" +
                        "}")
                .put("/user/update/99")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Delete")
    void shouldReturnOKWhenDeleteUserByID() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenAdmin)
                .contentType(ContentType.JSON)
                .delete("/user/delete/8")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Delete")
    void shouldReturnUnauthorizedWhenDeleteUserByIDWithoutToken() {
        given()
                .when()
                .contentType(ContentType.JSON)
                .delete("/user/delete/8")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                .assertThat();
    }


    @Test
    @Tag("Delete")
    void shouldReturnUnauthorizedWhenTryToDeleteUnknownUser() {
        var response = given()
                .header("Authorization", "Bearer " + tokenAdmin)
                .when()
                .contentType(ContentType.JSON)
                .delete("/user/delete/45")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                .assertThat()
                .extract()
                .as(authResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertNull(response.payload);
            assertEquals("Unknown user with id: 45", response.message);
        });
    }

    @Test
    @Tag("Delete")
    void shouldReturnForbiddenWhenDeleteUserByIDWithoutSufficientAccess() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenEditor)
                .contentType(ContentType.JSON)
                .delete("/user/delete/8")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();

        given()
                .when()
                .header("Authorization", "Bearer " + tokenUser)
                .contentType(ContentType.JSON)
                .delete("/user/delete/8")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Delete")
    void shouldReturnBadRequestWhenDeleteUserByIDWithNegativeID() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenAdmin)
                .contentType(ContentType.JSON)
                .delete("/user/delete/-1")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Tag("Delete")
    @Timeout(100)
    void shouldReturnOKWhenDeleteUserWithTimeOut() {
        given()
                .when()
                .header("Authorization", "Bearer " + tokenAdmin)
                .contentType(ContentType.JSON)
                .delete("/user/delete/9")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }

    @NotNull
    static TypeRef<AuthResponseResponse> authResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    static class AuthResponseResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        AuthResponse payload;
    }

    @NotNull
    static TypeRef<UserListResponseResponse> userListResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    static class UserListResponseResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        UserListResponse payload;
    }

    public static <T> Collector<T, ?, List<T>> lastN(int n) {
        return Collector.<T, Deque<T>, List<T>>of(ArrayDeque::new, (acc, t) -> {
            if (acc.size() == n) {
                acc.pollFirst();
            }
            acc.add(t);
        }, (acc1, acc2) -> {
            while (acc2.size() < n && !acc1.isEmpty()) {
                acc2.addFirst(acc1.pollLast());
            }
            return acc2;
        }, ArrayList::new);
    }

}