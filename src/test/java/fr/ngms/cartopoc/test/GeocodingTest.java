package fr.ngms.cartopoc.test;

import fr.ngms.cartopoc.test.helpers.DatabaseCleaner;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

import static io.restassured.RestAssured.given;

@QuarkusTest
@Tag("integration")
class GeocodingTest {
  private static String token;

  @BeforeAll
  public static void setup() throws SQLException {
    DatabaseCleaner.clean();
    DatabaseCleaner.setup();
    var authResponse = given().
            port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class))
            .body("{\n" +
                    "    \"email\" : \"admin@admin\",\n" +
                    "    \"password\": \"admin\"\n" +
                    "}")
            .contentType(ContentType.JSON)
            .post("/user/login")
            .then()
            .statusCode(Response.Status.OK.getStatusCode())
            .extract()
            .as(authResponse());
    token = authResponse.payload.getToken();
  }

  @Test
  void testIsAuthenticateUserDirectGeocode(){
    given()
            .when()
            .body("{\n" +
                    "     \"name\": \"string\",\n" +
                    "     \"payload\": \"string\"" +
            "}")
            .contentType(ContentType.JSON)
            .post("/geocode/direct")
            .then()
            .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
  }
  @Test
  void testIsAuthenticateUserIndirectDirectGeocode(){
    given()
            .when()
            .body("{\n" +
                    "     \"lat\": \"string\",\n" +
                    "     \"long\": \"string\"" +
                    "}")
            .contentType(ContentType.JSON)
            .post("/geocode/indirect")
            .then()
            .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
  }

  @Test
  void shouldInvalidIncorrectJson() {
    given()
            .when()
            .header("Authorization", "Bearer " + token)
            .body("{\n" +
                    "     \"egggg\": \"string\",\n" +
                    "     \"lorgdrgng\": \"string\"" +
                    "}")
            .contentType(ContentType.JSON)
            .post("/geocode/direct")
            .then()
            .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode());
    given()
            .when()
            .header("Authorization", "Bearer " + token)
            .body("null")
            .contentType(ContentType.JSON)
            .post("/geocode/indirect")
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    given()
            .when()
            .header("Authorization", "Bearer " + token)
            .body("null")
            .contentType(ContentType.JSON)
            .post("/geocode/indirect")
            .then()
            .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    given()
            .when()
            .header("Authorization", "Bearer " + token)
            .body("{\n" +
                    "     \"egggg\": \"string\",\n" +
                    "     \"lorgdrgng\": \"string\"" +
                    "}")
            .contentType(ContentType.JSON)
            .post("/geocode/indirect")
            .then()
            .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode());
  }
  @NotNull
  static TypeRef<CategoryControllerTest.AuthResponseResponse> authResponse() {
    return new TypeRef<>() {
      // Kept empty on purpose
    };
  }

}
