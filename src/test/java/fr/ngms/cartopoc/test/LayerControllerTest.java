package fr.ngms.cartopoc.test;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload;
import fr.ngms.cartopoc.models.responses.geojson.LayerGeoJson;
import fr.ngms.cartopoc.models.user.AuthResponse;
import fr.ngms.cartopoc.test.helpers.DatabaseCleaner;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.*;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
@Tag("integration")
class LayerControllerTest {

    private static String token;

    @BeforeAll
    public static void setup() throws SQLException {
        DatabaseCleaner.clean();
        DatabaseCleaner.setup();
        var authResponse = given()
                .port(ConfigProvider.getConfig()
                        .getValue("quarkus.http.test-port", Integer.class))
                .body("{\n" +
                "    \"email\" : \"admin@admin\",\n" +
                "    \"password\": \"admin\"\n" +
                "}")
                .contentType(ContentType.JSON).post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        token = authResponse.payload.getToken();
    }

    @AfterAll
    public static void clean() throws SQLException {
        DatabaseCleaner.clean();
    }

    @Test
    void shouldReturnOKWhenFindSeveralLayersByID() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/layers/2")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .get("/layers/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturnBadRequestWhenIDisNegative() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/layers/-5")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat()
                .body(is("Id of layer cannot be negative"));
    }

    @Test
    void shouldFindLayerWhenFindById() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/layers/5")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(layerInformationResponse());

        assertAll(() -> {
            assertTrue(response.sucess);
            assertNull(response.message);
            assertEquals(5, response.payload.getId());
            assertEquals("layer_5_police", response.payload.getName());
            assertEquals("couche des pois de la police", response.payload.getDescription());
        });
    }

    @Test
    void shouldReturnMessageWhenTryToFindUnknownLayer() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/layers/28")
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .assertThat()
                .extract()
                .as(layerInformationResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertEquals("Layer with id (28) not exist", response.message);
            assertNull(response.payload);
        });
    }

    @Test
    void shouldReturnOKWhenFindPOIOfLayerByID() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/layers/2/pois")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(layerGeoJsonResponse());

        var features = response.payload.getFeatures();
        var properties = response.payload.getProperties();

        assertAll(() -> {
            assertTrue(response.sucess);
            assertNull(response.message);

            // Features layer
            assertEquals(1, features.size());
            assertEquals(List.of(6.00, 4.00), features.get(0).getGeometry().get("coordinates"));
            assertEquals("2", features.get(0).getProperties().get("id"));
            assertEquals("camera_zone_2", features.get(0).getProperties().get("name"));

            // Properties layer
            assertEquals("layer_2_camera_thermique", properties.get("name"));
            assertEquals("couche des camera thermique", properties.get("description"));
            assertEquals("2", properties.get("id"));
        });
    }


    @Test
    void shouldReturnOKWhenUpdateLayer() {
        var layer = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"layer_6_sapeurs_pompiers\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(basicLayerPayloadResponse());


        assertAll(() -> {
            assertNull(layer.message);
            assertTrue(layer.sucess);
            assertEquals(1, layer.payload.getLayers().size());
            assertEquals("layer_6_sapeurs_pompiers", layer.payload.getLayers().get(0).getName());
            assertEquals("couche des points d'intérêts pour les sapeurs pompiers", layer.payload
                    .getLayers()
                    .get(0)
                    .getDescription());
        });
    }


    @Test
    void shouldReturnMessageWhenTryToUpdateUnknownLayer() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche de test\",\n" +
                        "  \"name\": \"layer_17_none\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"test\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/17")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(layerInformationResponse());

        assertAll(() -> {
            assertNull(response.payload);
            assertFalse(response.sucess);
            assertEquals("Layer with id (17) not exist", response.message);
        });
    }


    @Test
    void shouldReturnOKWhenUpdateALayerALot() {
        for (var i = 0; i < 100; i++) {
            given()
                    .header("Authorization", "Bearer " + token)
                    .when()
                    .contentType(ContentType.JSON)
                    .body("{\n" +
                            "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                            "  \"name\": \"layer_6_sapeurs_pompiers\",\n" +
                            "  \"properties\": {\n" +
                            "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                            "  } \n" +
                            "}")
                    .put("/layers/update/6")
                    .then()
                    .statusCode(Response.Status.OK.getStatusCode())
                    .assertThat();
        }
    }


    @Test
    void shouldReturnUnsupportedMediaTypeErrorWhenFormatIsNotJson() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.TEXT)
                .body("name = Caserne")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.UNSUPPORTED_MEDIA_TYPE.getStatusCode())
                .assertThat();
    }


    @Test
    void shouldReturnBadRequestWhenJsonIsInvalid() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    {\"\"}\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"layer_6_sapeurs_pompiers\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturnBadRequestWhenUpdateLayerWithTooLongName() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"llllllllllllllayeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeer_66666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666_sapeeeeeeeeurs_pommmmmmmmmmmmmpiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiers\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }


    @Test
    void shouldReturnOKWhenDeleteLayer() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/layers/delete/8")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat()
                .extract()
                .as(layerInformationResponse());

        assertAll(() -> {
            assertTrue(response.sucess);
            assertNull(response.payload);
            assertNull(response.message);
        });
    }

    @Test
    void shouldReturnOKWhenDeleteSeveralLayers() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/layers/delete/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/layers/delete/3")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();

        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/layers/delete/4")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }


    @Test
    void shouldReturnMessageWhenTryToDeleteUnknownLayer() {
        var response = given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/layers/delete/20")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode())
                .assertThat()
                .extract()
                .as(layerInformationResponse());

        assertAll(() -> {
            assertFalse(response.sucess);
            assertNull(response.payload);
            assertEquals("Layer with id (20) not exist", response.message);
        });
    }

    @Test
    void shouldReturnBadRequestWhenAttachAPOIToLayerWithBadJson() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("[\n" +
                        "    {\n" +
                        "        \"x\" : 116.7,\n" +
                        "        \"name\": \"Station de lavage\",\n" +
                        "        \"y\": 44.5,\n" +
                        "        \"properties\" : {\n" +
                        "            \"type\": \"service payant\"\n" +
                        "            }\n" +
                        "    }, \n" +
                        "    {\n" +
                        "    \"x\" : 18.7,\n" +
                        "    \"name\": \"Garage\",\n" +
                        "    \"y\": 12.5,\n" +
                        "    \"properties\" : {\n" +
                        "        \"type\": \"réparation camion\"\n" +
                        "  }\n" +
                        "}\n" +
                        "\n" +
                        "]")
                .post("/layers/6/attachpoi")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }


    @Test
    void shouldReturnBadRequestWhenUpdateLayerWithEmptyDescription() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"\",\n" +
                        "  \"name\": \"layer_6_sapeurs_pompiers\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturnBadRequestWhenUpdateLayerWithEmptyJson() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturnBadRequestErrorWhenUpdateLayerWithNullJson() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("null")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturnBadRequestWhenUpdateLayerWithNullName() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": null,\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .assertThat();
    }

    @Test
    @Timeout(100)
    void shouldReturnOKWhenDeleteLayerWithTimeOut() {
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .delete("/layers/delete/7")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .assertThat();
    }

    @Test
    void shouldReturn401GetLayersNotConnected(){
        given()
                .when()
                .get("/layers/5")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }

    @Test
    void shouldReturn40GetLayersBadToken(){
        var badToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJuZ21zIiwiaWF0IjoxNjE0NTIyNzY4LCJleHAiOjE2NDYwNTg3NjgsImF1ZCI6Ind"+
                "3dy5leGFtcGxlLmNvbSIsInN1YiI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJHaXZlbk5hbWUiOiJKb2hubnkiLCJTdXJuYW1lIjoiUm9ja"+
                "2V0IiwiRW1haWwiOiJqcm9ja2V0QGV4YW1wbGUuY29tIiwiUm9sZSI6WyJNYW5hZ2VyIiwiUHJvamVjdCBBZG1pbmlzdHJhdG9yIl19"+
                ".A26ZuO1vNX1zHSYxhnhpcwfDW5ogtQOSWXuJSqZPqVY";
        given()
                .when()
                .header("Authorization", "Bearer " + badToken)
                .get("/layers/5")
                .then()
                .statusCode(Response.Status.UNAUTHORIZED.getStatusCode());
    }
    @Test
    void shouldReturn403LayerWithUser(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"Nom Update\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"Nom Update\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .post("/categories/5/attachlayer")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .delete("/layers/delete/5")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }
    @Test
    void shouldReturn403LayerWitEditor(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"Nom Update\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .post("/categories/5/attachlayer")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .delete("/layers/delete/5")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    void shoudEditorCanUpdateLayer(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"couche des points d'intérêts pour les sapeurs pompiers\",\n" +
                        "  \"name\": \"Nom Update\",\n" +
                        "  \"properties\": {\n" +
                        "      \"lieu\": \"objets utiles en cas d'incendies\"\n" +
                        "  } \n" +
                        "}")
                .put("/layers/update/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    @Test
    void shouldUserCantImportAndExportLayer(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"user@user\",\n" +
                        "    \"password\": \"user\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"eyJ0eXBlIjoiRmVhdHVyZUNvbGxlY3Rpb24iLCJmZWF0dXJlcyI6W3sidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0NzI4MjM4NzgzNTcsNDMuMzA2OTcwMjYyNTYzNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFdGF0IE1ham9yIiwiaWQiOiI5MDAyIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzU5NzI2NDYwMjI2NjQsNDMuMzIwODQ0MjgxNDE1NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIExhIEJpZ3VlIiwiaWQiOiI5MDAzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzgwMTM2OTA4OTI3NzEsNDMuMzE3OTA2MTkyNTMwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFBsb21iae+/vXJlcyIsImlkIjoiOTAwNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM5MTU5NTYyMzY0ODYzLDQzLjI3NjExNjA0NjE3MjddLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBMb3V2YWluIiwiaWQiOiI5MDA1In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNDAyMjYxOTM3MDU4MDIsNDMuMjkyMjA4MTc1NDg1OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IFBpZXJyZSIsImlkIjoiOTAwNiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM2MTgyOTk4NDE5ODA5LDQzLjI4NzM5Mzk3MjI2ODZdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBkIEVuZG91bWUiLCJpZCI6IjkwMDcifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zMjc4MTk0MTIyNDIzMSw0My4zNTQ3NzI1MDIwNjExXSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkNldGlzIFNhdW1hdHkiLCJpZCI6IjkwMDgifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zNTUxODgzNzg1MDgyLDQzLjM3Mzk5NDUyNjc5NTRdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBBbnRvaW5lIiwiaWQiOiI5MDA5In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzYyNjI2MjIxODk1MjgsNDMuMzc4NzQwMzE3MDMxOF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJE77+9dGFjaGVtZW50IEjvv71waXRhbCBOb3JkIiwiaWQiOiI5MDEwIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzk5NTA1MTUxNTY2OTIsNDMuMzI4NTAxNjQyNjM2OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIE1hbHBhc3Pvv70iLCJpZCI6IjkwMTEifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS40MzQyNjk3ODU4MDY1Niw0My4zMzE5ODM2MDIxMzc4XSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkTvv710YWNoZW1lbnQgZGUgbGEgUm9zZSIsImlkIjoiOTAxMiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQxMTI2MzU2NjcwNzQ0LDQzLjMxOTA5MzA1OTVdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBKdXN0IiwiaWQiOiI5MDEzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc1MjAyOTg5NzQzODksNDMuMzA2Mzg4NjY2MjQ4NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IExhemFyZSIsImlkIjoiOTAxNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM4MDAxMzc3NzE5NDA3LDQzLjI5NzM4NjYzNzEyMDNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBDYW5lYmnvv71yZSIsImlkIjoiOTAxNSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQyOTg5MzQ1MzU1NTMsNDMuMjg4OTIxNTU5OTUwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFY29sZSBkZSBsYSBQYXJldHRlIiwiaWQiOiI5MDE2In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNTAzMjYyODQ2OTY0OTEsNDMuMjg4MjQzNTY3NTA0N10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IE1lbmV0IiwiaWQiOiI5MDE3In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0MDA1OTY2ODczNDYsNDMuMjQ3MzA2MDIyOTEwMV0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIGRlIGxhIFBvaW50ZSByb3VnZSIsImlkIjoiOTAxOCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQzMzgxNjQ1OTk3MDMsNDMuMjM0ODU4NjY2MzY4M10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIEx1bWlueSIsImlkIjoiOTAxOSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjMwNDA3OTE0MDQwNzU5LDQzLjI3OTk5MjU0NzU2ODNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBGcmlvdWwiLCJpZCI6IjkwMjAifX1dLCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJQb21waWVycyBkZSBNYXJzZWlsbGUiLCJkZXNjcmlwdGlvbiI6IkNhc2VybmVzIGRlIHBvbXBpZXJzIGRlIE1hcnNlaWxsZSAiLCJpZCI6IjE5In19\" \n" +
                        "}")
                .post("/layers/6/importPois")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .get("/layers/export/6")
                .then()
                .statusCode(Response.Status.FORBIDDEN.getStatusCode());
    }

    @Test
    void shouldEditorCanImportAndExportLayer(){
        var authResponse = given()
                .body("{\n" +
                        "    \"email\" : \"editor@editor\",\n" +
                        "    \"password\": \"editor\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .post("/user/login")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(authResponse());
        var tmpToken = authResponse.payload.getToken();

        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"eyJ0eXBlIjoiRmVhdHVyZUNvbGxlY3Rpb24iLCJmZWF0dXJlcyI6W3sidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0NzI4MjM4NzgzNTcsNDMuMzA2OTcwMjYyNTYzNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFdGF0IE1ham9yIiwiaWQiOiI5MDAyIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzU5NzI2NDYwMjI2NjQsNDMuMzIwODQ0MjgxNDE1NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIExhIEJpZ3VlIiwiaWQiOiI5MDAzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzgwMTM2OTA4OTI3NzEsNDMuMzE3OTA2MTkyNTMwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFBsb21iae+/vXJlcyIsImlkIjoiOTAwNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM5MTU5NTYyMzY0ODYzLDQzLjI3NjExNjA0NjE3MjddLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBMb3V2YWluIiwiaWQiOiI5MDA1In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNDAyMjYxOTM3MDU4MDIsNDMuMjkyMjA4MTc1NDg1OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IFBpZXJyZSIsImlkIjoiOTAwNiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM2MTgyOTk4NDE5ODA5LDQzLjI4NzM5Mzk3MjI2ODZdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBkIEVuZG91bWUiLCJpZCI6IjkwMDcifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zMjc4MTk0MTIyNDIzMSw0My4zNTQ3NzI1MDIwNjExXSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkNldGlzIFNhdW1hdHkiLCJpZCI6IjkwMDgifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zNTUxODgzNzg1MDgyLDQzLjM3Mzk5NDUyNjc5NTRdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBBbnRvaW5lIiwiaWQiOiI5MDA5In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzYyNjI2MjIxODk1MjgsNDMuMzc4NzQwMzE3MDMxOF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJE77+9dGFjaGVtZW50IEjvv71waXRhbCBOb3JkIiwiaWQiOiI5MDEwIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzk5NTA1MTUxNTY2OTIsNDMuMzI4NTAxNjQyNjM2OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIE1hbHBhc3Pvv70iLCJpZCI6IjkwMTEifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS40MzQyNjk3ODU4MDY1Niw0My4zMzE5ODM2MDIxMzc4XSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkTvv710YWNoZW1lbnQgZGUgbGEgUm9zZSIsImlkIjoiOTAxMiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQxMTI2MzU2NjcwNzQ0LDQzLjMxOTA5MzA1OTVdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBKdXN0IiwiaWQiOiI5MDEzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc1MjAyOTg5NzQzODksNDMuMzA2Mzg4NjY2MjQ4NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IExhemFyZSIsImlkIjoiOTAxNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM4MDAxMzc3NzE5NDA3LDQzLjI5NzM4NjYzNzEyMDNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBDYW5lYmnvv71yZSIsImlkIjoiOTAxNSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQyOTg5MzQ1MzU1NTMsNDMuMjg4OTIxNTU5OTUwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFY29sZSBkZSBsYSBQYXJldHRlIiwiaWQiOiI5MDE2In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNTAzMjYyODQ2OTY0OTEsNDMuMjg4MjQzNTY3NTA0N10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IE1lbmV0IiwiaWQiOiI5MDE3In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0MDA1OTY2ODczNDYsNDMuMjQ3MzA2MDIyOTEwMV0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIGRlIGxhIFBvaW50ZSByb3VnZSIsImlkIjoiOTAxOCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQzMzgxNjQ1OTk3MDMsNDMuMjM0ODU4NjY2MzY4M10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIEx1bWlueSIsImlkIjoiOTAxOSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjMwNDA3OTE0MDQwNzU5LDQzLjI3OTk5MjU0NzU2ODNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBGcmlvdWwiLCJpZCI6IjkwMjAifX1dLCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJQb21waWVycyBkZSBNYXJzZWlsbGUiLCJkZXNjcmlwdGlvbiI6IkNhc2VybmVzIGRlIHBvbXBpZXJzIGRlIE1hcnNlaWxsZSAiLCJpZCI6IjE5In19\" \n" +
                        "}")
                .post("/layers/6/importPois")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + tmpToken)
                .contentType(ContentType.JSON)
                .get("/layers/export/6")
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void shouldReturnBadRequestExportUknownLayer(){
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .get("/layers/export/-4")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .get("/layers/export/556")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    void shouldAddPoisWithCorrectEncodeFileToLayer(){
        var category = given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"description\": \"importCategorie\",\n" +
                        "  \"name\": \"importCategorie\"\n" +
                        "}")
                .post("/categories/create")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode())
                .extract()
                .as(categoryInformationResponse()).payload;

        var layer = given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "            \"description\": \"layer_import\",\n" +
                        "                \"name\": \"layer\",\n" +
                        "                \"layerAttributes\": [\n" +
                        "            \"type\"\n" +
                        "  ]\n" +
                        "        }")
                .post("/categories/"+category.getId()+"/attachlayer")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(layerInformationResponse()).payload;
        var layerSize = given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .get("/layers/"+layer.getId()+"/pois")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(layerGeoJsonResponse()).payload.getFeatures().size();
        assertEquals(layerSize, 0);
        var  response= given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"eyJ0eXBlIjoiRmVhdHVyZUNvbGxlY3Rpb24iLCJmZWF0dXJlcyI6W3sidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0NzI4MjM4NzgzNTcsNDMuMzA2OTcwMjYyNTYzNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFdGF0IE1ham9yIiwiaWQiOiI5MDAyIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzU5NzI2NDYwMjI2NjQsNDMuMzIwODQ0MjgxNDE1NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIExhIEJpZ3VlIiwiaWQiOiI5MDAzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzgwMTM2OTA4OTI3NzEsNDMuMzE3OTA2MTkyNTMwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFBsb21iae+/vXJlcyIsImlkIjoiOTAwNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM5MTU5NTYyMzY0ODYzLDQzLjI3NjExNjA0NjE3MjddLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBMb3V2YWluIiwiaWQiOiI5MDA1In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNDAyMjYxOTM3MDU4MDIsNDMuMjkyMjA4MTc1NDg1OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IFBpZXJyZSIsImlkIjoiOTAwNiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM2MTgyOTk4NDE5ODA5LDQzLjI4NzM5Mzk3MjI2ODZdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBkIEVuZG91bWUiLCJpZCI6IjkwMDcifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zMjc4MTk0MTIyNDIzMSw0My4zNTQ3NzI1MDIwNjExXSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkNldGlzIFNhdW1hdHkiLCJpZCI6IjkwMDgifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zNTUxODgzNzg1MDgyLDQzLjM3Mzk5NDUyNjc5NTRdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBBbnRvaW5lIiwiaWQiOiI5MDA5In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzYyNjI2MjIxODk1MjgsNDMuMzc4NzQwMzE3MDMxOF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJE77+9dGFjaGVtZW50IEjvv71waXRhbCBOb3JkIiwiaWQiOiI5MDEwIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzk5NTA1MTUxNTY2OTIsNDMuMzI4NTAxNjQyNjM2OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIE1hbHBhc3Pvv70iLCJpZCI6IjkwMTEifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS40MzQyNjk3ODU4MDY1Niw0My4zMzE5ODM2MDIxMzc4XSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkTvv710YWNoZW1lbnQgZGUgbGEgUm9zZSIsImlkIjoiOTAxMiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQxMTI2MzU2NjcwNzQ0LDQzLjMxOTA5MzA1OTVdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBKdXN0IiwiaWQiOiI5MDEzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc1MjAyOTg5NzQzODksNDMuMzA2Mzg4NjY2MjQ4NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IExhemFyZSIsImlkIjoiOTAxNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM4MDAxMzc3NzE5NDA3LDQzLjI5NzM4NjYzNzEyMDNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBDYW5lYmnvv71yZSIsImlkIjoiOTAxNSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQyOTg5MzQ1MzU1NTMsNDMuMjg4OTIxNTU5OTUwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFY29sZSBkZSBsYSBQYXJldHRlIiwiaWQiOiI5MDE2In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNTAzMjYyODQ2OTY0OTEsNDMuMjg4MjQzNTY3NTA0N10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IE1lbmV0IiwiaWQiOiI5MDE3In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0MDA1OTY2ODczNDYsNDMuMjQ3MzA2MDIyOTEwMV0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIGRlIGxhIFBvaW50ZSByb3VnZSIsImlkIjoiOTAxOCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQzMzgxNjQ1OTk3MDMsNDMuMjM0ODU4NjY2MzY4M10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIEx1bWlueSIsImlkIjoiOTAxOSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjMwNDA3OTE0MDQwNzU5LDQzLjI3OTk5MjU0NzU2ODNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBGcmlvdWwiLCJpZCI6IjkwMjAifX1dLCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJQb21waWVycyBkZSBNYXJzZWlsbGUiLCJkZXNjcmlwdGlvbiI6IkNhc2VybmVzIGRlIHBvbXBpZXJzIGRlIE1hcnNlaWxsZSAiLCJpZCI6IjE5In19\" \n" +
                        "}")
                .post("/layers/"+layer.getId()+"/importPois")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(layerGeoJsonResponse()).payload.getFeatures().size();
                assertTrue(response > layerSize);
    }
    @Test
    void shouldImportExistingPOINotRaiseExceptionAndNotImport(){
                var  size= given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"eyJ0eXBlIjoiRmVhdHVyZUNvbGxlY3Rpb24iLCJmZWF0dXJlcyI6W3sidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0NzI4MjM4NzgzNTcsNDMuMzA2OTcwMjYyNTYzNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFdGF0IE1ham9yIiwiaWQiOiI5MDAyIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzU5NzI2NDYwMjI2NjQsNDMuMzIwODQ0MjgxNDE1NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIExhIEJpZ3VlIiwiaWQiOiI5MDAzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzgwMTM2OTA4OTI3NzEsNDMuMzE3OTA2MTkyNTMwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFBsb21iae+/vXJlcyIsImlkIjoiOTAwNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM5MTU5NTYyMzY0ODYzLDQzLjI3NjExNjA0NjE3MjddLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBMb3V2YWluIiwiaWQiOiI5MDA1In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNDAyMjYxOTM3MDU4MDIsNDMuMjkyMjA4MTc1NDg1OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IFBpZXJyZSIsImlkIjoiOTAwNiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM2MTgyOTk4NDE5ODA5LDQzLjI4NzM5Mzk3MjI2ODZdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBkIEVuZG91bWUiLCJpZCI6IjkwMDcifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zMjc4MTk0MTIyNDIzMSw0My4zNTQ3NzI1MDIwNjExXSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkNldGlzIFNhdW1hdHkiLCJpZCI6IjkwMDgifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zNTUxODgzNzg1MDgyLDQzLjM3Mzk5NDUyNjc5NTRdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBBbnRvaW5lIiwiaWQiOiI5MDA5In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzYyNjI2MjIxODk1MjgsNDMuMzc4NzQwMzE3MDMxOF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJE77+9dGFjaGVtZW50IEjvv71waXRhbCBOb3JkIiwiaWQiOiI5MDEwIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzk5NTA1MTUxNTY2OTIsNDMuMzI4NTAxNjQyNjM2OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIE1hbHBhc3Pvv70iLCJpZCI6IjkwMTEifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS40MzQyNjk3ODU4MDY1Niw0My4zMzE5ODM2MDIxMzc4XSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkTvv710YWNoZW1lbnQgZGUgbGEgUm9zZSIsImlkIjoiOTAxMiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQxMTI2MzU2NjcwNzQ0LDQzLjMxOTA5MzA1OTVdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBKdXN0IiwiaWQiOiI5MDEzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc1MjAyOTg5NzQzODksNDMuMzA2Mzg4NjY2MjQ4NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IExhemFyZSIsImlkIjoiOTAxNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM4MDAxMzc3NzE5NDA3LDQzLjI5NzM4NjYzNzEyMDNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBDYW5lYmnvv71yZSIsImlkIjoiOTAxNSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQyOTg5MzQ1MzU1NTMsNDMuMjg4OTIxNTU5OTUwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFY29sZSBkZSBsYSBQYXJldHRlIiwiaWQiOiI5MDE2In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNTAzMjYyODQ2OTY0OTEsNDMuMjg4MjQzNTY3NTA0N10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IE1lbmV0IiwiaWQiOiI5MDE3In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0MDA1OTY2ODczNDYsNDMuMjQ3MzA2MDIyOTEwMV0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIGRlIGxhIFBvaW50ZSByb3VnZSIsImlkIjoiOTAxOCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQzMzgxNjQ1OTk3MDMsNDMuMjM0ODU4NjY2MzY4M10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIEx1bWlueSIsImlkIjoiOTAxOSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjMwNDA3OTE0MDQwNzU5LDQzLjI3OTk5MjU0NzU2ODNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBGcmlvdWwiLCJpZCI6IjkwMjAifX1dLCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJQb21waWVycyBkZSBNYXJzZWlsbGUiLCJkZXNjcmlwdGlvbiI6IkNhc2VybmVzIGRlIHBvbXBpZXJzIGRlIE1hcnNlaWxsZSAiLCJpZCI6IjE5In19\" \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(layerGeoJsonResponse()).payload.getFeatures().size();
        var  secondImport= given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"eyJ0eXBlIjoiRmVhdHVyZUNvbGxlY3Rpb24iLCJmZWF0dXJlcyI6W3sidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0NzI4MjM4NzgzNTcsNDMuMzA2OTcwMjYyNTYzNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFdGF0IE1ham9yIiwiaWQiOiI5MDAyIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzU5NzI2NDYwMjI2NjQsNDMuMzIwODQ0MjgxNDE1NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIExhIEJpZ3VlIiwiaWQiOiI5MDAzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzgwMTM2OTA4OTI3NzEsNDMuMzE3OTA2MTkyNTMwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFBsb21iae+/vXJlcyIsImlkIjoiOTAwNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM5MTU5NTYyMzY0ODYzLDQzLjI3NjExNjA0NjE3MjddLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBMb3V2YWluIiwiaWQiOiI5MDA1In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNDAyMjYxOTM3MDU4MDIsNDMuMjkyMjA4MTc1NDg1OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IFBpZXJyZSIsImlkIjoiOTAwNiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM2MTgyOTk4NDE5ODA5LDQzLjI4NzM5Mzk3MjI2ODZdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBkIEVuZG91bWUiLCJpZCI6IjkwMDcifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zMjc4MTk0MTIyNDIzMSw0My4zNTQ3NzI1MDIwNjExXSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkNldGlzIFNhdW1hdHkiLCJpZCI6IjkwMDgifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS4zNTUxODgzNzg1MDgyLDQzLjM3Mzk5NDUyNjc5NTRdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBBbnRvaW5lIiwiaWQiOiI5MDA5In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzYyNjI2MjIxODk1MjgsNDMuMzc4NzQwMzE3MDMxOF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJE77+9dGFjaGVtZW50IEjvv71waXRhbCBOb3JkIiwiaWQiOiI5MDEwIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzk5NTA1MTUxNTY2OTIsNDMuMzI4NTAxNjQyNjM2OF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIE1hbHBhc3Pvv70iLCJpZCI6IjkwMTEifX0seyJ0eXBlIjoiRmVhdHVyZSIsImdlb21ldHJ5Ijp7ImNvb3JkaW5hdGVzIjpbNS40MzQyNjk3ODU4MDY1Niw0My4zMzE5ODM2MDIxMzc4XSwidHlwZSI6IlBvaW50In0sInByb3BlcnRpZXMiOnsibmFtZSI6IkTvv710YWNoZW1lbnQgZGUgbGEgUm9zZSIsImlkIjoiOTAxMiJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQxMTI2MzU2NjcwNzQ0LDQzLjMxOTA5MzA1OTVdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBTYWludCBKdXN0IiwiaWQiOiI5MDEzIn19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc1MjAyOTg5NzQzODksNDMuMzA2Mzg4NjY2MjQ4NF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IExhemFyZSIsImlkIjoiOTAxNCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjM4MDAxMzc3NzE5NDA3LDQzLjI5NzM4NjYzNzEyMDNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBDYW5lYmnvv71yZSIsImlkIjoiOTAxNSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQyOTg5MzQ1MzU1NTMsNDMuMjg4OTIxNTU5OTUwNF0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJFY29sZSBkZSBsYSBQYXJldHRlIiwiaWQiOiI5MDE2In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuNTAzMjYyODQ2OTY0OTEsNDMuMjg4MjQzNTY3NTA0N10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIFNhaW50IE1lbmV0IiwiaWQiOiI5MDE3In19LHsidHlwZSI6IkZlYXR1cmUiLCJnZW9tZXRyeSI6eyJjb29yZGluYXRlcyI6WzUuMzc0MDA1OTY2ODczNDYsNDMuMjQ3MzA2MDIyOTEwMV0sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIGRlIGxhIFBvaW50ZSByb3VnZSIsImlkIjoiOTAxOCJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjQzMzgxNjQ1OTk3MDMsNDMuMjM0ODU4NjY2MzY4M10sInR5cGUiOiJQb2ludCJ9LCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJDYXNlcm5lIEx1bWlueSIsImlkIjoiOTAxOSJ9fSx7InR5cGUiOiJGZWF0dXJlIiwiZ2VvbWV0cnkiOnsiY29vcmRpbmF0ZXMiOls1LjMwNDA3OTE0MDQwNzU5LDQzLjI3OTk5MjU0NzU2ODNdLCJ0eXBlIjoiUG9pbnQifSwicHJvcGVydGllcyI6eyJuYW1lIjoiQ2FzZXJuZSBGcmlvdWwiLCJpZCI6IjkwMjAifX1dLCJwcm9wZXJ0aWVzIjp7Im5hbWUiOiJQb21waWVycyBkZSBNYXJzZWlsbGUiLCJkZXNjcmlwdGlvbiI6IkNhc2VybmVzIGRlIHBvbXBpZXJzIGRlIE1hcnNlaWxsZSAiLCJpZCI6IjE5In19\" \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract()
                .as(layerGeoJsonResponse()).payload.getFeatures().size();
        assertEquals(size, secondImport);
    }
    @Test
    void shouldImportAcceptOnlyPointGeometryType(){
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"ICB7ICJ0eXBlIjogIkZlYXR1cmVDb2xsZWN0aW9uIiwKICAgICJmZWF0dXJlcyI6IFsKICAgICAgeyAidHlwZSI6ICJGZWF0dXJlIiwKICAgICAgICAiZ2VvbWV0cnkiOiB7InR5cGUiOiAiUG9pbnQiLCAiY29vcmRpbmF0ZXMiOiBbMTAyLjAsIDAuNV19LAogICAgICAgICJwcm9wZXJ0aWVzIjogeyJwcm9wMCI6ICJ2YWx1ZTAifQogICAgICAgIH0sCiAgICAgIHsgInR5cGUiOiAiRmVhdHVyZSIsCiAgICAgICAgImdlb21ldHJ5IjogewogICAgICAgICAgInR5cGUiOiAiTGluZVN0cmluZyIsCiAgICAgICAgICAiY29vcmRpbmF0ZXMiOiBbCiAgICAgICAgICAgIFsxMDIuMCwgMC4wXSwgWzEwMy4wLCAxLjBdLCBbMTA0LjAsIDAuMF0sIFsxMDUuMCwgMS4wXQogICAgICAgICAgICBdCiAgICAgICAgICB9LAogICAgICAgICJwcm9wZXJ0aWVzIjogewogICAgICAgICAgInByb3AwIjogInZhbHVlMCIsCiAgICAgICAgICAicHJvcDEiOiAwLjAKICAgICAgICAgIH0KICAgICAgICB9LAogICAgICB7ICJ0eXBlIjogIkZlYXR1cmUiLAogICAgICAgICAiZ2VvbWV0cnkiOiB7CiAgICAgICAgICAgInR5cGUiOiAiUG9seWdvbiIsCiAgICAgICAgICAgImNvb3JkaW5hdGVzIjogWwogICAgICAgICAgICAgWyBbMTAwLjAsIDAuMF0sIFsxMDEuMCwgMC4wXSwgWzEwMS4wLCAxLjBdLAogICAgICAgICAgICAgICBbMTAwLjAsIDEuMF0sIFsxMDAuMCwgMC4wXSBdCiAgICAgICAgICAgICBdCiAgICAgICAgIH0sCiAgICAgICAgICJwcm9wZXJ0aWVzIjogewogICAgICAgICAgICJwcm9wMCI6ICJ2YWx1ZTAiLAogICAgICAgICAgICJwcm9wMSI6IHsidGhpcyI6ICJ0aGF0In0KICAgICAgICAgICB9CiAgICAgICAgIH0KICAgICAgIF0KICAgICB9Cg\" \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode());
    }
    @Test
    void shouldNotAcceptInvalidJson(){
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": null \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"test\" \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode());
    }

    @Test
    void shouldNotAcceptInvalidGeoJsonFile(){
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"ICB7ICJ0eXBlIjogIkZlYXR1cmVDb2xsZWN0aW9uIiwKICAgICJmZWF0dXJlcyI6IFsKICAgICAgeyAidHlwZSI6ICJGZWF0dXJlIiwKICAgICAgICAiZ2VvbWV0cnkiOiB7InR5cGUiOiAiUG9pbnQiLCAiY29vcmRpbmF0ZXMiOiBbMTAyLjAsIDAuNV19LAogICAgICAgICJwcm9wZXJ0aWVzIjogeyJwcm9wMCI6ICJ2YWx1ZTAifQogICAgICAgIH0sCiAgICAgIHsgInR5cGUiOiAiRmVhdHVyZSIsCiAgICAgICAgImdlb21ldHJ5IjogewogICAgICAgICAgInR5cGUiOiAiTGluZVN0cmluZyIsCiAgICAgICAgICAiY29vcmRpbmF0ZXMiOiBbCiAgICAgICAgICAgIFsxMDIuMCwgMC4wXSwgWzEwMy4wLCAxLjBdLCBbMTA0LjAsIDAuMF0sIFsxMDUuMCwgMS4wXQogICAgICAgICAgICBdCiAgICAgICAgICB9LAogICAgICAgICJwcm9wZXJ0aWVzIjogewogICAgICAgICAgInByb3AwIjogInZhbHVlMCIsCiAgICAgICAgICAicHJvcDEiOiAwLjAKICAgICAgICAgIH0KICAgICAgICB9LAogICAgICB7ICJ0eXBlIjogIkZlYXR1cmUiLAogICAgICAgICAiZ2VvbWV0cnkiOiB7CiAgICAgICAgICAgInR5cGUiOiAiUG9seWdvbiIsCiAgICAgICAgICAgImNvb3JkaW5hdGVzIjogWwogICAgICAgICAgICAgWyBbMTAwLjAsIDAuMF0sIFsxMDEuMCwgMC4wXSwgWzEwMS4wLCAxLjBdLAogICAgICAgICAgICAgICBbMTAwLjAsIDEuMF0sIFsxMDAuMCwgMC4wXSBdCiAgICAgICAgICAgICBdCiAgICAgICAgIH0sCiAgICAgICAgICJwcm9wZXJ0aWVzIjogewogICAgICAgICAgICJwcm9wMCI6ICJ2YWx1ZTAiLAogICAgICAgICAgICJwcm9wMQo=\" \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode());
        given()
                .when()
                .header("Authorization", "Bearer " + token)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"file64Encoded\": \"ICB7ICJ0eXBlIjogIkZlYXR1cmVDb2xsZWN0aW9uIiwKICAgICJmZWF0dXJlcyI6IFsKICAgICAgeyAidHlwZSI6ICJGZWF0dXJlIiwKICAgICAgICAiZ2VvbWV0cnkiOiB7InR5cGUiOiAiUG9pbnQiLCAiY29vcmRpbmF0ZXMiOiBbMTAyLjAsIDAuNV19LAogICAgICAgICJwcm9wZXJ0aWVzIjogeyJwcm9wMCI6ICJ2YWx1ZTAifQogICAgICAgIH0sCiAgICAgIHsgInR5cGUiOiAiRmVhdHVyZSIsCiAgICAgICAgImdlb21ldHJ5IjogewogICAgICAgICAgInR5cGUiOiAiUG9pbnQiLAogICAgICAgICAgImNvb3JkaW5hdGVzIjogWwogICAgICAgICAgICBbMTAyLjAsIDAuMF0sIFsxMDMuMCwgMS4wXSwgWzEwNC4wLCAwLjBdLCBbMTA1LjAsIDEuMF0KICAgICAgICAgICAgXQogICAgICAgICAgfSwKICAgICAgICAicHJvcGVydGllcyI6IHsKICAgICAgICAgICJwcm9wMCI6ICJ2YWx1ZTAiLAogICAgICAgICAgInByb3AxIjogMC4wCiAgICAgICAgICB9CiAgICAgICAgfSwKICAgICAgeyAidHlwZSI6ICJGZWF0dXJlIiwKICAgICAgICAgImdlb21ldHJ5IjogewogICAgICAgICAgICJ0eXBlIjogIlBvaW50IiwKICAgICAgICAgICAiY29vcmRpbmF0ZXMiOiBbCiAgICAgICAgICAgICBbIFsxMDAuMCwgMC4wXSwgWzEwMS4wLCAwLjBdLCBbMTAxLjAsIDEuMF0sCiAgICAgICAgICAgICAgIFsxMDAuMCwgMS4wXSwgWzEwMC4wLCAwLjBdIF0KICAgICAgICAgICAgIF0KICAgICAgICAgfSwKICAgICAgICAgInByb3BlcnRpZXMiOiB7CiAgICAgICAgICAgInByb3AwIjogInZhbHVlMCIsCiAgICAgICAgICAgInByb3AxIjogeyJ0aGlzIjogInRoYXQifQogICAgICAgICAgIH0KICAgICAgICAgfQogICAgICAgXQogICAgIH0K\" \n" +
                        "}")
                .post("/layers/5/importPois")
                .then()
                .statusCode(Response.Status.EXPECTATION_FAILED.getStatusCode());
    }

    @NotNull
    static TypeRef<LayerInformationResponse> layerInformationResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    @NotNull
    static TypeRef<BasicLayerPayloadResponse> basicLayerPayloadResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    @NotNull
    static TypeRef<LayerGeoJsonResponse> layerGeoJsonResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }

    @NotNull
    static TypeRef<AuthResponseResponse> authResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }
    @NotNull
    static TypeRef<CategoryControllerTest.CategoryInformationResponse> categoryInformationResponse() {
        return new TypeRef<>() {
            // Kept empty on purpose
        };
    }


    static class LayerInformationResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        BasicLayersPayload.LayerInformation payload;
    }


    static class BasicLayerPayloadResponse implements Serializable {
        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("message")
        String message;

        @JsonProperty("payload")
        BasicLayersPayload payload;
    }


    static class LayerGeoJsonResponse implements Serializable {
        @JsonProperty("message")
        String message;

        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("payload")
        LayerGeoJson payload;
    }
    static class AuthResponseResponse implements Serializable {
        @JsonProperty("message")
        String message;

        @JsonProperty("success")
        boolean sucess;

        @JsonProperty("payload")
        AuthResponse payload;
    }
}
