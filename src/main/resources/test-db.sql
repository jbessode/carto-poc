--
-- Data for Name: ngms_categories; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO ngms_categories (id, description, name) VALUES (1, 'categorie des camera', 'camera');
INSERT INTO ngms_categories (id, description, name) VALUES (2, 'categorie des capteurs', 'capteurs');
INSERT INTO ngms_categories (id, description, name) VALUES (3, 'categorie de test', 'test');
INSERT INTO ngms_categories (id, description, name) VALUES (4, 'categorie de parcs attractions', 'parcs');
INSERT INTO ngms_categories (id, description, name) VALUES (5, 'categorie des lieux policiers européens', 'police');
INSERT INTO ngms_categories (id, description, name) VALUES (6, 'categorie de lieux pour les secours', 'pompiers');
INSERT INTO ngms_categories (id, description, name) VALUES (7, 'categorie de lieux de cultes européens', 'Lieux de cultes');
INSERT INTO ngms_categories (id, description, name) VALUES (8, 'categorie de centre d activités', 'Centre commerciaux');
--
-- Data for Name: ngms_layers; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (1, 'couche des camera normal', 'marque;type', 'layer_1_camera', 1);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (2, 'couche des camera thermique', 'marque;type;opacite', 'layer_2_camera_thermique', 1);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (3, 'couche exemple', '', 'layer_1', 2);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (4, 'couche des parcs en île de FRance ', 'lieu', 'layer_4_parcs', 4);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (5, 'couche des pois de la police', 'lieu', 'layer_5_police', 5);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (6, 'couche des pois des pompiers', 'lieu', 'layer_6_pompiers', 6);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (7, 'couche de lieux de cultes', 'lieu', 'layer_7_cultes', 7);
INSERT INTO ngms_layers (id, description, layer_attributes, name, category_id) VALUES (8, 'couche de centres d activités', 'lieu', 'layer_8_centres', 8);

--
-- Data for Name: ngms_poi; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (1, 'camera_zone_1', 1.85, 2.35, 1);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (2, 'camera_zone_2', 6.00, 4.00, 2);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (3, 'test poi', 1.00, 2.00, 3);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (4, 'DisneyLand', 2.45, 45.17, 4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (5, 'Asterix Park', 3.17, 15.10, 4);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (6, 'Vienne Police Station', 3.17, 15.10, 5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (7, 'Rome Police Station', 5.09, 12.04, 5);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (8, 'Hydrant', 12.13, 12.11, 6);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (9, 'Munich Fire Station', 88.11, 10.14, 6);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (10, 'Zurich Fire Station', 14.05, 08.10, 6);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (11, 'Santa Maria del Fiore cathedral', 10.02, 77.15, 7);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (12, 'Pise cathedral', 09.05, 17.14, 7);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (13, 'Gran Jonquera Outlet & Shopping', 11.12, 15.19, 8);
INSERT INTO ngms_poi (id, name, posx, posy, layer_id) VALUES (14, 'Centro Comercial Mendibil', 55.17, 12.14, 8);

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (1, 'gpioli0@msn.com', 'Gerta', 'Pioli', 'sleZPDEwq', 'USER');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (2, 'jpenzer1@netvibes.com', 'Jeremias', 'Penzer', '$2y$10$WFsw4ydWCtla6nvp2TYIU.Y9ovYRLEH4YyHiUW8UVMqhwqUmMNz8S', 'ADMIN');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (3, 'csimons2@nifty.com', 'Cynthy', 'Simons', 'mxPBegMfd', 'USER');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (4, 'rbasey3@twitter.com', 'Ryon', 'Basey', 'L4COhYt', 'USER');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (5, 'ehiseman4@ibm.com', 'Ediva', 'Hiseman', '3FBGaJ', 'EDITOR');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (6, 'vklagge5@godaddy.com', 'Verena', 'Klagge', '4vLxQfoudl8R', 'USER');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (8, 'lstockwell7@hp.com', 'Lisle', 'Stockwell', 'TgjkarkF9zPz', 'EDITOR');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (9, 'mthomazin8@bigcartel.com', 'Melinde', 'Thomazin', 'gzNHzO', 'USER');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (10, 'jdebroke9@vk.com', 'Jodi', 'de Broke', 'geQQj5ZPXr', 'USER');
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (11, 'admin@admin', 'ADMIN', 'ADMIN',
'$2a$10$ec7.4V9cyhghpgPQ7A0WqexswiKDDx0gzGyDAvqaPMFieyH3xRLNe', 'ADMIN'); --pwd=admin
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (12, 'editor@editor', 'EDITOR', 'EDITOR',
'$2a$10$teFUCDfBqC66ON6DZUwRj./0kjDZjgJ1o9gEeaoBdGmIRmrILs0JO', 'EDITOR'); --pwd=editor
INSERT INTO ngms_users (id, email, firstname, lastname, password, role) values (13, 'user@user', 'USER', 'USER',
'$2a$10$oBPVkXPJJGsQUPmQko.Zmeh4aQV3S7I3VUgR8nHJ.9JSfL.7qhmDy', 'USER'); --pwd=user


--
--   set sequences
--


ALTER SEQUENCE category_seq RESTART WITH 9;

ALTER SEQUENCE layer_seq RESTART WITH 9;

ALTER SEQUENCE poi_seq RESTART WITH 15;

ALTER SEQUENCE users_seq RESTART WITH 14;