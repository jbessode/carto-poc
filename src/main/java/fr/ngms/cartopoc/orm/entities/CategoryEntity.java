package fr.ngms.cartopoc.orm.entities;

import fr.ngms.cartopoc.models.resquests.CategoryCreationRequest;
import fr.ngms.cartopoc.models.resquests.LayerInformationRequest;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represent a category entity according to database schema.
 */
@Entity(name = "CategoryEntity")
@Table(name = "ngms_categories")
public class CategoryEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_generator")
  @SequenceGenerator(name = "category_generator", sequenceName = "category_seq", allocationSize = 1)
  @Column(name = "id", updatable = false, nullable = false)

  private long id;

  @Column(name = "name", unique = true)
  private String name;

  @Column(name = "description", columnDefinition = "TEXT")
  private String description;

  @Column(name = "attached_layers")
  @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
  private List<LayerEntity> layers;

  public CategoryEntity() {
  }

  public CategoryEntity(String name, String description) {
    this.name = name.trim();
    this.description = description.trim();
    this.layers = new ArrayList<>();
  }

    /**
     * This static method transform a {@link CategoryCreationRequest} to a {@link CategoryEntity}
     * @param request the {@link CategoryCreationRequest}
     * @return a {@link CategoryEntity}
     */
  public static CategoryEntity fromRequest(CategoryCreationRequest request) {
    return new CategoryEntity(request.getName(), request.getDescription());
  }

  public String getName() {
    return this.name;
  }

  public String getDescription() {
    return this.description;
  }

  public void setName(String name) {
    this.name = name.trim();
  }

  public void setDescription(String description) {
    this.description = description.trim();
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getId() {
    return this.id;
  }

  public List<LayerEntity> getLayers() {
    return layers;
  }

  public void setLayers(List<LayerEntity> layers) {
    this.layers = layers;
  }

  public void addLayer(LayerEntity entity) {
    layers.add(entity);
  }

  @Override
  public String toString() {
    return "PoiCategoryEntity{" +
      "name='" + name + '\'' +
      ", description='" + description + '\'' +
      '}';
  }

    /**
     * This meth check if a category contains a layer
     * @param request the layer to check
     * @return true if the cotegory contain the layer, false otherwise
     */
  public boolean containLayer(LayerInformationRequest request) {
    return layers.stream().anyMatch(l -> l.getName().equalsIgnoreCase(request.getName()));
  }
}