package fr.ngms.cartopoc.orm.entities;

import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload.LayerInformation;
import fr.ngms.cartopoc.models.resquests.LayerInformationRequest;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class represent a layer entity according to the database schema
 */
@Entity(name = "LayerEntity")
@Table(name = "ngms_layers")
@DiscriminatorValue("layer")
public class LayerEntity{
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "layer_generator")
  @SequenceGenerator(name = "layer_generator", sequenceName = "layer_seq", allocationSize = 1)
  public long id;

  @Column(name = "name")
  private String name;

  @Column(name = "description", columnDefinition = "TEXT")
  private String description;

  @Column(name = "layer_attributes")
  private String layerAttributes; //"name;marque;dateDeCreation" (csv)

  @OneToMany(mappedBy = "layer", cascade = CascadeType.ALL, orphanRemoval = true)
  @Column(name = "attached_pois")
  private List<POIEntity> attachedPOI;

  @ManyToOne
  private CategoryEntity category;

  @Column(name = "icon", length = 65000)
  private String icon;

  public LayerEntity() {
  }

  public LayerEntity(String name, String description, String layerAttributes, String icon) {
    this.layerAttributes = layerAttributes;
    this.description = description;
    this.name = name;
    this.icon = icon;
  }

  public LayerEntity(String name, String description, String layerAttributes, List<POIEntity> attachedPOI) {
    this.layerAttributes = layerAttributes;
    this.description = description;
    this.name = name;
    this.attachedPOI = attachedPOI;
  }

    /**
     * This static method create a {@link LayerEntity} from a {@link LayerInformationRequest} and an {@link List<POIEntity>}
     * @param request the layer information
     * @param attachedPOI attached POI of the layer
     * @return a {@link LayerEntity}
     */
  public static LayerEntity fromRequest(LayerInformationRequest request, List<POIEntity> attachedPOI) {
    var properties = String.join(";", request.getProperties());
    return new LayerEntity(request.getName(), request.getDescription(), properties, attachedPOI);
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getLayerAttributes() {
    return layerAttributes;
  }

  public void setLayerAttributes(String layerAttributes) {
    this.layerAttributes = layerAttributes;
  }

  public List<POIEntity> getAttachedPOI() {
    return attachedPOI;
  }

  public void setAttachedPOI(List<POIEntity> attachedPOI) {
    this.attachedPOI = attachedPOI;
  }

  public CategoryEntity getCategory() {
    return category;
  }

  public void setCategory(CategoryEntity category) {
    this.category = category;
  }

  public void addPOI(POIEntity entity) {
    attachedPOI.add(entity);
  }

  public void addListPOI(List<POIEntity> entities) {
    attachedPOI.addAll(entities);
  }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
  public String toString() {
    return "POILayerEntity{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", description='" + description + '\'' +
      ", layerAttributes='" + layerAttributes + '\'' +
      ", attachedPOI=" + attachedPOI +
      '}';
  }

  public LayerInformation toLayerInformation() {
    var properties = Arrays.stream(layerAttributes.split(";")).collect(Collectors.toList());
    return new LayerInformation(id, name, description, properties, icon);
  }

  public boolean containsPOI(POIEntity poiEntity) {
    return attachedPOI.contains(poiEntity);
  }
}