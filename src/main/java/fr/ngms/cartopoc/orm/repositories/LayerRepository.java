package fr.ngms.cartopoc.orm.repositories;

import fr.ngms.cartopoc.orm.entities.LayerEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * This class represent a layer orm repository according to the repository pattern
 */
@ApplicationScoped
public class LayerRepository implements PanacheRepository<LayerEntity> {
    /**
     * This method find a layer by the given name
     * @param name the name to find
     * @return a layer {@link LayerEntity} with the given name
     */
    public LayerEntity findByName(String name) {
        return find("name", name).firstResult();
    }
}
