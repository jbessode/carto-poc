package fr.ngms.cartopoc.orm.repositories;

import fr.ngms.cartopoc.orm.entities.POIEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * This class represent a POI orm repository according to the repository pattern
 */
@ApplicationScoped
public class POIRepository implements PanacheRepository<POIEntity> {
}
