package fr.ngms.cartopoc.orm.repositories;

import fr.ngms.cartopoc.orm.entities.UserEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

/**
 * This class represent a user orm repository according to the repository pattern
 */
@ApplicationScoped
public class UserRepository implements PanacheRepository<UserEntity> {
    /**
     * This method find a user in the database by the given email
     * @param mail the email to search
     * @return a {@link UserEntity}
     */
    public UserEntity findByMail(String mail){
        return find("email",mail).firstResult();
    }
}
