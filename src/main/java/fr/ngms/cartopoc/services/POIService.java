package fr.ngms.cartopoc.services;

import fr.ngms.cartopoc.helpers.AttributeDataHelper;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicPOIPayload;
import fr.ngms.cartopoc.models.resquests.POIInsertionRequest;
import fr.ngms.cartopoc.orm.repositories.POIRepository;
import org.hibernate.HibernateException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Objects;
import java.util.logging.Logger;
/**
 * This class represent the service of a POI. This class deal with the {@link POIRepository} to manage POI.
 * This class is the interface between {@link fr.ngms.cartopoc.endpoints.POIController} and {@link POIRepository}.
 */
@ApplicationScoped
public class POIService {

  private final POIRepository repository;

  private final Logger logger = Logger.getLogger(getClass().getName());

  private static String notFoundMessage(long id) {
    return "POI with id (" + id + ") not exist";
  }

  @Inject
  public POIService(POIRepository repository) {
    this.repository = repository;
  }

    /**
     * Delete a POI.
     * @param id id of the POI to delete
     * @return a {@link ResponseBody} with true if the POI was deleting, message otherwise
     */
  @Transactional
  public ResponseBody deletePOI(long id) {
    try {
      var poi = repository.findById(id);
      if (poi == null) {
        return new ResponseBody(notFoundMessage(id));
      }
      repository.delete(poi);
      return new ResponseBody(true);
    } catch (HibernateException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }

    /**
     * Update a POI.
     * @param id id of POI to update
     * @param request information of the POI
     * @return a {@link ResponseBody} with {@link BasicPOIPayload} if success, message otherwise
     */
  @Transactional
  public ResponseBody updatePOI(long id, POIInsertionRequest.POIRequest request) {
    Objects.requireNonNull(request);
    try {
      var targetPOI = repository.findById(id);
      if (targetPOI == null) {
        return new ResponseBody(notFoundMessage(id));
      }

      if (!request.getName().equals(targetPOI.getName())) {
        targetPOI.setName(request.getName());
      }

      if (request.getX() != targetPOI.getPosX()) {
        targetPOI.setPosX(request.getX());
      }

      if (request.getY() != targetPOI.getPosY()) {
        targetPOI.setPosY(request.getY());
      }

      targetPOI.setProperties(AttributeDataHelper.toJson(request.getProperties()));

      repository.persistAndFlush(targetPOI);

      var reFetchPOI = repository.findById(id);

      var response = BasicPOIPayload.from(reFetchPOI.getId(), reFetchPOI.getName(), reFetchPOI.getPosX(),
        reFetchPOI.getPosY(), AttributeDataHelper.parse(reFetchPOI.getProperties()));
      return new ResponseBody(response);
    } catch (HibernateException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }
}