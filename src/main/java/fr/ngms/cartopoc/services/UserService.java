package fr.ngms.cartopoc.services;

import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.user.*;
import fr.ngms.cartopoc.orm.entities.UserEntity;
import fr.ngms.cartopoc.orm.repositories.UserRepository;
import fr.ngms.cartopoc.security.utils.TokenUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hibernate.HibernateException;
import org.mindrot.jbcrypt.BCrypt;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;
import java.util.stream.Collectors;
/**
 * This class represent the service of a user. This class deal with the {@link UserRepository} to manage user.
 * This class is the interface between {@link fr.ngms.cartopoc.endpoints.UserController} and {@link UserRepository}.
 */
@ApplicationScoped
@Transactional
public class UserService {
  @ConfigProperty(name = "fr.ngms.quarkusjwt.jwt.duration")
  public Long duration;
  @ConfigProperty(name = "mp.jwt.verify.issuer")
  public String issuer;
  private final UserRepository userRepository;
  private static final String UNAUTHORIZED = "Unauthorized operation";

  @Inject
  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

    /**
     * Check if the given user is known by then database
     * @param context the security context
     * @return true if it is known, false otherwise
     */
  public boolean isKnownUser(SecurityContext context){
      Objects.requireNonNull(context);
      var targetUser = this.userRepository.findByMail(context.getUserPrincipal().getName());
      if(targetUser == null ){
          return false;
      }
      return context.isUserInRole(targetUser.getRole());
  }

    /**
     * Register a user
     * @param request the user information
     * @return a {@link ResponseBody} with true if success, message otherwise
     */
  public ResponseBody signUp(UserInformationRequest request) {
    try {
      var requestEmail = request.getEmail();
      if (userRepository.findByMail(requestEmail) != null) {
        return new ResponseBody("user with this mail (" + requestEmail + ") already exist");
      }
      var entity = new UserEntity(requestEmail,request.getPassword(), request.getFirstname(), request.getLastname());
      userRepository.persistAndFlush(entity);
      return new ResponseBody(true);
    } catch (HibernateException exception) {
      return new ResponseBody(exception.getMessage());
    }
  }

    /**
     * Autheticate a user
     * @param request user information
     * @return a {@link ResponseBody} with a token if success, message otherwise
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
  public ResponseBody login(UserLoginRequest request) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
    var targetUser = userRepository.findByMail(request.getEmail());
    if (targetUser == null) {
      return new ResponseBody("Unknown user with email:  " + request.getEmail());
    }
    var checkPassword = BCrypt.checkpw(request.getPassword().toString(), targetUser.getPassword());
    if (checkPassword) {
      return new ResponseBody(new AuthResponse(
        TokenUtils.generateToken(targetUser.getEmail(), targetUser.getRole(), duration, issuer))
      );
    }
    return new ResponseBody(UNAUTHORIZED);
  }
    /**
     * Update a user
     * @param userId id of user to update
     * @param userInformation new information of user
     * @param mailRequester mail of the requester (admin)
     * @return a {@link ResponseBody} with the updated user information, message otherwise
     */
  public ResponseBody updateUser(long userId, UserInformationRequest userInformation, String mailRequester) {
    try {
      var targetUser = userRepository.findById(userId);
      if (targetUser == null) {
        return new ResponseBody("unknown user with id (" + userId + ")");
      }

      var requestMail = userInformation.getEmail();
      if (userRepository.findByMail(requestMail) != null && !targetUser.getEmail().equals(requestMail)) {
        return new ResponseBody("Email already used");
      }
      var currentUser = userRepository.findByMail(mailRequester);
      if(!currentUser.getRole().equalsIgnoreCase(Role.ADMIN.toString())){
        return new ResponseBody(UNAUTHORIZED);
      }
      if(currentUser.getEmail().equalsIgnoreCase(targetUser.getEmail()) && !userInformation.getRole().equals(currentUser.getRole()) ){
        return new ResponseBody(UNAUTHORIZED);
      }
      targetUser.setFirstname(userInformation.getFirstname());
      targetUser.setLastname(userInformation.getLastname());
      if (!userInformation.getPassword().equals(targetUser.getPassword())) {
        targetUser.setPassword(userInformation.getPassword());
      }
      targetUser.setEmail(userInformation.getEmail());
      String role = Objects.requireNonNull(userInformation.getRole());
      for (Role r : Role.values()) {
        if (role.equals(r.toString())) {
          targetUser.setRole(userInformation.getRole());
        }
      }
      userRepository.persistAndFlush(targetUser);
      var reFetchUser = userRepository.findById(userId);
      return new ResponseBody(UserInformation.fromEntity(reFetchUser));
    } catch (HibernateException exception) {
      return new ResponseBody(exception.getMessage());
    }
  }

    /**
     * List all users of the database
     * @return a {@link UserListResponse}
     */
  public UserListResponse listAll() {
    var users = userRepository
      .listAll()
      .stream()
      .map(UserInformation::fromEntity)
      .collect(Collectors.toList());
    return new UserListResponse(users);
  }

    /**
     * Find a user by the given id
     * @param id id to search
     * @return a {@link ResponseBody} with {@link UserInformation} if success, message otherwise
     */
  public ResponseBody findById(long id) {
    var targetUser = userRepository.findById(id);
    if (targetUser == null) {
      return new ResponseBody("Unknown user with id: " + id);
    }
    return new ResponseBody(UserInformation.fromEntity(targetUser));
  }

    /**
     * Delete a user
     * @param id id of user to delete
     * @param mail mail of the requester
     * @return a {@link ResponseBody} with true if success, false otherwise
     */
  public ResponseBody deleteById(long id, String mail) {
    try {
      var targetUser = userRepository.findById(id);
      if (targetUser == null) {
        return new ResponseBody("Unknown user with id: " + id);
      }
      var currentUser = userRepository.findByMail(mail);
      if(currentUser == null){
          return new ResponseBody("Uknown current user");
      }
      if(!currentUser.getRole().equalsIgnoreCase(Role.ADMIN.toString())){
          return new ResponseBody(UNAUTHORIZED);
      }
      if(targetUser.getEmail().equalsIgnoreCase(currentUser.getEmail())){
          return new ResponseBody(UNAUTHORIZED);
      }
      userRepository.delete(targetUser);
      return new ResponseBody(true);
    } catch (HibernateException e) {
      return new ResponseBody(e.getMessage());
    }
  }
}