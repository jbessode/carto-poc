package fr.ngms.cartopoc.services;

import fr.ngms.cartopoc.models.geocoding.GeocodingPayload;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Objects;

/**
 * This class represent the service the geocoding interface.
 */
@ApplicationScoped
public class GeoCodingService {
    @ConfigProperty(name = "geocoding.format")
    String format;
    @ConfigProperty(name = "geocoding.url")
    String url;

    public GeoCodingService() {
        //Necessary for parsing
    }

    /**
     * This method request <a href="https://nominatim.org/">Nominatim</a> api, and return it is response.
     * @param payload the geocoding payload
     * @return a {@link Response} with <a href="https://nominatim.org/">Nominatim</a> api response
     * @throws IOException
     * @throws InterruptedException
     */
    public Response geocode(GeocodingPayload payload) throws IOException, InterruptedException {
        Objects.requireNonNull(payload);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + payload.getPayload().replace(" ", "+") + format))
                .timeout(Duration.ofMinutes(1))
                .GET()
                .build();
        HttpClient client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.body().contains("error"))
            return Response.status(400, "Unable to geocode").build();
        return Response.ok(response.body()).build();
    }

}
