package fr.ngms.cartopoc.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import fr.ngms.cartopoc.helpers.ImageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload.LayerInformation;
import fr.ngms.cartopoc.models.responses.geojson.LayerGeoJson;
import fr.ngms.cartopoc.models.resquests.LayerInformationRequest;
import fr.ngms.cartopoc.models.resquests.POIInsertionRequest;
import fr.ngms.cartopoc.models.resquests.POIImportRequest;
import fr.ngms.cartopoc.orm.entities.POIEntity;
import fr.ngms.cartopoc.orm.repositories.LayerRepository;
import org.hibernate.HibernateException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
/**
 * This class represent the service of a layer. This class deal with the {@link LayerRepository} to manage layer.
 * This class is the interface between {@link fr.ngms.cartopoc.endpoints.LayerController} and {@link LayerRepository}.
 */
@ApplicationScoped
public class  LayerService {
  private final LayerRepository layerRepository;

  private final Logger logger = Logger.getLogger(getClass().getName());

  private static String notFoundMessage(long id) {
    return "Layer with id (" + id + ") not exist";
  }

  private static String notFoundMessage(String name) {
    return "Layer with name (" + name + ") not exist";
  }

  @Inject
  public LayerService(LayerRepository layerRepository) {
    this.layerRepository = layerRepository;
  }
    /**
     * This method return a specified layer, find by an id.
     * @param layerId id to find the layer.
     * @return a {@link ResponseBody} with {@link LayerInformation} if success, message otherwise
     */
  public ResponseBody findById(long layerId) {
    var poiLayerEntity = layerRepository.findById(layerId);
    return (poiLayerEntity == null)
      ? new ResponseBody(notFoundMessage(layerId))
      : new ResponseBody(LayerInformation.fromPOILayerEntity(poiLayerEntity));
  }
    /**
     * This method return a specified layer, find by a name.
     * @param name name to find the layer.
     * @return a {@link ResponseBody} with {@link LayerInformation} if success, message otherwise
     */
  public ResponseBody findByName(String name) {
    var targetLayer = layerRepository.findByName(name);
    if (targetLayer == null) {
      return new ResponseBody(notFoundMessage(name));
    }
    return new ResponseBody(LayerInformation.fromPOILayerEntity(targetLayer));
  }
    /**
     * This method return all POIS of a layer.
     * @param layerId id of the layer
     * @return a {@link ResponseBody} with {@link LayerGeoJson} in payload if success, a message otherwise
     */
  public ResponseBody findLayerPOIById(long layerId) {
    var poiLayerEntity = layerRepository.findById(layerId);
    if (poiLayerEntity == null) {
      return new ResponseBody(notFoundMessage(layerId));
    }
    var attachedPois = poiLayerEntity.getAttachedPOI().stream()
            .sorted(Comparator.comparing(poi -> poi.getName().toUpperCase())).collect(Collectors.toList());
      var response = LayerGeoJson.fromPOILayerEntityAndPois(poiLayerEntity, attachedPois);
    return new ResponseBody(response);
  }
    /**
     * Update a layer
     * @param layerId id of layer to update
     * @param request updated information of the layer
     * @return a {@link ResponseBody} with {@link BasicLayersPayload} if success, message otherwise
     */
  @Transactional
  public ResponseBody updateLayerInformation(long layerId, LayerInformationRequest request) {
    try {
      var targetLayer = layerRepository.findById(layerId);
      if (targetLayer == null) {
        return new ResponseBody(notFoundMessage(layerId));
      }
      targetLayer.setName(request.getName());
      targetLayer.setDescription(request.getDescription());
      targetLayer.setLayerAttributes(String.join(";", request.getProperties()));
      if(request.getIcon() != null ){
          targetLayer.setIcon(ImageConverter.convertImage(request.getIcon()));
      }else {
          targetLayer.setIcon(null);
      }
      layerRepository.persistAndFlush(targetLayer);
      return new ResponseBody(BasicLayersPayload.fromPOILayerEntity(targetLayer));
    } catch (HibernateException | IOException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }
    /**
     * Delete a layer.
     * @param layerId id of layer to delete
     * @return a {@link ResponseBody} with true if the layer was deleted, message otherwise
     */
  @Transactional
  public ResponseBody deleteLayer(long layerId) {
    try {
      var targetLayer = layerRepository.findById(layerId);
      if (targetLayer == null) {
        return new ResponseBody(notFoundMessage(layerId));
      }
      layerRepository.delete(targetLayer);
      return new ResponseBody(true);
    } catch (HibernateException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }
    /**
     * Attach a POI to a layer.
     * @param layerId id of the layer
     * @param request the information of the POI
     * @return a {@link ResponseBody} with {@link LayerGeoJson} if success, message otherwise
     */
  @Transactional
  public ResponseBody attachPOIAtLayer(long layerId, POIInsertionRequest request) {
    try {
      var targetLayer = layerRepository.findById(layerId);
      if (targetLayer == null) {
        return new ResponseBody(notFoundMessage(layerId));
      }
      var poiEntities = new ArrayList<POIEntity>();
      request
        .getPois()
        .forEach(poiRequest -> {
          var poiEntity = POIEntity.fromRequest(poiRequest);
          if (!targetLayer.containsPOI(poiEntity)) {
            poiEntity.setLayer(targetLayer);
            poiEntities.add(poiEntity);
          }
        });
      targetLayer.addListPOI(poiEntities);
      layerRepository.persistAndFlush(targetLayer);

      var reFetchLayer = layerRepository.findById(layerId);
      return new ResponseBody(LayerGeoJson.fromPOILayerEntityAndPois(reFetchLayer, reFetchLayer.getAttachedPOI()));
    } catch (HibernateException e) {
      logger.severe(e.getMessage());
      return new ResponseBody(e.getMessage());
    }
  }
    /**
     * Export a layer.
     * @param layer layer information to export
     * @return a {@link ResponseBody} with base64 encoded file
     */
    public ResponseBody exportLayer(LayerInformation layer) throws IOException {
        var resp = this.findLayerPOIById(layer.getId());
        var file = Paths.get(layer.getName() + ".geojson");
        Files.createFile(file);
        var objectMappper = new ObjectMapper();
        var string = objectMappper.writeValueAsString(resp.getPayload());
        Files.writeString(file, string);
        return new ResponseBody(Base64.getEncoder().encodeToString(Files.readAllBytes(file)), true);
    }

    /**
     * Import POI into a layer
     * @param id id of layer
     * @param layer encoded GeoJson file in base64 Format
     * @return ResponseBody with {@link LayerGeoJson} if success, message otherwise
     */
    @Transactional
    public ResponseBody importPoi(long id, POIImportRequest layer) throws IOException {
       try {
           var targetLayer = layerRepository.findById(id);

           if (targetLayer == null) {
               return new ResponseBody(notFoundMessage(id));
           }
           var file = Base64.getDecoder().decode(layer.getFile64Encoded());
           if (file == null) {
               return new ResponseBody("Incorrect encoded file");
           }
           var objectMapper = new ObjectMapper();
           objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
           var layerParsed = objectMapper.readValue(file, LayerGeoJson.class);
           var poiInsertion = new POIInsertionRequest();
           var lstPoiRequest = new ArrayList<POIInsertionRequest.POIRequest>();
           for (var feature : layerParsed.getFeatures()) {
               if (!feature.getGeometry().get(
                       feature.getGeometry().keySet().stream().filter(key -> key.equalsIgnoreCase("TYPE")).findFirst()
                               .orElse("NULL")).toString().equalsIgnoreCase("POINT")) {
                   return new ResponseBody("Incorrect geometry type");
               }
               var poiRequest = new POIInsertionRequest.POIRequest();
               String name = null;
               var nameKey = feature.getProperties().keySet().stream().filter(key -> key.toUpperCase().contains("NAME")).findFirst();
               if (nameKey.isEmpty()) {
                   nameKey = feature.getProperties().keySet().stream().filter(key -> key.toUpperCase().contains("NOM")).findFirst();
               }
               if (nameKey.isEmpty()) {
                   nameKey = feature.getProperties().keySet().stream().filter(key -> key.toUpperCase().contains("ID")).findFirst();
               }
               if (nameKey.isEmpty()) {
                   name = "POI : " + targetLayer.getName();
               } else {
                   name = (String) feature.getProperties().get(nameKey.get());
               }
               poiRequest.setName(name);
               var coord = (List<Double>) feature.getGeometry().get("coordinates");
               if (coord == null) {
                   return new ResponseBody("Incorrect file format");
               }
               poiRequest.setX(coord.get(0));
               poiRequest.setY(coord.get(1));
               var listAttr = Arrays.asList(targetLayer.getLayerAttributes().split(";"));
               var prop = new HashMap<String, Object>();
               feature.getProperties().forEach((key, value) -> {
                   if (listAttr.contains(key)) {
                       prop.put(key, value);
                   }
               });
               poiRequest.setProperties(prop);
               lstPoiRequest.add(poiRequest);
           }
           poiInsertion.setPois(lstPoiRequest);
           return this.attachPOIAtLayer(id, poiInsertion);
       }catch (Exception e ){
           return new ResponseBody("Incorrect imported file");
       }
    }


}