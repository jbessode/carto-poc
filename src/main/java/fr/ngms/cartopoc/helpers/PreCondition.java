package fr.ngms.cartopoc.helpers;

/**
 * Pre condition Helper class
 */
public class PreCondition {
  private PreCondition() {
  }

    /**
     * Control the value of the given id
     * @param id the id to check
     * @return true if the id is valid, false otherwise
     */
  public static boolean idIsNegative(long id) {
    return id <= 0;
  }

}
