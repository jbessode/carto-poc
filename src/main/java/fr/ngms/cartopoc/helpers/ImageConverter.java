package fr.ngms.cartopoc.helpers;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * Helper class to convert Image
 */
public class ImageConverter {
    private ImageConverter(){
        //private constructor
    }

    /**
     * Static method that convert and resize base64 image
     * @param image encode image in base64
     * @return the resized encode base64 image
     * @throws IOException
     */
    public static String convertImage(String image) throws IOException {
        var str = Base64.getDecoder().decode(image);
        try(var input = new ByteArrayInputStream(str)){
            var im = ImageIO.read(input);
            var resized = new BufferedImage(20,20, BufferedImage.TYPE_4BYTE_ABGR_PRE);
            Graphics2D g2 = (Graphics2D) resized.getGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2.drawImage(im, 0, 0,20,20, new Color(0,0,0,0),null);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write( resized, "png", baos );
                baos.flush();
                byte[] resizedInByte = baos.toByteArray();
               return Base64.getEncoder().encodeToString(resizedInByte);
            }
        }
    }
}
