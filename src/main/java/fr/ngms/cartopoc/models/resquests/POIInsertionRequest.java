package fr.ngms.cartopoc.models.resquests;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classic model class for Json parsing, representing an insertion request of a POI
 */
public class POIInsertionRequest {
  public static class POIRequest {
    @NotNull
    @JsonProperty("posX")
    private double x;

    @NotNull
    @NotEmpty
    @NotBlank
    @Size(max = 255)
    @JsonProperty("name")
    private String name;

    @NotNull
    @JsonProperty("posY")
    private double y;

    @JsonProperty("attributesData")
    private Map<String, Object> properties = new HashMap<>();

    public double getX() {
      return x;
    }

    public double getY() {
      return y;
    }

    public void setX(double x) {
      this.x = x;
    }

    public void setY(double y) {
      this.y = y;
    }

    public Map<String, Object> getProperties() {
      return properties;
    }

    public void setProperties(Map<String, Object> properties) {
      this.properties = properties;
    }

    @Override
    public String toString() {
      return "POIRequest{" +
        "x=" + x +
        ", name='" + name + '\'' +
        ", y=" + y +
        ", properties=" + properties +
        '}';
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }
  }


  @JsonProperty("pois")
  private List<POIRequest> pois;

  public POIInsertionRequest() {
    // default constructor for jackson
  }

  public List<POIRequest> getPois() {
    return pois;
  }

  public void setPois(List<POIRequest> pois) {
    this.pois = pois;
  }
}
