package fr.ngms.cartopoc.models.resquests;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic model class, for Json parsing, that represent a Layer with basic information
 */
public class LayerInformationRequest {
  @NotNull
  @NotEmpty
  @NotBlank
  @Size(max = 50)
  @JsonProperty("name")
  private String name;

  @NotNull
  @NotEmpty
  @NotBlank
  @JsonProperty("description")
  private String description;

  @JsonProperty("layerAttributes")
  private List<String> properties = new ArrayList<>();

  @JsonProperty("icon")
  private String icon;

  public List<String> getProperties() {
    return properties;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setProperties(List<String> properties) {
    this.properties = properties;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

    @Override
  public String toString() {
    return "POILayerRequest{" +
      "name='" + name + '\'' +
      ", description='" + description + '\'' +
      ", properties=" + properties +
      '}';
  }

  public String errorMessage() {
    var builder = new StringBuilder("fields: ");
    if (name == null || name.isEmpty() || name.isBlank()) {
      builder.append("name").append(" , ");
    }

    if (description == null || description.isEmpty() || description.isBlank()) {
      builder.append("description ");
    }

    builder.append("missing");
    return builder.toString();
  }
}
