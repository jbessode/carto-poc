package fr.ngms.cartopoc.models.resquests;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Basic model class, for Json parsing that represent a category
 */
public class CategoryCreationRequest {
  @NotNull
  @NotEmpty
  @NotBlank
  @Size(max = 50)
  @JsonProperty("name")
  private String name;

  @NotNull
  @NotEmpty
  @NotBlank
  @JsonProperty("description")
  private String description;

  public CategoryCreationRequest() {

  }

  public CategoryCreationRequest(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public String getName() {
    return this.name;
  }

  public String getDescription() {
    return this.description;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String errorMessage() {
    var builder = new StringBuilder("fields: ");
    if (name == null || name.isEmpty() || name.isBlank()) {
      builder.append("name").append(" , ");
    }

    if (description == null || description.isEmpty() || description.isBlank()) {
      builder.append("description ");
    }

    builder.append("missing");
    return builder.toString();
  }
}
