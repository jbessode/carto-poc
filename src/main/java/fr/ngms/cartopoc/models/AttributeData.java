package fr.ngms.cartopoc.models;

import java.util.Objects;

/**
 * Classic POJO class representing attribute data of a POI
 */
public class AttributeData {
  private String key;
  private String value;

  public AttributeData() {
  }

  public AttributeData(String key, String value) {
    this.key = Objects.requireNonNull(key);
    this.value = Objects.requireNonNull(value);
  }

  public String getKey() {
    return key;
  }

  public String getValue() {
    return value;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
