package fr.ngms.cartopoc.models.responses;

import java.io.Serializable;

public interface IResponsePayload extends Serializable { }
