package fr.ngms.cartopoc.models.responses;

import java.io.Serializable;

/**
 * This class represent a classic server response body.
 */
public class ResponseBody implements Serializable {
  private final String message;
  private final boolean success;
  private final IResponsePayload payload;

  public ResponseBody(String message) {
    this.message = message;
    this.success = false;
    this.payload = null;
  }

  public ResponseBody(String message, boolean success) {
    this.message = message;
    this.success = success;
    this.payload = null;
  }

  public ResponseBody(String message, boolean success, IResponsePayload result) {
    this.message = message;
    this.success = success;
    this.payload = result;
  }

  public ResponseBody(IResponsePayload payload) {
    this.message = null;
    this.success = true;
    this.payload = payload;
  }

  public ResponseBody(boolean success) {
    this.message = null;
    this.success = success;
    this.payload = null;
  }

  public String getMessage() {
    return message;
  }

  public boolean isSuccess() {
    return success;
  }

  public IResponsePayload getPayload() {
    return payload;
  }

  @Override
  public String toString() {
    return "ResponseBody{" +
      "message='" + message + '\'' +
      ", success=" + success +
      ", paylaod=" + payload +
      '}';
  }
}
