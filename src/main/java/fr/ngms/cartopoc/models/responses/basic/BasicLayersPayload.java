package fr.ngms.cartopoc.models.responses.basic;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.IResponsePayload;
import fr.ngms.cartopoc.orm.entities.LayerEntity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classic model class, representing a layer with it's basic information
 */
public class BasicLayersPayload implements IResponsePayload {
  public static class LayerInformation implements IResponsePayload {
    @JsonProperty("id")
    private final long id;
    @JsonProperty("name")
    private final String name;

    @JsonProperty("description")
    private final String description;

    @JsonProperty("attributes")
    private final List<String> properties;

    @JsonProperty("icon")
    private final String icon;

    //use for tests
    LayerInformation() {
      this.description = null;
      this.id = -1;
      this.name = null;
      this.properties = null;
      this.icon = null;
    }

    public LayerInformation(long id, String name, String description, List<String> properties, String icon) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.properties = properties;
      this.icon = icon;
    }

      /**
       * Static method that transform {@link LayerEntity} to {@link LayerInformation}
       * @param entity the layer entity
       * @return the layer information {@link LayerInformation}
       */
    public static LayerInformation fromPOILayerEntity(LayerEntity entity) {
      var description = entity.getDescription();
      var name = entity.getName();
      var properties = Arrays.stream(entity.getLayerAttributes().split(";")).collect(Collectors.toList());
      return new LayerInformation(entity.getId(), name, description, properties, entity.getIcon());
    }

    public String getName() {
      return name;
    }

    public String getDescription() {
      return description;
    }

    public List<String> getProperties() {
      return properties;
    }

    public long getId() {
      return id;
    }
  }

  @JsonProperty("layers")
  private final List<LayerInformation> layers;

  public BasicLayersPayload() {
    this.layers = null;
  }

  public BasicLayersPayload(List<LayerInformation> layers) {
    this.layers = layers;
  }

    /**
     * Static method that transform {@link LayerEntity} to a {@link BasicLayersPayload}
     * @param entity layer entity
     * @return a {@link BasicLayersPayload}
     */
  public static BasicLayersPayload fromPOILayerEntity(LayerEntity entity) {
    return new BasicLayersPayload(List.of(LayerInformation.fromPOILayerEntity(entity)));
  }

  public List<LayerInformation> getLayers() {
    return layers;
  }

  @Override
  public String toString() {
    return "BasicLayersPayload{" +
      "layers=" + layers +
      '}';
  }
}
