package fr.ngms.cartopoc.models.responses.basic;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.IResponsePayload;
import fr.ngms.cartopoc.orm.entities.CategoryEntity;

import java.util.List;

/**
 * Classic model class, representing a category with it's basic information.
 */
public class BasicCategoryPayload implements IResponsePayload {
  public static class CategoryInformation implements IResponsePayload {
    @JsonProperty("id")
    private final long id;

    @JsonProperty("name")
    private final String name;

    @JsonProperty("description")
    private final String description;

    //use for tests
    CategoryInformation() {
      this.id = -1;
      this.name = null;
      this.description = null;
    }

    public CategoryInformation(long id, String name, String description) {
      this.id = id;
      this.name = name;
      this.description = description;
    }

      /**
       * Static method that transform {@link CategoryEntity} to a {@link CategoryInformation}
       * @param category the entity to transform
       * @return the {@link CategoryInformation} parsed
       */
    public static CategoryInformation from(CategoryEntity category) {
      return new CategoryInformation(category.getId(), category.getName(), category.getDescription());
    }

    public long getId() {
      return id;
    }

    public String getName() {
      return name;
    }

    public String getDescription() {
      return description;
    }
  }

  @JsonProperty("categories")
  private final List<CategoryInformation> categories;

  public BasicCategoryPayload() {
    this.categories = null;
  }

  public BasicCategoryPayload(List<CategoryInformation> categories) {
    this.categories = categories;
  }

  public List<CategoryInformation> getCategories() {
    return categories;
  }

}
