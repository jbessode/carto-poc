package fr.ngms.cartopoc.models.responses.geojson;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.helpers.AttributeDataHelper;
import fr.ngms.cartopoc.models.AttributeData;
import fr.ngms.cartopoc.orm.entities.POIEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Model class that represent a POI in GeoJson format
 */
public class POIGeoJson {
  @JsonProperty("type")
  private final String type;

  @JsonProperty("geometry")
  private final Map<String, Object> geometry;

  @JsonProperty("properties")
  private final Map<String, Object> properties;

  //use for tests
  POIGeoJson() {
    this.type = null;
    this.geometry = null;
    this.properties = null;
  }

  private POIGeoJson(Map<String, Object> geometry, Map<String, Object> properties) {
    this.type = "Feature";
    this.geometry = geometry;
    this.properties = properties;
  }

    /**
     * Static class that transform a {@link POIEntity} to a {@link POIGeoJson}
     * @param entity Entity of the POI
     * @return a POI in GeoJson format {@link POIGeoJson}
     */
  public static POIGeoJson fromPOIEntity(POIEntity entity) {
    var geometry = new HashMap<String, Object>();
    geometry.put("type", "Point");
    geometry.put("coordinates", List.of(entity.getPosX(), entity.getPosY()));
    Map<String,Object> properties = AttributeDataHelper.parse(entity.getProperties())
      .stream()
      .collect(Collectors.toMap(AttributeData::getKey, AttributeData::getValue));
    properties.put("id", "" + entity.getId());
    properties.put("name", "" + entity.getName());
    return new POIGeoJson(geometry, properties);
  }

  public Map<String, Object> getGeometry() {
    return geometry;
  }

  public Map<String, Object> getProperties() {
    return properties;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return "{" +
      "type : "+ type +
      ", properties : " + properties +

      '}';
  }
}
