package fr.ngms.cartopoc.models.responses.geojson;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.IResponsePayload;
import fr.ngms.cartopoc.orm.entities.LayerEntity;
import fr.ngms.cartopoc.orm.entities.POIEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LayerGeoJson implements IResponsePayload {
  @JsonProperty("type")
  private final String type;

  @JsonProperty("features")
  private final List<POIGeoJson> features;

  @JsonProperty("properties")
  private final Map<String, Object> properties;

  //use for tests
  LayerGeoJson() {
    this.type = null;
    this.features = null;
    this.properties = null;
  }

  private LayerGeoJson(List<POIGeoJson> features, Map<String, Object> properties) {
    this.type = "FeatureCollection";
    this.features = features;
    this.properties = properties;
  }

  private static LayerGeoJson buildGeoJson(LayerEntity layerEntity, List<POIEntity> poiEntities) {
    var attachedPois = poiEntities;
    var properties = new HashMap<String, Object>();
    properties.put("id", "" + layerEntity.getId());
    properties.put("name", layerEntity.getName());
    properties.put("description", layerEntity.getDescription());
    var feature = attachedPois.stream().map(POIGeoJson::fromPOIEntity).collect(Collectors.toList());
    return new LayerGeoJson(feature, properties);
  }

  public static LayerGeoJson fromPOILayerEntityAndPois(LayerEntity targetLayer, List<POIEntity> poiEntities) {
    return buildGeoJson(targetLayer, poiEntities);
  }

  public Map<String, Object> getProperties() {
    return properties;
  }

  public List<POIGeoJson> getFeatures() {
    return features;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return "{" +
      "type : " + type  +
      ", features : " + features +
      '}';
  }
}
