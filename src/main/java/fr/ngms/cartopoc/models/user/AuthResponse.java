package fr.ngms.cartopoc.models.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.IResponsePayload;

/**
 * Model class use for Json parsing. Use to authenticate a user and response a token.
 */
public class AuthResponse implements IResponsePayload {
  private String token;

  @JsonCreator
  public AuthResponse(@JsonProperty("token") String token) {
    this.token = token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }
}
