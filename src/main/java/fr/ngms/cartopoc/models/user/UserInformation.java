package fr.ngms.cartopoc.models.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.IResponsePayload;
import fr.ngms.cartopoc.orm.entities.UserEntity;

/**
 * Model class use to show user information
 */
public class UserInformation implements IResponsePayload {
  @JsonProperty("id")
  private final long id;

  @JsonProperty("email")
  private final String email;

  @JsonProperty("password")
  private final String password;

  @JsonProperty("firstname")
  private final String firstname;

  @JsonProperty("lastname")
  private final String lastname;

  @JsonProperty("role")
  private final String role;

  public UserInformation() { // for tests
    this.id = -1;
    this.email = null;
    this.password = null;
    this.firstname = null;
    this.lastname = null;
    this.role = null;
  }

  public UserInformation(long id, String email, String password, String firstname, String lastname, String role) {
    this.id = id;
    this.email = email;
    this.password = password;
    this.firstname = firstname;
    this.lastname = lastname;
    this.role = role;
  }

    /**
     * Static method that transform {@link UserEntity} to {@link UserInformation}, transform the orm entity to Json format
      * @param entity user entity
     * @return
     */
  public static UserInformation fromEntity(UserEntity entity) {
    return new UserInformation(entity.id, entity.getEmail(), entity.getPassword(),
      entity.getFirstname(), entity.getLastname(), entity.getRole());
  }

  public long getId() {
    return id;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public String getRole() {
    return role;
  }
}
