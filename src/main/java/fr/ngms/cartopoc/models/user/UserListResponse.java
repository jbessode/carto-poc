package fr.ngms.cartopoc.models.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ngms.cartopoc.models.responses.IResponsePayload;

import java.util.ArrayList;
import java.util.List;

/**
 * Model use to gel all users known by the api
 */
public class UserListResponse implements IResponsePayload {

  @JsonProperty("users")
  private final List<UserInformation> userInformations;

  public UserListResponse() {
    this.userInformations = new ArrayList<>(); //  for tests
  }

  public UserListResponse(List<UserInformation> userInformations) {
    this.userInformations = userInformations;
  }

  public List<UserInformation> getUserInformations() {
    return userInformations;
  }

}
