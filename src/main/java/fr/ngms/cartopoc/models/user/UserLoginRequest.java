package fr.ngms.cartopoc.models.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Model use for login a user
 */
public class UserLoginRequest {
  @NotNull
  @NotEmpty
  @NotBlank
  @Email
  private String email;

  @NotNull
  @NotEmpty
  @NotBlank
  private String password;

  public UserLoginRequest() {
    //Empty constructor
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }
}
