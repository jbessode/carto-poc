package fr.ngms.cartopoc.security.utils;


import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is used for manage token. We use JWT token encode with BCrypt.
 */
public class TokenUtils {

  private TokenUtils() {
  }

    /**
     * This method is called when a user is login. If the user is known by the system, a token will
     * generated with this information
     * @param username username (email) of the user
     * @param role the {@link fr.ngms.cartopoc.models.user.Role} of the user
     * @param duration a time duration
     * @param issuer the issuer of the token
     * @return the generated token
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
  public static String generateToken(String username, String role, Long duration, String issuer) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
    String privateKeyLocation = "/META-INF/resources/privatekey.pem";
    PrivateKey privateKey = readPrivateKey(privateKeyLocation);

    JwtClaimsBuilder claimsBuilder = Jwt.claims();
    long currentTimeInSecs = currentTimeInSecs();

    Set<String> groups = new HashSet<>();
    groups.add(role);

    claimsBuilder.issuer(issuer);
    claimsBuilder.subject(username);
    claimsBuilder.issuedAt(currentTimeInSecs);
    claimsBuilder.expiresAt(currentTimeInSecs + duration);
    claimsBuilder.groups(groups);

    return claimsBuilder.jws().sign(privateKey);
  }

  public static PrivateKey readPrivateKey(final String pemResName) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
    try (InputStream contentIS = TokenUtils.class.getResourceAsStream(pemResName)) {
      byte[] tmp = new byte[4096];
      int length = contentIS.read(tmp);
      return decodePrivateKey(new String(tmp, 0, length, StandardCharsets.UTF_8));
    }
  }

  public static PrivateKey decodePrivateKey(final String pemEncoded) throws NoSuchAlgorithmException, InvalidKeySpecException {
    byte[] encodedBytes = toEncodedBytes(pemEncoded);

    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    return kf.generatePrivate(keySpec);
  }

  public static byte[] toEncodedBytes(final String pemEncoded) {
    final String normalizedPem = removeBeginEnd(pemEncoded);
    return Base64.getDecoder().decode(normalizedPem);
  }

  public static String removeBeginEnd(String pem) {
    pem = pem.replaceAll("-----BEGIN (.*)-----", "");
    pem = pem.replaceAll("-----END (.*)----", "");
    pem = pem.replace("\r\n", "");
    pem = pem.replace("\n", "");
    return pem.trim();
  }

  public static int currentTimeInSecs() {
    long currentTimeMS = System.currentTimeMillis();
    return (int) (currentTimeMS / 1000);
  }
}
