package fr.ngms.cartopoc.endpoints;

import fr.ngms.cartopoc.helpers.Constants;
import fr.ngms.cartopoc.helpers.PreCondition;
import fr.ngms.cartopoc.models.responses.ResponseBody;
import fr.ngms.cartopoc.models.responses.basic.BasicCategoryPayload;
import fr.ngms.cartopoc.models.responses.basic.BasicLayersPayload;
import fr.ngms.cartopoc.models.resquests.CategoryCreationRequest;
import fr.ngms.cartopoc.models.resquests.LayerInformationRequest;
import fr.ngms.cartopoc.services.CategoryService;
import fr.ngms.cartopoc.services.UserService;
import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

import static fr.ngms.cartopoc.helpers.Constants.UNKNOWN_USER;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * This class is the api endpoint for managing categories. This endpoint will create, update, delete a category.
 * Also, create and attach a layer to a category.
 */

@Path("/categories")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class CategoryController {

    private final CategoryService service;
    private final Validator validator;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final UserService userService;

    @Inject
    public CategoryController(CategoryService service, Validator validator, UserService userService) {
        this.service = service;
        this.validator = validator;
        this.userService = userService;
    }

    /**
     * This method return all the categories known by the api.
     * @param context
     * @return all the categories
     */
    @APIResponse(description = "Return all categories",
            content = @Content(schema = @Schema(implementation = BasicCategoryPayload.class)
            ))
    @Authenticated
    @GET
    @Path("/")
    public Response getAll(@Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            var response = service.getAllCategories();
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method return a category, find by the given id.
     * @param id the id to find category
     * @param context
     * @return the category find by the given ID
     */
    @APIResponse(description = "Return a specify categorie", content = @Content(
            schema = @Schema(implementation = BasicCategoryPayload.CategoryInformation.class)
    ))
    @Authenticated
    @GET
    @Path("/{id}")
    public Response findById(@Valid @PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            if (PreCondition.idIsNegative(id)) {
                return Response.status(Status.BAD_REQUEST).entity("id must be greater than 0 not").build();
            }
            var response = service.findPoiCategoryById(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.NOT_FOUND).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method create a category.
     * @param request the category to create
     * @param context
     * @return the category created
     */
    @APIResponse(description = "Return a specify categorie", content = @Content(
            schema = @Schema(implementation = BasicCategoryPayload.CategoryInformation.class)
    ))
    @RolesAllowed({"ADMIN"})
    @POST
    @Path("/create")
    public Response createPoiCategory(CategoryCreationRequest request, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            if (request == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                logger.severe(request.errorMessage());
                return Response.status(Status.BAD_REQUEST).entity(new ResponseBody(request.errorMessage())).build();
            }

            var response = service.createPoiCategoryEntity(request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.CONFLICT).entity(response).build();
            }
            return Response.status(Status.CREATED).entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method delete a category. The category is find by the given id.
     * @param id the id of the category
     * @param context
     * @return true if category is deleted, false otherwise
     */
    @RolesAllowed({"ADMIN"})
    @DELETE
    @Path("/delete/{id}")
    public Response deleteCategory(@Valid @PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            var response = service.deleteCategory(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method update a category.
     * @param id the id of the category
     * @param request the updated information of the category
     * @param context
     * @return return the updated category
     */
    @APIResponse(description = "Return the category with its modified attributes", content = @Content(
            schema = @Schema(implementation = BasicCategoryPayload.CategoryInformation.class)
    ))
    @RolesAllowed({"EDITOR", "ADMIN"})
    @PUT
    @Path("/update/{id}")
    public Response updateCategory(@Valid @PathParam("id") long id, CategoryCreationRequest request,
                                   @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            if (request == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response.status(Status.BAD_REQUEST).entity(new ResponseBody(request.errorMessage())).build();
            }
            var response = service.updateCategory(id, request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method return all layers of a category.
     * @param id the id of the category
     * @param context
     * @return the attached layer of the given category
     */
    @APIResponse(description = "Return all layers attached by the specified category", content = @Content(
            schema = @Schema(implementation = BasicLayersPayload.class)
    ))
    @Authenticated
    @GET
    @Path("/{id}/layers")
    public Response getLayersOfCategory(@Valid @PathParam("id") long id, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            var response = service.getLayersOfCategory(id);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method attach a layer into a category. In other terms, this method create a layer, and attach it to the
     * given category, find by the given id.
     * @param id the id of the category
     * @param request the layer that will be attached to the category
     * @param context
     * @return the category with its informations
     */
    @APIResponse(description = "Return the layer atteched", content = @Content(
            schema = @Schema(implementation = BasicLayersPayload.class)
    ))
    @RolesAllowed({"ADMIN"})
    @POST
    @Path("/{id}/attachlayer")
    public Response attachLayerInCategory(@Valid @PathParam("id") long id, LayerInformationRequest request,
                                          @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            if (request == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(request);
            if (!violations.isEmpty()) {
                return Response
                        .status(Status.BAD_REQUEST).entity(new ResponseBody(request.errorMessage()))
                        .build();
            }
            var response = service.attachLayer(id, request);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }

    /**
     * This method update the category of a layer. In other terms,
     * this method attach the given layer to an other category.
     * @param idCurrentCategory id of the current category wich layer is attached
     * @param layer layer to update
     * @param idCategoryToAttach id of the category wich layer will be attached
     * @param context
     * @return true if layer was correctly attached, false otherwise
     */
    @APIResponse(description = "Attach a layer to an other category", content = @Content(
            schema = @Schema(implementation = BasicLayersPayload.class)
    ))
    @RolesAllowed({"ADMIN"})
    @POST
    @Path("/{idCurrentCategory}/attachLayerTo/{idCategoryToAttach}")
    public Response attachLayerOtherCategoryTo(@Valid @PathParam("idCurrentCategory") long idCurrentCategory, BasicLayersPayload.LayerInformation layer,
                                               @Valid @PathParam("idCategoryToAttach") long idCategoryToAttach, @Context SecurityContext context) {
        try {
            if(!userService.isKnownUser(context)){
                return Response.status(Status.UNAUTHORIZED).entity(UNKNOWN_USER).build();
            }
            if (layer == null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(Constants.REQUEST_NOT_NULL).build();
            }

            var violations = validator.validate(layer);
            if (!violations.isEmpty()) {
                return Response
                        .status(Status.BAD_REQUEST).entity(new ResponseBody("Constraint Violations"))
                        .build();
            }
            var response = service.attachLayerOtherCategoryTo(idCurrentCategory, idCategoryToAttach, layer);
            if (!response.isSuccess() || response.getMessage() != null) {
                return Response.status(Status.EXPECTATION_FAILED).entity(response).build();
            }
            return Response.ok().entity(response).build();
        } catch (Exception e) {
            logger.severe(e.getMessage());
            return Response.serverError().entity(new ResponseBody(e.getMessage())).build();
        }
    }


}