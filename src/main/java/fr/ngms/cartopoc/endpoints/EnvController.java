package fr.ngms.cartopoc.endpoints;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.HashMap;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Endpoint to get system environment variable.
 */
@Path("/env")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class EnvController {
  @ConfigProperty(name = "TILESERVER_URL")
  String tileserverUrl;

    /**
     * Get environment variable
     * @param query the name of environment variable
     * @return a String corresponding to the environment variable requested
     */
  @Path("/values")
  @GET
  public Response getAngularConfigurationVariable(@QueryParam(value = "query") String query) {
    try {
      var unmodifiableMap = System.getenv();
      var env = new HashMap<>(unmodifiableMap);
      if (!env.containsKey(query)) {
        env.put(query, tileserverUrl);
      }

      if (query != null && (!query.isEmpty() || !query.isBlank())) {
        var result = new HashMap<String, String>();
        result.put(query, env.get(query));
        return Response.ok().entity(result).build();
      }
      return Response.ok().entity(env).build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getMessage()).build();
    }
  }
}