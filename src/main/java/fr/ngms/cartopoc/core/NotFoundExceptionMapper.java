package fr.ngms.cartopoc.core;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    /**
     *
      * @param exception
     * @return to the specified 404 not foud page.
     */
  @Override
  public Response toResponse(NotFoundException exception) {
    String text = new Scanner(getClass().getResourceAsStream("/META-INF/resources/index.html"), StandardCharsets.UTF_8)
      .useDelimiter("\\A")
      .next();
    return Response.status(302).entity(text).build();
  }
}
