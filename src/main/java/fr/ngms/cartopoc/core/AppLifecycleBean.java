package fr.ngms.cartopoc.core;

import fr.ngms.cartopoc.models.resquests.CategoryCreationRequest;
import fr.ngms.cartopoc.orm.entities.UserEntity;
import fr.ngms.cartopoc.orm.repositories.UserRepository;
import fr.ngms.cartopoc.services.CategoryService;
import fr.ngms.cartopoc.services.UserService;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.logging.Logger;

@ApplicationScoped
public class AppLifecycleBean {
  private final Logger logger = Logger.getLogger(getClass().getName());
  @Inject
  CategoryService categoryService;
  @Inject
  UserRepository userRepository;
  @Inject
  UserService userService;
  @ConfigProperty(name = "fr.ngms.other.category.name")
  private String nameCategory;
  @ConfigProperty(name = "fr.ngms.other.category.description")
  private String descriptionCategory;
  private static final String ADMIN = "ADMIN";
  private static final String EDITOR = "EDITOR";


    /**
     *
     * @param ev
     */
  @Transactional
    void onStart(@Observes StartupEvent ev) {
      categoryService.createPoiCategoryEntity(new CategoryCreationRequest(nameCategory, descriptionCategory));
      var persitsUsers = new ArrayList<UserEntity>();
      var adminUSer = new UserEntity("admin@admin","admin",ADMIN,ADMIN);
      adminUSer.setRole(ADMIN);
      if(userRepository.findByMail(adminUSer.getEmail()) == null ){
          persitsUsers.add(adminUSer);
      }
        var editor = new UserEntity("editor@editor","editor", EDITOR, EDITOR);
        editor.setRole(EDITOR);
        if(userRepository.findByMail(editor.getEmail()) == null ){
            persitsUsers.add(editor);
        }
        var user = new UserEntity("user@user","user","USER","USER");
        if(userRepository.findByMail(user.getEmail()) == null ){
            persitsUsers.add(user);
        }
        userRepository.persist(persitsUsers.stream());
        userRepository.flush();
      logger.info("The application is starting...");

  }

  void onStop(@Observes ShutdownEvent ev) {
    logger.info("The application is stopping...");
  }
}